<?php

namespace app\service;

use app\models\Conversation;
use app\models\ConversationSettings;
use app\models\User;
use Yii;
use function Amp\Promise\all;

class Notify
{
    public static function sendNotify($key, $user_id, $message)
    {
        $user = User::findOne($user_id);
        $setting = ConversationSettings::findOne($key);
        if ($setting->conversation_inner) {
            Notify::sendInnerNotify($user->id,$message);
        }
        if ($setting->conversation_email) {
            Notify::sendEmailNotify($user->email,$message);
        }
        if ($setting->conversation_telegram) {
            Notify::sendTelegramNotify($user->nik_telegram,$message);
        }
    }

    public static function sendNotifyToAdmin($key, $message)
    {
        $setting = ConversationSettings::findOne($key);
        if ($setting->conversation_inner) {
            Notify::sendInnerNotifyToAdmin($message);
        }
        if ($setting->conversation_email) {
            Notify::sendEmailNotifyToAdmin($message);
        }
        if ($setting->conversation_telegram) {
            Notify::sendTelegramNotifyToAdmin($message);
        }
    }

    public static function sendInnerNotify($user, $message)
    {
        $notify = new Conversation();
        $notify->user_one = 999999;
        $notify->user_two = $user;
        $notify->message = $message;
        $notify->save();
        Yii::error(serialize($notify->errors));
    }

    public static function sendInnerNotifyToAdmin($message)
    {
        $users = User::find()->where(['role' => User::ROLE_ADMIN])->andWhere(['<>', 'id', 999999])->all();
        foreach ($users as $user) {
            $notify = new Conversation();
            $notify->user_one = 999999;
            $notify->user_two = $user->id;
            $notify->message = $message;
            $notify->save();
            Yii::error(serialize($notify->errors));
        }


    }

    public static function sendEmailNotify($email, $message)
    {
        Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom(['system@gmail.com' => 'Система'])
            ->setSubject('Сообщение системі оповещений apartamio.ru')
            ->setTextBody($message)
            ->send();
    }

    public static function sendEmailNotifyToAdmin($message)
    {
        $users = User::find()->where(['role' => User::ROLE_ADMIN])->andWhere(['<>', 'id', 999999])->all();
        foreach ($users as $user) {
            Yii::$app->mailer->compose()
                ->setTo($user->email)
                ->setFrom(['system@gmail.com' => 'Система'])
                ->setSubject('Сообщение системі оповещений apartamio.ru')
                ->setTextBody($message)
                ->send();
        }
    }

    public static function sendTelegramNotify($id, $message)
    {

    }

    public static function sendTelegramNotifyToAdmin($message)
    {

    }

}