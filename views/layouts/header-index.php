<?php

use app\widgets\TopMenu;
use yii\helpers\Html;

?>
<div id="header-index">

    <div class="container">
        <div style="width: 100%">
            <div class="row">

                <div class="col-lg-4 col-md-10" style="padding-top: 32px">
                    <?= Html::a(Html::img('/img/logo.png',['class' => "img-responsive"]),['/']) ?>
                </div>
                <div id="nav-toggle" class="col-lg-4 col-md-2">
                    <i  class="fa fa-bars" style="color:black; font-size: 40px"></i>
                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="top-header" class="pull-right">

                                <?= Html::a(Html::img('/img/viber.png')) ?>
                                <?= Html::a(Html::img('/img/telegram.png')) ?>
                                <span><?=Yii::$app->params['settings']['contact_phone_viber']['value']?></span>
                                <span id="top-head-phone"> <?= Html::img('/img/phone.png'); ?> <?=Yii::$app->params['settings']['contact_phone_main']['value']?></span>
                                <?= Html::a(Html::img('/img/delemiter.png')) ?>
                                <?= Html::a('<i class="fa fa-instagram" style="color:black; font-size: 1.2em"></i>') ?>
                                <?= Html::a('<i class="fa fa-vk" style="color:black; font-size: 1.2em"></i>') ?>
                                <?= Html::a(Html::img('/img/delemiter.png')) ?>
                                <span style="display: inline-block">
                                    <?= Html::a(Html::img('/img/login.png') . " <span class='login-partner'>вход&nbspдля&nbspпартнеров</span>", ["/admin/profile"]) ?>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="top-menu" class="pull-right">
                                <?= TopMenu::widget() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="we-apartamio">Мы апартамио</div>
                <h1>Квартиры студии от <span class="r-brown">2 млн.руб.</span><br>в Москве и Подмосковье</h1>
                <p>Мы выкупаем большие квартиры и превращаем их в несколько <br> современных студий с чистовой отделкой и заменой коммуникаций</p>
                <?=Html::a('Получить консультацию',[''],['class'=>'get-consultation'])?>
            </div>
        </div>
        <div style="height: 261px">&nbsp;</div>
    </div>
</div>

<script>
    $('#nav-toggle').on('click', function(){
        console.log('test');
        $('#top-menu').toggle(
            function () {
                $('#top-menu').addClass('pull-right');
                console.log('test hide');
            },
            function () {
                $('#top-menu').removeClass('pull-right');
                console.log('test show');
            },
        );
    });
</script>