<?php


use yii\helpers\Html;
use app\models\SubscriptionForm;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

$subscriptionForm = new SubscriptionForm();

?>
<div class="footer">
    <div class="container">
        <div>
            <div class="row">
                <div class="col-md-4"><?=Html::a(Html::img('/img/logo-v-i.png', ['class' => 'imgCenter']),'/'); ?></div>
                <div class="col-md-8"></div>
            </div>
            <div class="row">
                <div class="col-md-4">

                    <h4>Компания Апартамио существует с 2018 года.</h4>

                    <p>Мы предлагаем уникальный продукт – возможность купить жилье по цене от 1 900 000 рублей в Москве
                        и
                        ближайшем Подмосковье (до 10 км от МКАД</p>
                    <p>Идея заключается в том, что мы подбираем объекты недвижимости, которые можно купить совместно и
                        разделить на отдельные квартиры-студии. Таким образом, каждая доля объекта становиться
                        отдельной</p>
                    <br>
                    <div class="object-details r-blue"><?= Html::img('/img/detail-ico.png') ?>
                        &nbsp;&nbsp; <?=Html::a('Читать далее',['/site/about'])?>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row">
                <div class="col-md-4 footer-nav">
                    <h3 class="center-block"><strong>Навигация</strong></h3>
                    <?= Html::img('/img/image-znak.png'); ?>
                    <ul>
                        <li><?= Html::a('Главная', ['/']) ?></li>
                        <li><?= Html::a('Наши объекты', ['/']) ?></li>
                        <li><?= Html::a('Проект', ['/']) ?></li>
                        <li><?= Html::a('Статьи', ['/']) ?></li>
                        <li><?= Html::a('О компании', ['/']) ?></li>
                        <li><?= Html::a('Контакты', ['/']) ?></li>
                        <li><?= Html::a('FAQ', ['/']) ?></li>
                        <li><?= Html::a('Для инвесторов', ['/']) ?></li>
                    </ul>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6 footer-contact">
                            <h3 class="center-block"><strong>Контакты</strong></h3>
                            <?= Html::img('/img/image-znak.png'); ?>
                            <strong>
                                <div>
                                    <?= Html::img('/img/footer-ico2.png'); ?>
                                    <?=Yii::$app->params['settings']['contact_phone_viber']['value']?>
                                    <?= Html::img('/img/footer-ico3.png'); ?>
                                    <?= Html::img('/img/footer-ico4.png'); ?>
                                </div>
                                <div>
                                    <?= Html::img('/img/footer-ico2.png'); ?>
                                    <?=Yii::$app->params['settings']['contact_phone_main']['value']?>
                                </div>
                                <div>
                                    <?= Html::img('/img/footer-ico1.png'); ?>
                                    &nbsp;<?=Yii::$app->params['settings']['contact_email']['value']?>
                                </div>
                                <div>
                                    <?= Html::img('/img/footer-ico5.png'); ?>
                                    &nbsp;вход для партнеров
                                </div>

                            </strong>
                        </div>
                        <div class="col-md-6 footer-subscribe">
                            <h3 class="center-block"><strong>Подписаться</strong></h3>
                            <?= Html::img('/img/image-znak.png'); ?>
                            <?php $form = \kartik\form\ActiveForm::begin([
                                'method' => 'post',
                                'action' => Url::to(['site/subscrition']),
                            ]); ?>
                            <div class="col-md-12" style="text-align: center">
                                <div class='has-feedback'>
                                    <?= $form->field($subscriptionForm, 'email')
                                        ->widget(MaskedInput::className(),
                                            [

                                                'class' => 'form-control tel_input get-catalog-input',
                                                'clientOptions' => [
                                                    'clearIncomplete' => true,
                                                    'alias' =>  'email',
                                                ],
                                                'options' => [
                                                    'class' => 'form-control tel_input get-catalog-input',
                                                    'placeholder' => 'E-mail',
                                                ],
                                            ])->label(false);
                                    ?>
                                    <span style='font-size: 20px; top:8px'
                                          class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                            </div>
                            <div class="col-md-12" style="text-align: center">
                                <?= Html::submitButton('ПОДПИСАТЬСЯ НА НОВОСТИ', ['class' => 'get-catalog-submit']) ?>
                            </div>
                            <div class="col-md-12 footer-condent" >
                                Ваши данные не будут переданы третьим лицам
                            </div>
                            <?php $form::end() ?>
                        </div>
                    </div>

                </div>
                    </div>
                </div>
                <div class="col-md-3">

                </div>
            </div>
            <div class="row" style="padding-top: 80px">
                <span class="copyrigth">
                    2019 C Все права защищены "АПАРТАМИО"
                </span>
                <div class="footer-soc pull-right">
                    Мы в соцсетях:
                    <i class="fa fa-facebook r-brown" ></i>&nbsp;
                    <i class="glyphicon glyphicon-send r-brown"></i>&nbsp;
                    <?= Html::img('/img/ok-ico-i.png'); ?>
                </div>
            </div>
        </div>
    </div>
</div>