<?php

use app\models\Instagram;
use app\models\Myobject;
use yii\helpers\Html;

?>
<div id="site-block-work">
    <div class="container">
        <div class="site-objects">
            <div id="object-instagram" class="head1">
                <span class="r-blue"> По </span> <span class="r-brown"> Работам Делаем </span> <br> <span class="r-blue"> Следующее:</span>
                <br>
                <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                <div class="row">
                    <div class="col-lg-6" style="padding-right: 20px "  >
                        <?=Html::img('/img/work-image.png',['style'=>'max-width:100%']);?>
                    </div>
                    <div class="col-lg-6" style="padding-left: 20px ">
                        <div class="work-item">
                            <?=Html::img('img/work1.png');?>
                            <p>Узаканиваем перепланировку помещения</p>
                        </div>
                        <div class="work-item">
                            <?=Html::img('img/work2.png');?>
                            <p>Делаем ремонт – в каждой студии строим сан-узел, по размерам – зависит от заказчика </p>
                        </div>
                        <div class="work-item">
                            <?=Html::img('img/work3.png');?>
                            <p>Делаем разводку воды (горячей и холодной) </p>
                        </div>
                        <div class="work-item">
                            <?=Html::img('img/work4.png');?>
                            <p>Канализацию подключаем через подвал в каждой студии, также делаем закладку канализационных труб для подключения соло-лифтов (резервная схема) </p>
                        </div>
                        <div class="work-item">
                            <?=Html::img('img/work5.png');?>
                            <p>Делаем разводку вентиляции </p>
                        </div>
                        <div class="work-item">
                            <?=Html::img('img/work6.png');?>
                            <p>В квартиру заводим новый силовой кабель (при необходимости), тянем силовой кабель до каждой из студий </p>
                        </div>
                        <div class="work-item">
                            <?=Html::img('img/work7.png');?>
                            <p>При необходимости делаем ремонт под ключ по цене 12 000 руб./кв.м. – со всеми материалами и доставкой </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>