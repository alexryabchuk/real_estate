<?php

use app\models\Instagram;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\MaskedInput;

$getCatalogForm = new \app\models\GetCatalogForm();
?>
<div id="catalog-fon">
    <div class="container">
        <div class="site-objects">
            <div id="object-call" class="pull-right">
                <div class="col-md-12">
                <h3><strong>Получите актуальный каталог свободных студий с вариантами отделки и персональнй подбор по
                        заданным критериям</strong></h3>
                <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                <br>
                </div>
                <?php $form = \kartik\form\ActiveForm::begin([
                    'method' => 'post',
                    'action' => Url::to(['site/get-catalog']),
                ]); ?>
                <div class="col-md-12" style="text-align: center">
                    <div class='has-feedback'>
                        <?= $form->field($getCatalogForm, 'phone')
                            ->widget(MaskedInput::className(),
                                [
                                    'mask' => '+7(999)999-99-99',
                                    'class' => 'form-control tel_input get-catalog-input',
                                    'clientOptions' => [
                                        'clearIncomplete' => true
                                    ],
                                    'options' => [
                                        'class' => 'form-control tel_input get-catalog-input',
                                        'placeholder' => 'Номер телефона',
                                    ],
                                ])->label(false);
                        ?>
                        <span style='font-size: 20px; top:8px'
                              class="glyphicon glyphicon-phone form-control-feedback"></span>
                    </div>
                </div>
                <div class="col-md-12">
                    <?= $form->field($getCatalogForm, 'captcha')->widget(\yii\captcha\Captcha::classname(), [
                        'options' => [
                            'class' => 'form-control tel_input get-catalog-input',

                        ],
                    ]) ?>
                </div>
                <div class="col-md-12">
                    <div class='iagree'>
                        <?= $form->field($getCatalogForm, 'agree')->checkbox(['class' => 'get-catalog-checkbox'])->label('<span>Я ознакомился с <a href=\'#\'> условиями обратотки данных и политикой конфиденциальности</a></span>') ?>
                    </div>
                </div>

                <div class="col-md-12" style="text-align: center">
                    <?= Html::submitButton('ПОЛУЧИТЬ КАТАЛОГ И КОНСУЛЬТАЦИЮ', ['class' => 'get-catalog-submit']) ?>
                </div>
                <?php $form::end() ?>

            </div>
        </div>
    </div>
</div>