<?php

use app\models\Article;
use app\models\Instagram;
use app\models\Myobject;
use yii\helpers\Html;

?>
<div id="site-block-news">
    <div class="container">
        <div class="site-objects">
            <div id="object-instagram" class="head1">
                <div class="row">
                    <div class="col-lg-6">
                        <span class="r-blue"> Последние </span> <span class="r-brown"> Новости</span> <br>
                        <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                    </div>
                    <div class="col-lg-6 pull-right">
                        <div class="post-nav pull-right">
                            <?= Html::img('/img/detail-ico.png'); ?>
                            <?=Html::a('  Все новости',['/site/articles'],['class'=>'post-nav-all'])?>
                            <a href="#" class="post-nav-ico"> <i class="fa fa-long-arrow-left"></i></a>
                            <a href="#" class="post-nav-ico"> <i class="fa fa-long-arrow-right"></i></a>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <?php
                    $objects = Article::find()->where(['deleted'=>0])->orderBy('id DESC')->limit(3)->all();
                    foreach ($objects as $article) { ?>
                        <div class="col-lg-4">
                            <div class="article-item">
                                <?= Html::a(Html::img($article->photo, ['class' => 'img-responsive img-rounded']),['/article-full','id'=>$article->id])?>
                                <div class="object-list-item">
                                    <div class="article-date"><?= date('M d, Y', $article['created_at']) ?></div>
                                    <div class="article-name">
                                        <?=Html::a($article->name,['/article-full','id'=>$article->id],['class'=>'object-detail'])?>
                                    </div>
                                    <div class="article-short_description"><?php
                                        $string = strip_tags($article->short_description);
                                        $string = substr($string, 0, 20);
                                        $string = rtrim($string, "!,.-");
                                        $string = substr($string, 0, strrpos($string, ' '));
                                        echo $string . "… ";
                                        ?></div>
                                    <div class="clearfix"></div>
                                    <div class="object-details r-blue"><?= Html::img('/img/detail-ico.png') ?>
                                            <?=Html::a('Подробнее',['/article-full','id'=>$article->id],['class'=>'object-detail'])?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php }?>
                </div>

            </div>
        </div>
    </div>