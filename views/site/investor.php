<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use kartik\slider\Slider;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html; ?>
<div class="site-about">
    <div class="container">
        <div class="site-objects" style="padding-top: 20px">
            <div id="object-breadcrumbs">
                <?= Html::a('Главная ', ['/']) ?>/<?= Html::a(' Статьи', ['/articles']) ?>
            </div>
            <div id="object-head" class="head1">
                <span class="r-white">Для</span><span class="r-brown"> инвестора</span><br>
                <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <h3>
                        Компания АПАРТАМИО сущeствует с 2018 года
                    </h3>
                    <p>
                        Мы предлагаем уникальный продукт – возможность купить жилье по цене от 1 900 000 рублей в Москве
                        и ближайшем Подмосковье (до 10 км от МКАД).
                    </p>
                    <p>
                        Идея заключается в том, что мы подбираем объекты недвижимости, которые можно купить совместно и
                        разделить на отдельные квартиры-студии. Таким образом, каждая доля объекта становится отдельной
                        квартирой-студией. В каждой квартире есть СВОЙ санузел с ванной или душевой кабиной, кухонный
                        блок и спальная зона. В некоторых есть кабинет и кладовая.
                    </p>
                    <p>
                        Благодаря тому, что мы покупаем один большой объект и делим его на несколько маленьких, каждый
                        из участников долевой сделки имеет возможность купить квартиру по цене в 2-3 раза ниже обычной
                        стоимости квартиры в этом же районе.
                    </p>
                    <p>
                        Мы подбираем районы с развитой инфраструктурой, хорошей транспортной доступностью и
                        благоприятной экологической обстановкой. Также мы проверяем юридическую чистоту объектов. Таким
                        образом с нашей помощью квартиры приобрели уже сотни довольных клиентов.
                    </p>
                    <p>
                        <?= Html::img('/img/investor-but1.png') ?>&nbsp;
                        <?= Html::img('/img/investor-but2.png') ?>
                    </p>
                </div>
                <div class="col-lg-6">
                    <?= Html::img('/img/about1-i.png', ['class' => "about-img img-responsive"]); ?>
                </div>
            </div>
            <div id="object-head" class="head1">
                <span class="r-white">Наши</span><span class="r-brown"> преимущества</span><br>
                <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
            </div>
            <div class="row">
                <div class="col-lg-3 ">
                    <div class="investor-item">
                        <?= Html::img('/img/investor-item1.png') ?>
                        <div class="investor-item-h">В каждой студии строим</div>
                        <div class="investor-item-p">Делаем ремонт - в каждой студии строим сан-узел, по размерам -
                            зависит от заказчика
                        </div>
                    </div>

                </div>
                <div class="col-lg-3">
                    <div class="investor-item">
                        <?= Html::img('/img/investor-item2.png') ?>
                        <div class="investor-item-h">В каждой студии строим</div>
                        <div class="investor-item-p">Делаем ремонт - в каждой студии строим сан-узел, по размерам -
                            зависит от заказчика
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="investor-item">
                        <?= Html::img('/img/investor-item3.png') ?>
                        <div class="investor-item-h">В каждой студии строим</div>
                        <div class="investor-item-p">Делаем ремонт - в каждой студии строим сан-узел, по размерам -
                            зависит от заказчика
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="investor-item">
                        <?= Html::img('/img/investor-item4.png') ?>
                        <div class="investor-item-h">В каждой студии строим</div>
                        <div class="investor-item-p">Делаем ремонт - в каждой студии строим сан-узел, по размерам -
                            зависит от заказчика
                        </div>
                    </div>
                </div>
            </div>
            <div id="object-head" class="head1">
                <span class="r-white">Наши квартиры-студии</span><br>
                <span class="r-brown"> ефективно вигоднее</span><br>
                <span class="r-white">альтернативных вариантов</span><br>
                <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
            </div>
            <div class="row">
                <table class="table-investor" width="100%">
                    <tr>
                        <th></th>
                        <th>Квартира <br>в комуналке</th>
                        <th>Студия <br>на окраине</th>
                        <th>Апарт-отель</th>
                        <th>Наши <br>квартиры-студии</th>
                    </tr>
                    <tr>
                        <td>Средняя <br>стоимость (20 м<sup>2</sup>)</td>
                        <td>2 600 000&#8381</td>
                        <td>2 800 000&#8381</td>
                        <td>4 000 000&#8381</td>
                        <td>3 500 000&#8381</td>
                    </tr>
                    <tr>
                        <td>Средняя доходность <br>при сдаче в аренду</td>
                        <td>8%</td>
                        <td>10%</td>
                        <td>10%</td>
                        <td>17%</td>
                    </tr>
                    <tr>
                        <td>Ежемесячний доход</td>
                        <td>16 000&#8381</td>
                        <td>23 000&#8381</td>
                        <td>23 000&#8381</td>
                        <td>45 000&#8381</td>
                    </tr>
                    <tr>
                        <td>Возможность <br>сдачи через УК</td>
                        <td>нет</td>
                        <td>нет</td>
                        <td>да</td>
                        <td>да</td>
                    </tr>
                    <tr>
                        <td>Рост стоимости на етапе<br> "от застройки до ключей"</td>
                        <td>-</td>
                        <td>10%</td>
                        <td>10%</td>
                        <td>15%</td>
                    </tr>
                    <tr>
                        <td>Срок етапа "от застройки <br>до ключей"</td>
                        <td>-</td>
                        <td>1,5 года</td>
                        <td>1,5 года</td>
                        <td>3 месяца</td>
                    </tr>
                </table>
            </div>
            <div id="object-head" class="head1">
                <span class="r-white">Калькулятор </span>
                <span class="r-brown"> доходов</span><br>
                <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
            </div>
            <div class="row">
                <div class="investor-calc col-lg-12">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label class="control-label" for="client-firstname">Сумма инвестиций</label>
                                <input type="text" id="calc-summa" class="form-control" name="calc[fsumma]"
                                       maxlength="255">
                            </div>
                        </div>
                        <div class="col-lg-9" style ="position: relative"><b class="badge1">0</b>
                            <?= Slider::widget([
                                'name' => 'slider1',
                                'id'=> 'slider1',
                                'value' => 0,
                                'pluginOptions' => [
                                    'min' => 0,
                                    'max' => 25000000,
                                    'step' => 1000
                                ],
                                'pluginEvents' => [
                                    "slide" => 'function() { 
                                    var item = $("#slider1").val();
                                    $("#calc-summa").val(item);
                                    console.log(item); }',
                                ],
                            ]); ?>
                            <b class="badge2">25 000 000</b>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label class="control-label" for="client-firstname">Желаемая прибиль</label>
                                <input type="text" id="calc-dohod" class="form-control" name="calc[fsumma]"
                                       maxlength="255">
                            </div>
                        </div>
                        <div class="col-lg-9" style ="position: relative"><b class="badge1">0</b>
                            <?= Slider::widget([
                                'name' => 'slider2',
                                'id'=> 'slider2',
                                'value' => 0,
                                'pluginOptions' => [
                                    'min' => 0,
                                    'max' => 5000000,
                                    'step' => 1000
                                ],
                                'pluginEvents' => [
                                    "slide" => 'function() { 
                                    var item = $("#slider2").val();
                                    $("#calc-dohod").val(item);
                                    console.log(item); }',
                                ],
                            ]); ?>
                            <b class="badge2">25 000 000</b>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">

                                <button type="button" id="calculate" class="form-control"> ВыПОЛНИТЬ РАСЧЕТ </button>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row calc-result">
                        <br>
                        <div class="col-lg12 " style="font-size: 2em; font-weight: bold">Результаты расчета:</div>
                        <br>
                        <div class="col-lg12" id="calc-res1">Сумма инвестиционная =</div>
                        <div class="col-lg12" id="calc-res2">Сумма затрат на ремонт =</div>
                        <br>
                        <div class="col-lg12" id="calc-res3">Прибыльность проекта без затрат на ремонт =</div>
                        <div class="col-lg12" id="calc-res4">Прибыльность проекта с ремонтом =</div>
                        <div class="col-lg12" id="calc-res5">Срок инвестиций =</div>
                        <br>
                        <div class="col-lg12" id="calc-res6">Средняя цена кв.м. по району =</div>
                        <br>
                        <div class="col-lg12 investor-form">Результаты расчета:</div>
                    </div>

                    <br><br><br>
                        <?php $form = ActiveForm::begin() ?>
                        <div class="row calc-form">
                            <div class="col-md-3 has-feedback">
                                <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя'])->label(false) ?>
                                <?= Html::img('/img/contact-user-ico.png', ['class' => 'investor-form-ico']); ?>
                            </div>
                            <div class="col-md-3 has-feedback">
                                <?= $form->field($model, 'email')->textInput(['placeholder' => 'E-mail'])->label(false) ?>
                                <?= Html::img('/img/contact-email-ico.png', ['class' => 'contact-form-ico']); ?>
                            </div>
                            <div class="col-md-3" style="text-align: center">
                                <?= Html::submitButton('ХОЧУ УЗНАТЬ ПОДРОБНЕЕ', ['class' => 'calc-form-submit']) ?>
                            </div>
                            <div class="col-md-9" style="text-align: center">
                                <div class='iagree' style="padding: 0px">
                                    <?= $form->field($model, 'agree')->checkbox(['class' => 'get-calc-checkbox'])->label('<span>Я ознакомился с <a href=\'#\'> условиями обратотки данных и политикой конфиденциальности</a></span>') ?>
                                </div>
                            </div>
                        </div>
                        <?php $form::end() ?>

                </div>
            </div>
        </div>
    </div>
    <br><br><br><br><br><br>
    <?= $this->render('_map'); ?>
    <?= $this->render('_catalog'); ?>
    <?= $this->render('_instagram-dark'); ?>
</div>

