<?php

use app\models\Instagram;
use app\models\Myobject;
use yii\helpers\Html;

?>
<div id="fon1">
    <div class="container">
        <div class="site-objects">
            <div id="object-instagram" class="head1">

                <div class="row">
                    <div class="col-lg-6">
                        <span class="r-blue"> Реализованные </span> <span class="r-brown"> Проекты</span>
                        <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                    </div>
                    <div class="col-lg-6 pull-right">
                        <div class="post-nav pull-right">
                            <?= Html::img('/img/detail-ico.png'); ?>
                            <?=Html::a('  Все проекты',['/site/projects'],['class'=>'post-nav-all'])?>
                            <a href="#" class="post-nav-ico"> <i class="fa fa-long-arrow-left"></i></a>
                            <a href="#" class="post-nav-ico"> <i class="fa fa-long-arrow-right"></i></a>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <?php
                    $objects = Myobject::find()->where(['object_type' => 1])->limit(3)->all();
                    foreach ($objects

                    as $object) { ?>
                    <div class="col-lg-4 col-md-4">
                        <div class="object-item">
                            <?= Html::img($object->image, ['class' => 'img-responsive img-rounded']) ?>
                            <div class="object-list-item">
                                <div class="object-metro r-blue">
                                    Метро: <?= $object->metro ?></div>
                                <div class="object-short_description"><?= $object->short_description ?></div>
                                <div class="object-details r-blue"><?= Html::img('/img/detail-ico.png') ?>
                                    <?=Html::a('Подробнее',['/projects-full','id'=>$object->id],['class'=>'object-detail'])?>
                                </div>
                            </div>


                        <?php echo '</div></div>';
                        }
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>