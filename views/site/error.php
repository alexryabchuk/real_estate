<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">
    <div class="container">
        <div class="site-objects">
            <div class="row" style="margin-bottom: 40px ">
                <div class="col-lg-6">
                    <div id="object-breadcrumbs">
                        <?= Html::a('Главная ', ['/']) ?> / <?= Html::a(' 404', ['/error']) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 error_404">404</div>
                <div class="col-md-6">
                    <p><span>К СОЖАЛЕНИЮ...</span> СТРАНИЦА НЕ НАЙДЕНА.</p>
                    <p>Страница которую ви ищите, не существует,<br> возможно она была перемещена или удалена</p>
                    <a href="/" class="site-error-back">Вернутся на главную</a>
                </div>
            </div>
        </div>
    </div>
</div>
