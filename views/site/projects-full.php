<?php

/* @var $this yii\web\View */


use app\models\Article;
use app\models\Studio;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\MaskedInput;

$this->title = 'Наши объекты';


\xj\galleria\GalleriaAssets::register($this);


\xj\galleria\themes\DefaultAssets::register($this);
xj\galleria\Galleria::widget([
    'selector' => '#galleria',
]);
?>
    <style>
        #galleria {
            width: auto;
            height: 600px;
            background: #FFFFFF;
            margin-top: 30px;
        }
    </style>
    <div id="project-full">
        <div class="container">
            <div class="site-objects">

                <div id="object-breadcrumbs">
                    <?= Html::a('Главная ', ['/']) ?> / <?= Html::a('  Реализованные проекты', ['/projects']) ?>
                    / <?= Html::a('Метро '.$object->metro.', '.$object->address, ['/project-full','id'=>$object->id]) ?>
                </div>
                <div class="row object-block">
                    <div class="col-lg-8 object-full-block">
                        <div id="galleria">
                            <?php foreach ($object_photo as $photo) : ?>
                                <?= Html::img($photo->filename) ?>
                            <?php endforeach; ?>
                        </div>
                        <div class="object-list-item" style="width: 100%">
                            <div class="object-full-metro"> Метро: <?= $object->metro ?></div>
                            <div class="object-full-etazh"> <?= Html::img('/img/object-etazh-icol.png') ?>  Этаж: 3/5
                            </div>
                            <div class="object-full-studio"> <?= Html::img('/img/object-studio-ico.png') ?> Студий: 5
                            </div>
                            <div class="object-full-address"> <?= Html::img('/img/address-ico.png') ?>  <?= $object->address ?> </div>
                            <div class="object-full-short_description"><?= $object->short_description ?></div>
                            <div class="row object-full-studios">
                                <?php foreach ($studio as $s): ?>
                                    <div class="col-md-4">
                                        <h4><?= $s->name ?></h4>
                                        <ul>
                                            <li><strong>Площадь: </strong><?= $s->square ?></li>
                                            <li><strong>Балкон: </strong><?= $s->balcon == 0 ? 'Нет' : 'Да' ?></li>
                                            <li><strong>Статус: </strong><?= Studio::getStatuses()[$s->status] ?></li>
                                            <li><strong>Цена: </strong><span
                                                        class="r-brown"> от <?= $s->price ?> руб </span></li>
                                        </ul>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="object-full-description"><?= $object->description1 ?> </div>
                            <div id="object-full-map">
                            </div>
                        </div>
                        <div class="object-full-form">
                            <h3>
                                Оставьте заявку, чтобы получить специальное предложение
                            </h3>
                            <?php $form = ActiveForm::begin() ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class='has-feedback'>
                                        <?= $form->field($model, 'phone')
                                            ->widget(MaskedInput::className(),
                                                [
                                                    'mask' => '+7(999)999-99-99',
                                                    'class' => 'form-control tel_input get-catalog-input',
                                                    'clientOptions' => [
                                                        'clearIncomplete' => true
                                                    ],
                                                    'options' => [
                                                        'class' => 'form-control tel_input get-catalog-input',
                                                        'placeholder' => 'Номер телефона',
                                                    ],
                                                ])->label(false);
                                        ?>
                                        <span style='font-size: 20px; top:8px'
                                              class="glyphicon glyphicon-phone form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-6" style="text-align: center">
                                    <?= Html::submitButton('ЗАПИСАТЬСЯ НА ПРОСМОТР', ['class' => 'get-catalog-submit']) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::classname(), [
                                        'template' => '<div class="col-xs-6" style="padding-left: 0px">{input}</div><div class="col-xs-6">{image}</div>',
                                        'options' => ['placeholder' => 'CAPTCHA Code', 'class' => 'form-control']
                                    ])->label(false) ?>
                                </div>

                                <div class="col-md-12">
                                    <div class='iagree'>
                                        <?= $form->field($model, 'agree')->checkbox(['class' => 'get-catalog-checkbox'])->label('<span>Я ознакомился с <a href=\'#\'> условиями обратотки данных и политикой конфиденциальности</a></span>') ?>
                                    </div>
                                </div>

                            </div>
                            <?php $form::end() ?>
                        </div>

                    </div>
                    <div class="col-lg-4 article-right">
                        <div class="search-article">
                            <?= Html::beginForm(['/site/email1'], 'post') . "<div class='has-feedback'>"
                            . Html::input('', 'email3', '', ['placeholder' => 'Поиск ...']) . "<span style='font-size: 20px;color: #a58f73' class=\"glyphicon glyphicon-search form-control-feedback2\"></span><br>"
                            . "</div>"
                            . Html::endForm() ?>
                        </div>
                        <div class="article-category">
                            <h3 class="center-block"><strong>Категории</strong></h3>
                            <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                            <?php
                            foreach (Article::getCategoryWithCount() as $article) {
                                echo '<div class="article-category-item">';
                                echo Html::a($article["name"], ['/article-category', 'id' => $article['category_id']]);
                                echo "<div class='pull-right'>(" . $article['category_count'] . ")</div>";
                                echo "</div>";
                            }
                            ?>
                        </div>
                        <div class="article-new">
                            <h3 class="center-block"><strong>Свежие новости</strong></h3>
                            <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                            <?php
                            foreach (Article::getLastArticle() as $article) {
                                echo '<div class="article-new-item">';
                                echo Html::a($article["name"], ['/article', 'id' => $article['id']]);
                                echo "<div>" . date('M d, Y', $article['created_at']) . "</div>";
                                echo "</div>";
                            }
                            ?>
                        </div>
                        <div class="article-tags">
                            <h3 class="center-block"><strong>Популярние теги</strong></h3>
                            <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
<?= $this->render('_project'); ?>
<?= $this->render('_catalog'); ?>
<?= $this->render('_instagram'); ?>

    <style>
        #object-full-map {
            width: 100%;
            height: 400px;
            padding: 0;
            margin: 0;
        }
    </style>
    <style>
        .YMaps-layer-container img {
            max-width: none;
        }
    </style>
<?php
$coordX = $object->coord_x;
$coordY = $object->coord_y;
$script = <<<JS
        ymaps.ready(function () {
        var myMap = new ymaps.Map('object-full-map', {
center: [$coordX,$coordY],
zoom: 16,
controls: ['smallMapDefaultSet']
});
var myPlacemark = new ymaps.Placemark([$coordX,$coordY], {
    balloonContentHeader: "Балун метки",
    balloonContentBody: "Содержимое <em>балуна</em> метки",
    balloonContentFooter: "Подвал",
    hintContent: "Хинт метки"
    },{
            iconLayout: 'default#image',
            iconImageHref: '/img/map-ico.png',
            iconImageSize: [30, 42],
            iconImageOffset: [-5, -38]
        });
myMap.geoObjects.add(myPlacemark);
myMap.behaviors.disable('scrollZoom');
});
JS;
$this->registerJsFile("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=&lt;ваш API-ключ&gt;", ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJs($script, yii\web\View::POS_READY);
?>