<?php

use app\models\Myobject;
use yii\helpers\Html;

?>
<div id="map-fon">
    <div class="container">
        <div class="site-objects">
            <div id="object-map" class="head1">
                <span class="r-blue">Карта </span> <span class="r-brown"> Объектов</span>
                <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                <div id="map">

                </div>
            </div>
        </div>
    </div>
</div>

    <style>
        #map {
            width: 100%;
            height: 770px;
            padding: 0;
            margin: 0;
        }
    </style>
    <style>
        .YMaps-layer-container img {
            max-width: none;
        }
    </style>
<?php
$myObject = Myobject::getAllGeoObject();
$countObject = count($myObject);
$coordX = $myObject[0]['coord_x'];
$coordY = $myObject[0]['coord_y'];
$script = <<<JS
var myMap = null;
var objectCollection = null;
function addPlacemark (arr) {
    
    for (step=0;step<arr.length;step++) {
        var myPlacemark = new ymaps.Placemark([arr[step].coord_x,arr[step].coord_y], {
            hintContent: 'asdasdasd',
        },{
            iconLayout: 'default#image',
            iconImageHref: '/img/map-ico.png',
            iconImageSize: [30, 42],
            
        });
        objectCollection.add(myPlacemark);
        myMap.geoObjects.add(objectCollection);
    }
}       
ymaps.ready(function () {
    objectCollection = new ymaps.GeoObjectCollection();
    myMap = new ymaps.Map('map', {
        center: [$coordX,$coordY],
        zoom: 11,
    });
    $.get("/site/get-geo-object",{type:0},function (data) {
        addPlacemark(data);
    })
    myMap.behaviors.disable('scrollZoom');
    myMap.behaviors.disable('dblClickZoom');
});
JS;
$this->registerJsFile("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=7c4fab10-b262-4dc9-b2d0-968fd789cb4a;", ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJs($script, yii\web\View::POS_READY);
?>

