<?php

/* @var $this yii\web\View */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use app\widgets\Alert;

$this->title = 'О компании';

?>

    <div class="site-contact">
        <div class="container">
            <div class="site-objects">
                <div class="row">
                        <div id="object-breadcrumbs">
                            <?= Html::a('Главная ', ['/']) ?> / <?= Html::a(' Контакты', ['/objects']) ?>
                        </div>
                </div>
                <div class="row">
                    <div class="col-lg-6" style="padding: 0px">
                        <div id="object-head" class="head1">
                            <span class="r-blue"
                                  style="font-size: 3.33em; font-weight: bolder; text-align: left; padding-bottom: 0px; padding-top: 0px">Контакты</span>
                            <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                        </div>
                        <h4 class="contact-h4">
                            Иванов максим
                        </h4>
                        <div class="contact-brown">
                            Генеральний директор "АПАРТАМИО"
                        </div>
                        <p>
                            Идея заключается в том, что мы подбираем объекты недвижимости, которые можно купить
                            совместно и разделить на отдельные квартиры-студии. Таким образом, каждая доля объекта
                            становится отдельной квартирой-студией. В каждой квартире есть СВОЙ санузел с ванной или
                            душевой кабиной, кухонный блок и спальная зона. В некоторых есть кабинет и кладовая.
                            Благодаря тому, что мы покупаем один большой объект и делим его на несколько маленьких,
                            каждый из участников долевой сделки имеет возможность купить квартиру по цене в 2-3 раза
                            ниже обычной стоимости квартиры в этом же районе.<br>

                            Мы подбираем районы с развитой инфраструктурой, хорошей транспортной доступностью и
                            благоприятной экологической обстановкой. Также мы проверяем юридическую чистоту объектов.
                            Таким образом с нашей помощью квартиры приобрели уже сотни довольных клиентов.
                        </p>
                        <p></p>
                        <div class="contact-info">
                            <div class="row">
                                <div class="col-md-6">
                                    <i class="glyphicon glyphicon-earphone" style="font-size: 17px;color:rgb(165, 143, 115)"></i>
                                    <?= Yii::$app->params['settings']['contact_phone_viber']['value'] ?>
                                    <?= Html::img('/img/footer-ico3.png'); ?>
                                    <i class="glyphicon glyphicon-send" style="font-size: 17px"></i>
                                </div>
                                <div class="col-md-6">
                                    <i class="glyphicon glyphicon-envelope" style="font-size: 17px;color:rgb(165, 143, 115)"></i>
                                    &nbsp;<?= Yii::$app->params['settings']['contact_email']['value'] ?>
                                </div>
                            </div>
                            <div class="row" style="padding-top: 10px">
                                <div class="col-md-6">
                                    <i class="glyphicon glyphicon-earphone" style="font-size: 17px;color:rgb(165, 143, 115)"></i>
                                    <?= Yii::$app->params['settings']['contact_phone_main']['value'] ?>
                                </div>

                                <div class="col-md-6">
                                    <i class="fa fa-map-marker" style="font-size: 22px;color:rgb(165, 143, 115)"></i>
                                    <?= Yii::$app->params['settings']['contact_address']['value'] ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div id="contact-map">

                        </div>
                    </div>
                    <div class="col-lg-12 contact-form-fon">
                        <div class="contact-form">
                            <h3>
                                Остались вопросы
                            </h3>
                            <span>Напишите нам и мы ответим на ваши вопросы</span>
                            <?php $form = ActiveForm::begin() ?>
                            <div class="row">
                                <div class="col-md-6 has-feedback">
                                    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя'])->label(false) ?>
                                    <?= Html::img('/img/contact-user-ico.png', ['class' => 'contact-form-ico']); ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'email')->textInput(['placeholder' => 'E-mail'])->label(false) ?>
                                    <?= Html::img('/img/contact-email-ico.png', ['class' => 'contact-form-ico']); ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $form->field($model, 'body')->textarea(['placeholder' => 'Сообщение'])->label(false) ?>
                                    <?= Html::img('/img/contact-message-ico.png', ['class' => 'contact-form-ico']); ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::classname(), [
                                        // configure additional widget properties here
                                    ]) ?>
                                </div>
                                <div class="col-md-12">
                                    <div class='iagree' style="padding: 0px">
                                        <?= $form->field($model, 'agree')->checkbox(['class' => 'ge-catalog-checkbox'])->label('<span style="padding: 0px">Я ознакомился с <a href=\'#\'> условиями обратотки данных и политикой конфиденциальности</a></span>') ?>
                                    </div>
                                </div>
                                <div class="col-md-12" style="text-align: center">
                                    <?= Html::submitButton('ОТПРАВИТЬ СООБЩЕНИЕ', ['class' => 'contact-form-submit']) ?>
                                </div>
                            </div>
                            <?php $form::end() ?>
                        </div>
                    </div>
                    <div class="col-lg-12 ">

                    </div>
                </div>
            </div>

        </div>
    </div>
<?= $this->render('_catalog'); ?>
<?= $this->render('_instagram'); ?>
    </div>

    <style>
        #contact-map {
            width: 100%;
            height: 500px;
            padding: 0;
            margin: 0;
        }
    </style>
    <style>
        .YMaps-layer-container img {
            max-width: none;
        }
    </style>
<?php
$coord = Yii::$app->params['settings']['contact_coord']['value'];
$script = <<<JS
ymaps.ready(function () {
    var myMap = new ymaps.Map('contact-map', {
        center: [$coord],
        zoom: 16,
        controls: ['smallMapDefaultSet']
    });
    var myPlacemark = new ymaps.Placemark([$coord], {
        // Чтобы балун и хинт открывались на метке, необходимо задать ей определенные свойства.
        
        hintContent: "Компания Апартамио"
    },{
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: '/img/map-ico.png',
            // Размеры метки.
            iconImageSize: [30, 42],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-5, -38]
        });
    myMap.geoObjects.add(myPlacemark);
});
JS;
$this->registerJsFile("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=&lt;ваш API-ключ&gt;", ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJs($script, yii\web\View::POS_READY);
?>