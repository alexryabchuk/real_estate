<?php

/* @var $this yii\web\View */


use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'Реализованные проекты';
?>
    <div id="site-project">
        <div class="container">
            <div class="site-objects">

                <div id="object-breadcrumbs">
                    <?= Html::a('Главная ', ['/']) ?>/<?= Html::a(' Реализованные Проекты', ['/objects']) ?>
                </div>
                <div id="object-head" class="head1">
                    <span class="r-blue">Реализованные </span> <span class="r-brown"> Проекты</span><br>
                    <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                </div>
                <div id="#project-list">
                    <?php $count_object = count($objects);
                    $i = 0 ?>
                    <?php foreach ($objects as $object) : ?>
                        <?php $i++; ?>
                        <?php if (($i % 2) == 1) {
                            echo '<div class="row">';
                        } ?>
                        <div class="col-md-6 ">
                            <div class="object-item">
                                <?=Html::a(Html::img($object->image, ['class' => 'img-responsive img-rounded']),['/projects-full','id'=>$object->id])?>
                                <div class="object-list-item">
                                    <?=Html::a('Метро: '.$object->metro,['/projects-full','id'=>$object->id],['class'=>"object-metro"])?>
                                    <div class="object-short_description">
                                        <?php
                                        $string = strip_tags($object->short_description);
                                        $string = substr($string, 0, 160);
                                        $string = rtrim($string, "!,.-");
                                        $string = substr($string, 0, strrpos($string, ' '));
                                        echo $string . "… ";
                                    ?></div>
                                    <div class="clearfix"></div>
                                    <div class="object-details <?= ($i % 2) == 0 ? 'r-blue' : 'r-brown'; ?>"><?= Html::img('/img/detail-ico.png') ?>
                                        &nbsp;&nbsp; <?=Html::a('Подробнее',['/projects-full','id'=>$object->id],['class'=>'object-detail'])?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ((($i % 2) == 0) || ($i == $count_object)) {
                            echo "</div>";
                        } ?>
                    <?php endforeach; ?>

                </div>
                <div id="object-pagination" align="center" class="row" style="width: 100%; height: 100px">

                    <?= LinkPager::widget([
                        'pagination' => $pages,
                        'options' => [
                            'class' => 'pagination justify-content-center',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?=$this->render('_map');?>
<?=$this->render('_instagram');?>

