<?php

use app\models\Instagram;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\MaskedInput;

?>
<div class="contact-form">

    <h3>
        Оставить комментарий
    </h3>

    <?php $form = ActiveForm::begin() ?>
    <div class="row">
        <div class="col-md-6 has-feedback">
            <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя'])->label(false) ?>
            <?= Html::img('/img/contact-user-ico.png', ['class' => 'contact-form-ico']); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput(['placeholder' => 'E-mail'])->label(false) ?>
            <?= Html::img('/img/contact-email-ico.png', ['class' => 'contact-form-ico']); ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'body')->textarea(['placeholder' => 'Сообщение'])->label(false) ?>
            <?= Html::img('/img/contact-message-ico.png', ['class' => 'contact-form-ico']); ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::classname(), [
                'template' => '<div class="col-xs-6" style="padding-left: 0px">{input}</div><div class="col-xs-6">{image}</div>',
                'options' => ['placeholder' => 'CAPTCHA Code', 'class' => 'form-control']
            ])->label(false) ?>
        </div>
        <div class="col-md-12" style="text-align: center">
            <br>
            <?= Html::submitButton('ОТПРАВИТЬ', ['class' => 'contact-form-submit']) ?>
        </div>
    </div>
    <?php $form::end() ?>
</div>