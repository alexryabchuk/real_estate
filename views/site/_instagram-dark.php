<?php

use app\models\Instagram;
use yii\helpers\Html;

?>
<div id="site-instagram">
    <div class="container">
        <div class="site-objects">
            <div id="object-instagram" class="head1">
                <div class="row">
                    <div class="col-lg-6">
                        <span class="r-brown"> Instagram</span>
                        <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                    </div>
                    <div class="col-lg-6 pull-right">
                        <div class="post-nav pull-right">
                            <?= Html::img('/img/detail-ico.png'); ?>
                            <?=Html::a('  Все посты',['/'],['class'=>'post-nav-all'])?>
                                <a href="#" class="post-nav-ico"> <i class="fa fa-long-arrow-left"></i></a>
                                <a href="#" class="post-nav-ico"> <i class="fa fa-long-arrow-right"></i></a>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <?php
                    $d = new Instagram();
                    foreach ($d->getPost() as $post) {
                        echo '<div class="col-lg-3 col-md-4"><div class="object-item">';
                        echo Html::a(Html::img($post->images->standard_resolution->url,['class' => 'img-responsive img-rounded ']),$post->link);
                        echo Html::img('/img/inst-ico.png',['class'=>'instagram-ico']);
                        echo '</div></div>';
                    }
                    ?>
                </div>
            </div>

        </div>
    </div>
</div>