<?php

/* @var $this yii\web\View */


use app\models\Article;
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'Статьи';
?>
    <div id="fon1">
        <div class="container">
            <div class="site-objects">

                <div id="object-breadcrumbs">
                    <?= Html::a('Главная ', ['/']) ?>/<?= Html::a(' Статьи', ['/articles']) ?>
                </div>

                <div id="object-list">
                    <div class="col-md-9">
                    <?php $count_articles = count($articles);
                    $i = 0 ?>
                    <?php foreach ($articles as $article) : ?>
                        <?php $i++; ?>
                        <?php if (($i % 3) == 1) {
                            echo '<div class="row">';
                        } ?>
                        <div class="col-md-4 ">
                            <div class="article-item">
                                <?=Html::a(Html::img($article->photo, ['class' => 'img-responsive img-rounded']),['/article-full','id'=>$article->id]) ?>
                                <div class="object-list-item">
                                    <div class="article-date"><?=date('M d, Y',$article['created_at'])?></div>
                                    <div class="article-name"><?=Html::a($article->name,['/article-full','id'=>$article->id])?></div>
                                    <div class="article-short_description"><?= $article->short_description ?></div>
                                    <div class="clearfix"></div>
                                    <div class="object-details r-blue"><?= Html::img('/img/detail-ico.png') ?>
                                        &nbsp;&nbsp; <?=Html::a('Подробнее',['/article-full','id'=>$article->id],['class'=>'object-detail'])?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ((($i % 3) == 0) || ($i == $count_articles)) {
                            echo "</div>";
                        } ?>
                    <?php endforeach; ?>
                    </div>
                    <div class="col-md-3">
                        <div class="search-article">
                            <?= Html::beginForm(['/site/email1'], 'post') . "<div class='has-feedback'>"
                            . Html::input('', 'email3', '', ['placeholder' => 'Поиск ...']) . "<span style='font-size: 20px;color: #a58f73' class=\"glyphicon glyphicon-search form-control-feedback2\"></span><br>"
                            . "</div>"
                            . Html::endForm() ?>
                        </div>
                        <div class="article-category">
                            <h3 class="center-block"><strong>Категории</strong></h3>
                            <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                            <?php
                            foreach (Article::getCategoryWithCount() as $article) {
                                echo '<div class="article-category-item">';
                                echo Html::a($article["name"],['/articles','category'=>$article['category_id']]);
                                echo "<div class='pull-right'>(".$article['category_count'].")</div>";
                                echo "</div>";
                            }
                            ?>
                            </div>
                        <div class="article-new">
                            <h3 class="center-block"><strong>Свежие новости</strong></h3>
                            <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                            <?php
                            foreach (Article::getLastArticle() as $article) {
                                echo '<div class="article-new-item">';
                                echo Html::a($article["name"],['/article-full','id'=>$article['id']]);
                                echo "<div>".date('M d, Y',$article['created_at'])."</div>";
                                echo "</div>";
                            }
                            ?>
                        </div>
                        <div class="article-tags">
                            <h3 class="center-block"><strong>Популярние теги</strong></h3>
                            <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                            </div>
                    </div>

                </div>
                <div id="object-pagination" align="center" class="row" style="width: 100%; height: 100px">

                    <?= LinkPager::widget([
                        'pagination' => $pages,
                        'options' => [
                            'class' => 'pagination justify-content-center',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

<?=$this->render('_catalog');?>
<?=$this->render('_instagram');?>

