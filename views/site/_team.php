<?php

use yii\helpers\Html;

?>
    <div id="team-fon">
        <div class="container">
            <div class="site-objects">
                <div id="object-map" class="head1">
                    <span class="r-blue">Наша </span> <span class="r-brown"> Команда</span><br>
                    <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                    <div class="row" style="margin-top: 100px">
                        <div class="col-lg-3 col-md-6">
                            <div style="position: relative">
                                <?= Html::img('/img/command1.png', ['class' => 'img-responsive']) ?>
                                <?= Html::img('/img/i.png', ['class' => 'img-responsive team-info-ico', 'id' => 'team-info-ico-1']) ?>
                            </div>
                            <div id="team-info-1" class="team-info">
                                <div class="team-info-name">
                                    Иван Попов
                                </div>
                                <div class="team-info-work">
                                    Дизайнер
                                </div>
                                <div class="team-info-viver">
                                    <?= Html::img('/img/footer-ico2.png'); ?>
                                    <?= Yii::$app->params['settings']['contact_phone_viber']['value'] ?>
                                    <?= Html::img('/img/footer-ico3.png'); ?>
                                    <?= Html::img('/img/footer-ico4.png'); ?>
                                </div>
                                <div class="team-info-phone">
                                    <?= Html::img('/img/footer-ico2.png'); ?>
                                    <?= Yii::$app->params['settings']['contact_phone_main']['value'] ?>
                                </div>
                                <div class="team-info-email">
                                    <?= Html::img('/img/footer-ico1.png'); ?>
                                    &nbsp;<?= Yii::$app->params['settings']['contact_email']['value'] ?>
                                </div>
                            </div>
                            <div class="team-name">
                                Иван Попов
                            </div>
                            <div class="team-work">
                                Дизайнер
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6" style="margin-top: 110px">
                            <div style="position: relative">
                                <?= Html::img('/img/command2.png', ['class' => 'img-responsive']) ?>
                                <?= Html::img('/img/i.png', ['class' => 'team-info-ico', 'id' => 'team-info-ico-2']) ?>
                            </div>
                            <div id="team-info-2" class="team-info">
                                <div class="team-info-name">
                                    Екатерина Петрова
                                </div>
                                <div class="team-info-work">
                                    Главный Бухгалтер
                                </div>
                                <div class="team-info-viver">
                                    <?= Html::img('/img/footer-ico2.png'); ?>
                                    <?= Yii::$app->params['settings']['contact_phone_viber']['value'] ?>
                                    <?= Html::img('/img/footer-ico3.png'); ?>
                                    <?= Html::img('/img/footer-ico4.png'); ?>
                                </div>
                                <div class="team-info-phone">
                                    <?= Html::img('/img/footer-ico2.png'); ?>
                                    <?= Yii::$app->params['settings']['contact_phone_main']['value'] ?>
                                </div>
                                <div class="team-info-email">
                                    <?= Html::img('/img/footer-ico1.png'); ?>
                                    &nbsp;<?= Yii::$app->params['settings']['contact_email']['value'] ?>
                                </div>
                            </div>
                            <div class="team-name">
                                Екатерина Петрова
                            </div>
                            <div class="team-work">
                                Главный Бухгалтер
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div style="position: relative">
                                <?= Html::img('/img/command3.png', ['class' => 'img-responsive']) ?>
                                <?= Html::img('/img/i.png', ['class' => 'team-info-ico', 'id' => 'team-info-ico-3']) ?>
                            </div>
                            <div id="team-info-3" class="team-info">
                                <div class="team-info-name">
                                    Виталий Ершов
                                </div>
                                <div class="team-info-work">
                                    Инженер проектировщик
                                </div>
                                <div class="team-info-viver">
                                    <?= Html::img('/img/footer-ico2.png'); ?>
                                    <?= Yii::$app->params['settings']['contact_phone_viber']['value'] ?>
                                    <?= Html::img('/img/footer-ico3.png'); ?>
                                    <?= Html::img('/img/footer-ico4.png'); ?>
                                </div>
                                <div class="team-info-phone">
                                    <?= Html::img('/img/footer-ico2.png'); ?>
                                    <?= Yii::$app->params['settings']['contact_phone_main']['value'] ?>
                                </div>
                                <div class="team-info-email">
                                    <?= Html::img('/img/footer-ico1.png'); ?>
                                    &nbsp;<?= Yii::$app->params['settings']['contact_email']['value'] ?>
                                </div>
                            </div>
                            <div class="team-name">
                                Виталий Ершов
                            </div>
                            <div class="team-work">
                                Инженер проектировщик
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6" style="margin-top: 110px">
                            <div style="position: relative">
                                <?= Html::img('/img/command4.png', ['class' => 'img-responsive']) ?>
                                <?= Html::img('/img/i.png', ['class' => 'team-info-ico', 'id' => 'team-info-ico-4']) ?>
                            </div>
                            <div id="team-info-4" class="team-info">
                                <div class="team-info-name">
                                    Лиза Семенова
                                </div>
                                <div class="team-info-work">
                                    Проект менеджер
                                </div>
                                <div class="team-info-viver">
                                    <?= Html::img('/img/footer-ico2.png'); ?>
                                    <?= Yii::$app->params['settings']['contact_phone_viber']['value'] ?>
                                    <?= Html::img('/img/footer-ico3.png'); ?>
                                    <?= Html::img('/img/footer-ico4.png'); ?>
                                </div>
                                <div class="team-info-phone">
                                    <?= Html::img('/img/footer-ico2.png'); ?>
                                    <?= Yii::$app->params['settings']['contact_phone_main']['value'] ?>
                                </div>
                                <div class="team-info-email">
                                    <?= Html::img('/img/footer-ico1.png'); ?>
                                    &nbsp;<?= Yii::$app->params['settings']['contact_email']['value'] ?>
                                </div>
                            </div>
                            <div class="team-name">
                                Лиза Семенова
                            </div>
                            <div class="team-work">
                                Проект менеджер
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
$script = <<<JS
$("#team-info-ico-1").on("mouseover", function() { $("#team-info-1").show(); }).on("mouseout", function() { $("#team-info-1").hide(); });
$("#team-info-ico-2").on("mouseover", function() { $("#team-info-2").show(); }).on("mouseout", function() { $("#team-info-2").hide(); });
$("#team-info-ico-3").on("mouseover", function() { $("#team-info-3").show(); }).on("mouseout", function() { $("#team-info-3").hide(); });
$("#team-info-ico-4").on("mouseover", function() { $("#team-info-4").show(); }).on("mouseout", function() { $("#team-info-4").hide(); });
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>