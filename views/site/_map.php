<?php

use app\models\Myobject;
use yii\helpers\Html;

?>
<div id="map-fon">
    <div class="container">
        <div class="site-objects">
            <div id="object-map" class="head1">
                <span class="r-blue">Карта </span> <span class="r-brown"> Объектов</span>
                <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                <div id="map">

                </div>
            </div>
        </div>
    </div>
</div>

    <style>
        #map {
            width: 100%;
            height: 770px;
            padding: 0;
            margin: 0;
        }
    </style>
    <style>
        .YMaps-layer-container img {
            max-width: none;
        }
    </style>
<?php
$myObject = Myobject::getAllGeoObject();
$countObject = count($myObject);
$coordX = $myObject[0]['coord_x'];
$coordY = $myObject[0]['coord_y'];
$script = <<<JS
var myMap = null;
var objectCollection = null;
function addPlacemark (arr) {
    
    for (step=0;step<arr.length;step++) {
        let desc = arr[step].short_description;
        desc = desc.substring(0,160);
        desc = desc.substring(0,desc.lastIndexOf(' ')); 
        var myPlacemark = new ymaps.Placemark([arr[step].coord_x,arr[step].coord_y], {
            balloonContentHeader: "<div class='map-metro'>"+arr[step].metro+"</div>",
            balloonContentBody: "<div class='map-studio'> Студий:"+arr[step].studio+"</div><div class='map-short'>"+desc+
            "</div><div class='map-address' ><i class='fa fa-map-marker' style='color:rgb(172,152,127)'></i> "+
            arr[step].address+"</div><div class='map-cena'>Цена от <span>"+new Intl.NumberFormat().format(arr[step].price)+" руб</span></div>",
            balloonContentFooter: "<div class='map-footer'><img src='/img/detail-ico.png'> Подробнее <a href='#'>Забронировать студию</a></div>",
            hintContent: arr[step].address,
        },{
            iconLayout: 'default#image',
            iconImageHref: '/img/map-ico.png',
            iconImageSize: [30, 42],
            
        });
        objectCollection.add(myPlacemark);
        myMap.geoObjects.add(objectCollection);
    }
}       
ymaps.ready(function () {
    objectCollection = new ymaps.GeoObjectCollection();
    myMap = new ymaps.Map('map', {
        center: [$coordX,$coordY],
        zoom: 11,
    });
    $.get("/site/get-geo-object",{type:0},function (data) {
        addPlacemark(data);
    })
    myMap.behaviors.disable('scrollZoom');
    
});
JS;
$this->registerJsFile("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=7c4fab10-b262-4dc9-b2d0-968fd789cb4a;", ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJs($script, yii\web\View::POS_READY);
?>

<style>
    .map-metro {
        font-size: 1.87em;
        font-weight: bold;
        line-height: 1.2;
        color: rgb(32,36,59);
    }
    .map-studio {
        font-size: 1.33em;
        font-weight: bold;
        line-height: 1.875;
        color: rgb(32,36,59);
    }
    .map-short {
        font-size: 1.33em;
        line-height: 1.5;
        color: rgb(71,75,98);
    }
    .map-address {
        font-size: 1.33em;
        color: rgb(32,36,59);
        line-height: 1.75;

    }
    .map-cena {
        font-size: 1.33em;
        font-weight: bold;
        line-height: 1.875;
        color: rgb(32,36,59);
    }
    .map-cena span {
        font-size: 1.33em;
        color: rgb(172,152,127);
    }
    .map-footer {
        font-size: 1.33em;
        font-weight: bold;
        line-height: 1.875;
        color: rgb(32,36,59);
        vertical-align: center;
    }
    .map-footer a {
        text-align: center;
        display: block;
        float: right;
        background-color: #a58f73;
        padding: 10px 10px;
        border-radius: 24px;
        border: 1px solid #a58f73;
        color: #ffffff;
        font-weight: bold;
        width: 150px;
        line-height: 1;

    }

</style>