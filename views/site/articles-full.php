<?php

/* @var $this yii\web\View */


use app\models\Article;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Статьи';
?>
    <div id="article-full">
        <div class="container">
            <div class="site-objects">

                <div id="object-breadcrumbs">
                    <?= Html::a('Главная ', ['/']) ?>/<?= Html::a(' Статьи', ['/articles']) ?> / <?= Html::a($article->name, ['/articles','id'=>$article->id]) ?>
                </div>

                <div id="object-list">
                    <div class="row">
                        <div class="col-md-9">

                            <div class="article-item">
                                <?= Html::img($article->photo, ['class' => 'img-responsive img-rounded']) ?>
                                <div class="object-list-item">
                                    <div class="article-date"><?=date('M d, Y',$article['created_at'])?></div>

                                    <div class="article-description"><?= $article->description ?></div>


                                </div>
                            </div>
                    </div>
                        <div class="col-md-3">
                        <div class="search-article">
                            <?= Html::beginForm(['/site/email1'], 'post') . "<div class='has-feedback'>"
                            . Html::input('', 'email3', '', ['placeholder' => 'Поиск ...']) . "<span style='font-size: 20px;color: #a58f73' class=\"glyphicon glyphicon-search form-control-feedback2\"></span><br>"
                            . "</div>"
                            . Html::endForm() ?>
                        </div>
                        <div class="article-category">
                            <h3 class="center-block"><strong>Категории</strong></h3>
                            <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                            <?php
                            foreach (Article::getCategoryWithCount() as $articleItem) {
                                echo '<div class="article-category-item">';
                                echo Html::a($articleItem["name"],['/articles','category'=>$articleItem['category_id']]);
                                echo "<div class='pull-right'>(".$articleItem['category_count'].")</div>";
                                echo "</div>";
                            }
                            ?>
                            </div>
                        <div class="article-new">
                            <h3 class="center-block"><strong>Свежие новости</strong></h3>
                            <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                            <?php
                            foreach (Article::getLastArticle() as $articleItem) {
                                echo '<div class="article-new-item">';
                                echo Html::a($articleItem["name"],['/article-full','id'=>$articleItem['id']]);
                                echo "<div>".date('M d, Y',$articleItem['created_at'])."</div>";
                                echo "</div>";
                            }
                            ?>
                        </div>
                        <div class="article-tags">
                            <h3 class="center-block"><strong>Популярние теги</strong></h3>
                            <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                            </div>
                    </div>
                    </div>
                    <div class="row">
                        <div class="comments_wrap">
                            <ul>
                                <?php echo $comments;?>
                            </ul>
                        </div>
                    </div>
                    <?=Html::button('Оставить коментарий', ['style'=>'margin-top:35px', 'class'=>'contact-form-submit','onclick'=>"addComment(0)"])?>


                </div>
            </div>
        </div>
    </div>

<?=$this->render('_news');?>
<?=$this->render('_catalog');?>
<?=$this->render('_instagram');?>

<style>
    div.comments_wrap{
        width: 1000px;
    }

    div.comments_wrap ul
    {
        list-style-type: none;
    }

    div.comments_wrap ul li
    {
        margin: 7px 0 7px 7px;
    }

    div.comments_wrap ul li div.comment
    {
        padding: 5px 10px;
    }

    span.date
    {
        padding-left: 20px;
        font-size: 0.8em ;
        color: rgb(32,36,59);
    }
    span.time
    {
        padding-left: 20px;
        font-size: 0.8em ;
        color: rgb(32,36,59);
    }

    div.author
    {
        font-weight: bold;
        font-size: 1.667em;
        color:rgb(71,75,98);
        margin: 3px 0;
    }

    div.comment-answer
    {
        padding-top: 20px;
        font-weight: bold;
        font-size: 1.33em;
        color:rgb(32,36,59);
    }

    div.comment img {
        margin-right: 25px;
        height: 100px;
    }

    div.comment-answer img {
        margin-right: 10px;
        height: auto;
    }

    div.comment_text
    {
        overflow: auto;
        font-size: 1.5em;
        color:rgb(71,75,98);

    }
</style>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<div class="contact-form">

    <h3>
        Оставить комментарий
    </h3>

    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => Url::to(['site/create-comment','id'=>$article->id]),
    ]) ?>
    <?= $form->field($model, 'commentId')->hiddenInput()->label(false) ?>
    <div class="row">
        <div class="col-md-6 has-feedback">

            <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя'])->label(false) ?>
            <?= Html::img('/img/contact-user-ico.png', ['class' => 'contact-form-ico']); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput(['placeholder' => 'E-mail'])->label(false) ?>
            <?= Html::img('/img/contact-email-ico.png', ['class' => 'contact-form-ico']); ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'body')->textarea(['placeholder' => 'Сообщение'])->label(false) ?>
            <?= Html::img('/img/contact-message-ico.png', ['class' => 'contact-form-ico']); ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::classname(), [
                'template' => '<div class="col-xs-6" style="padding-left: 0px">{input}</div><div class="col-xs-6">{image}</div>',
                'options' => ['placeholder' => 'CAPTCHA Code', 'class' => 'form-control']
            ])->label(false) ?>
        </div>
        <div class="col-md-12" style="text-align: center">
            <br>
            <?= Html::submitButton('ОТПРАВИТЬ', ['class' => 'contact-form-submit']) ?>
        </div>
    </div>
    <?php $form::end() ?>
</div>
<?php Modal::end(); ?>

<style>
    .modal-dialog {
        width: 770px;
    }
</style>

<script>
    function addComment(id) {
        $('[name="ContactForm[commentId]"]').val(id);
        $('#ajaxCrudModal').modal();
    }
</script>