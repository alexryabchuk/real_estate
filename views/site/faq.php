<?php

/* @var $this yii\web\View */

/* @var $faq_cat1 Faq */

/* @var $faq_cat2 Faq */

use app\models\Article;
use app\models\Faq;
use yii\helpers\Html;

$this->title = 'О компании';

?>
<div class="site-faq">
    <div class="container">
        <div class="site-objects">

            <div class="row">
                <div class="col-lg-9" style="padding-right: 20px">
                    <div class="site-faq-list">
                    <h3>
                        Получите ответы на наиболее часто задаваемые вопросы (FAQ)
                    </h3>
                    <span class="faq-category"> Основные вопросы</span>
                    <?php foreach ($faq_cat1 as $faq): ?>
                        <div class="row">
                            <div class="faq-item">
                                <div class="faq-question">
                                    <span class="glyphicon glyphicon-plus"></span>
                                    <p><?= $faq->question ?></p>
                                </div>
                                <div class="faq-answer">
                                   <p> <?= $faq->answer ?> </p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <span class="faq-category"> Другие вопросы</span>
                        <?php foreach ($faq_cat2 as $faq): ?>
                            <div class="row">
                                <div class="faq-item">
                                    <div class="faq-question">
                                        <span class="glyphicon glyphicon-plus"></span>
                                        <p><?= $faq->question ?></p>
                                    </div>
                                    <div class="faq-answer">
                                        <p> <?= $faq->answer ?> </p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="search-article">
                        <?= Html::beginForm(['/site/email1'], 'post') . "<div class='has-feedback'>"
                        . Html::input('', 'email3', '', ['placeholder' => 'Поиск ...']) . "<span style='font-size: 20px;color: #a58f73' class=\"glyphicon glyphicon-search form-control-feedback2\"></span><br>"
                        . "</div>"
                        . Html::endForm() ?>
                    </div>
                    <div class="article-category">
                        <h3 class="center-block"><strong>Категории</strong></h3>
                        <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                        <?php
                        foreach (Article::getCategoryWithCount() as $article) {
                            echo '<div class="article-category-item">';
                            echo Html::a($article["name"],['/article-category','id'=>$article['category_id']]);
                            echo "<div class='pull-right'>(".$article['category_count'].")</div>";
                            echo "</div>";
                        }
                        ?>
                    </div>
                    <div class="article-new">
                        <h3 class="center-block"><strong>Свежие новости</strong></h3>
                        <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                        <?php
                        foreach (Article::getLastArticle() as $article) {
                            echo '<div class="article-new-item">';
                            echo Html::a($article["name"],['/article','id'=>$article['id']]);
                            echo "<div>".date('M d, Y',$article['created_at'])."</div>";
                            echo "</div>";
                        }
                        ?>
                    </div>
                    <div class="article-tags">
                        <h3 class="center-block"><strong>Популярние теги</strong></h3>
                        <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('_catalog'); ?>
    <?= $this->render('_instagram'); ?>
</div>
<?php
$script = <<<JS
$(document).on( "click", ".faq-question", function() {
    if ($(this).children().first().hasClass('glyphicon-plus')) {
        $(this).children().first().removeClass('glyphicon-plus');
        $(this).children().first().addClass('glyphicon-minus');
    } else {
        $(this).children().first().removeClass('glyphicon-minus');
        $(this).children().first().addClass('glyphicon-plus');
    }
    $(this).parent().children().last().toggle();
});
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>