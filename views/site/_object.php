<?php

use app\models\Instagram;
use app\models\Myobject;
use yii\helpers\Html;

?>
<div id="site-block-object">
    <div class="container">
        <div class="site-objects">
            <div id="object-instagram" class="head1">

                <div class="row">
                    <div class="col-lg-6">
                        <span class="r-blue"> Еще</span> <span class="r-brown"> Объекты</span>
                        <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
                    </div>
                    <div class="col-lg-6 pull-right">
                        <div class="post-nav pull-right">
                            <?= Html::img('/img/detail-ico.png'); ?>
                            <?=Html::a('  Все oбъекты',['/site/objects'],['class'=>'post-nav-all'])?>
                            <a href="#" class="post-nav-ico"> <i class="fa fa-long-arrow-left"></i></a>
                            <a href="#" class="post-nav-ico"> <i class="fa fa-long-arrow-right"></i></a>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <?php
                    $objects = Myobject::find()->where(['object_type' => 0,'deleted'=>0])->limit(3)->all();
                    $studio = [];
                    foreach ($objects as $object) {
                        $studio[$object->id] = Myobject::getStudioCount($object->id);
                    }
                    foreach ($objects

                    as $object) { ?>
                    <div class="col-lg-4 col-md-4">
                        <div class="object-item">
                            <?= Html::a(Html::img($object->image, ['class' => 'img-responsive img-rounded']), ['/objects-full', 'id' => $object->id]) ?>
                            <div class="object-list-item">
                                <div class="object-metro r-blue">
                                    <?= Html::a('Метро:',[$object->metro]) ?></div>
                                <div class="object-short_description"><?php
                                    $string = strip_tags($object->short_description);
                                    $string = substr($string, 0, 160);
                                    $string = rtrim($string, "!,.-");
                                    $string = substr($string, 0, strrpos($string, ' '));
                                    echo $string . "… ";
                                    ?></div>
                                <div class="object-address">
                                    <?= Html::img('/img/address-ico.png') ?>
                                    <?= $object->address ?>
                                </div>
                                <div class="object-studio"><span class="pull-left">Студий: <?=$studio[$object->id]['payStudio']?>/<?=$studio[$object->id]['allStudio']?></span>
                                    <span class="pull-right r-brown font-weight-bold">от <?= number_format((int)$object->price, 0, '.', ' '); ?> руб</span>
                                </div>
                                <div class="clearfix"></div>
                                <div class="object-details r-blue"><?= Html::img('/img/detail-ico.png') ?>
                                    <?=Html::a('Подробнее',['/objects-full','id'=>$object->id],['class'=>'object-detail'])?>
                                </div>
                            </div>


                            <?php echo '</div></div>';
                            }
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>