<?php

/* @var $this yii\web\View */


use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\MaskedInput;

$this->title = 'Наши объекты';
?>
<div id="fon1">
    <div class="container">
        <div class="site-objects">

            <div id="object-breadcrumbs">
                <?= Html::a('Главная ', ['/']) ?> / <?= Html::a(' Наши Объекты', ['/objects']) ?>
            </div>
            <div id="object-head" class="head1">
                <span class="r-blue">Наши </span> <span class="r-brown"> Объекты</span><br>
                <?= Html::img('/img/image-znak.png', ['class' => 'image-znak']); ?>
            </div>
            <div id="object-filter">
                <form>
                    <strong>Площадь: </strong><span class="r-grey">от</span>
                    <?=MaskedInput::widget([
                        'name' => 'suare1',
                        'value' => $filter['suare1'],
                        'options' => [
                            'class' => 'filter-input1'
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'groupSeparator' => ' ',
                            'autoGroup' => true
                        ],

                    ]);
                    ?>
                    <span class="r-grey">до</span>
                    <?=MaskedInput::widget([
                        'name' => 'suare1',
                        'value' => $filter['suare2'],
                        'options' => [
                            'class' => 'filter-input1'
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'groupSeparator' => ' ',
                            'autoGroup' => true
                        ],

                    ]);
                    ?>
                    <span class="r-grey">м<sup>2</sup>;</span>&nbsp;&nbsp;
                    <strong>Цена: </strong><span class="r-grey">от</span>
                    <?=MaskedInput::widget([
                        'name' => 'suare1',
                        'value' => $filter['price1'],
                        'options' => [
                            'class' => 'filter-input2'
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'groupSeparator' => ' ',
                            'autoGroup' => true
                        ],

                    ]);
                    ?>
                    <span class="r-grey">до</span>
                    <?=MaskedInput::widget([
                        'name' => 'suare1',
                        'value' => $filter['price2'],
                        'options' => [
                            'class' => 'filter-input2'
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'groupSeparator' => ' ',
                            'autoGroup' => true
                        ],

                    ]);
                    ?>
                    <?php
                    ?>
                    <span class="r-grey">руб.</span>
                    <?= Html::submitButton('Фильтровать', ['class' => 'filter-submit']) ?>
                </form>
            </div>
            <div id="object-list">
                <?php $count_object = count($objects);
                $i = 0 ?>
                <?php foreach ($objects as $object) : ?>
                    <?php $i++; ?>
                    <?php if (($i % 3) == 1) {
                        echo '<div class="row">';
                    } ?>
                    <div class="col-md-4 ">
                        <div class="object-item">

                            <?= Html::a(Html::img($object->image, ['class' => 'img-responsive img-rounded']), ['/objects-full', 'id' => $object->id], ['class' => "object-metro"]) ?>
                            <div class="object-list-item">
                                <?= Html::a('Метро: ' . $object->metro, ['/objects-full', 'id' => $object->id], ['class' => "object-metro"]) ?>

                                <div class="object-short_description">
                                    <?php
                                    $string = strip_tags($object->short_description);
                                    $string = substr($string, 0, 160);
                                    $string = rtrim($string, "!,.-");
                                    $string = substr($string, 0, strrpos($string, ' '));
                                    echo $string . "… ";
                                    ?></div>
                                <div class="object-address">
                                    <?= Html::img('/img/address-ico.png') ?>
                                    <?= $object->address ?>
                                </div>
                                <div class="object-studio"><span
                                            class="pull-left">Студий: <?= $studio[$object->id]['payStudio'] ?>/<?= $studio[$object->id]['allStudio'] ?></span>
                                    <span class="pull-right r-brown font-weight-bold">от <?= number_format((int)$object->price, 0, '.', ' '); ?> руб</span>
                                </div>
                                <div class="clearfix"></div>
                                <div class="object-details <?= ($i % 2) == 0 ? 'r-blue' : 'r-brown'; ?>"><?= Html::img('/img/detail-ico.png') ?>
                                    &nbsp;&nbsp; <?= Html::a('Подробнее', ['/objects-full', 'id' => $object->id], ['class' => 'object-detail']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ((($i % 3) == 0) || ($i == $count_object)) {
                        echo "</div>";
                    } ?>
                <?php endforeach; ?>

            </div>
            <div id="object-pagination" align="center" class="row" style="width: 100%; height: 100px">

                <?= LinkPager::widget([
                    'pagination' => $pages,
                    'options' => [
                        'class' => 'pagination justify-content-center',
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?= $this->render('_map'); ?>
<?= $this->render('_catalog'); ?>
<?= $this->render('_instagram'); ?>

