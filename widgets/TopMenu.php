<?php
namespace app\widgets;

use app\models\Menu;
use Yii;
use yii\bootstrap\Widget;
use yii\helpers\Html;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * Yii::$app->session->setFlash('error', 'This is the message');
 * Yii::$app->session->setFlash('success', 'This is the message');
 * Yii::$app->session->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 */
class TopMenu extends Widget
{
    public function run()
    {
        $menus = Menu::find()->orderBy('position')->all();
        //$i =0;
        //echo '<div class="col-lg-6 col-md-12">';
        foreach ($menus as $menu) {
          //  $i++;
          //  if ($i ==4) {
          //      echo '</div><div class="col-lg-6 col-md-12">';
          //  }
            $action = Yii::$app->controller->action->id;
            if (strrpos($action, '-')) {
                $action = substr($action, 0, strrpos($action, '-'));
            }
            if ('/'.$action == $menu->href) {
                echo Html::a($menu->name, $menu->href,['class'=>'menu-active']);
            } else {
                echo Html::a($menu->name, $menu->href);
            }

        }
        //echo '</div>';
        //echo Yii::$app->controller->action->id;
    }
}
