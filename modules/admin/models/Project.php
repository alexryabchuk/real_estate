<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "project".
 *
 * @property int $id
 * @property string $name
 */
class Project extends \yii\db\ActiveRecord
{
    const BINET_ONE = 'one';
    const BINET_MULTIPLE = 'multiple';
    public $file;
    public $count_used;
    public $binet;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            ['name', 'required'],
            [['allowed_groups'],'integer'],
            [['tz_binet'],'string'],
            [['file'],'file']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'allowed_groups' => 'Необходимое количество групп',
            'count_used' => 'Готовых групп / должно быть',
            'tz_binet' => 'Проект в TZ Binet',
            'binet' => 'Проекты в TZ Binet'
        ];
    }

    public function getTasksCountByStatus($status = false)
    {
        $count = (new Query())
            ->select('task.id')
            ->from('task')
            ->where(['project_id' => $this->id]);

        if ($status) {
            $count->andWhere(['status' => $status]);
        }

        return $count->count();
    }


    public function getTasksCountByStatusForSemantic($status = false)
    {
        $count = (new Query())
            ->select('task.id')
            ->from('task')
            ->where(['project_id' => $this->id])
            ->andWhere(['user_id' => Yii::$app->user->identity->id]);

        if ($status) {
            $count->andWhere(['status' => $status]);
        }

        return $count->count();
    }

    public function isFire($status)
    {
        $projectsFire = (new Query())
            ->from('task')
            ->where(['project_id' => $this->id, 'fire' => 1, 'status' => $status])->count();

        if ($projectsFire > 0) {
            return true;
        }
    }

    public function isDeadline($status)
    {
        $projects = (new Query())
            ->from('task')
            ->where(['project_id' => $this->id, 'status' => $status])
            ->andWhere(['<', 'deadline', date('Y-m-d')])->count();
        if ($projects > 0) {
            return true;
        }
    }

    public function getGroupByStatus($status)
    {
        $count = (new Query())
            ->select('keyword_groups.id')
            ->from('keyword_groups')
            ->where(['project' => $this->id])
            ->andWhere(['semantik' => Yii::$app->user->getId()]);


        if ($status) {
            $count->andWhere(['status' => $status]);
        }

        return $count->count();
    }
    public function getGroups($status)
    {
        $count = (new Query())
            ->select('keyword_groups.id')
            ->from('keyword_groups')
            ->where(['project' => $this->id]);


        if ($status) {
            $count->andWhere(['status' => $status]);
        }

        return $count->count();
    }

    public function getCountUsedGroups()
    {
        $count = (new Query())
            ->select('keyword_groups.id')
            ->from('keyword_groups')
            ->where(['project' => $this->id])
            ->andWhere(['status' => KeywordGroups::STATUS_GOTOVIE_GRUPPI]);


        return $count->count();
    }
    public function getCountUnclearUsedGroups()
    {
        $count = (new Query())
            ->select('keyword_groups.id')
            ->from('keyword_groups')
            ->where(['project' => $this->id])
            ->andWhere(['status'=>[KeywordGroups::STATUS_OBRABOTKA_GRUPP, KeywordGroups::STATUS_V_RABOTE,
                KeywordGroups::STATUS_NA_MODERATSII, KeywordGroups::STATUS_PROVERKA_GRUPP]]);


        return $count->count();
    }

    public static function getProjectNameById($id)
    {
        $project = Project::findOne(['id' => $id]);
        return $project->name;
    }
}
