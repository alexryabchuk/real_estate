<?php

namespace app\modules\admin\models;

use app\models\PhotoObject;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "colors".
 *
 * @property int $id
 * @property string $code
 */
class PhotoGallery extends Model
{
    public $projects = [];
    public $objects = [];
    public $id;

    public function __construct($object_id, $config = [])
    {
        parent::__construct($config);
        $this->projects = ArrayHelper::map(PhotoObject::find()->where(['myobject_id'=>$object_id, 'photo_type' => 1])->all(), 'id', 'filename');
        $this->objects = ArrayHelper::map(PhotoObject::find()->where(['myobject_id'=>$object_id, 'photo_type' => 0])->all(), 'id', 'filename');
        $this->id = $object_id;
    }

    public function save()
    {
        PhotoObject::deleteAll(['myobject_id'=>$this->id]);
        foreach ($this->objects as $object) {
            $photo_object = new PhotoObject();
            $photo_object->photo_type = 0;
            $photo_object->filename = $object;
            $photo_object->myobject_id = $this->id;
            $photo_object->save();
        }
        foreach ($this->projects as $project) {
            $photo_object = new PhotoObject();
            $photo_object->photo_type = 1;
            $photo_object->filename = $project;
            $photo_object->myobject_id = $this->id;
            $photo_object->save();
        }
        return true;
    }

}
