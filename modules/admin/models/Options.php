<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "options".
 *
 * @property int $id
 * @property string $key
 * @property string $value
 */
class Options extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'options';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value'], 'required'],
            [['key', 'value'], 'string', 'max' => 255],
            [['key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Использование',
            'value' => 'Значение',
        ];
    }

    public function getKeyAlias($key)
    {
        $aliases = [
            'site_email' => 'Email сайта',
            'site_email_password' => 'Email пароль',
            'site_email_name' => 'Имя отправителя',
            'email_host' => 'Email Хост',
            'email_port' => 'Email Порт',
            'email_encryption' => 'Email кодировка',
            'email_password' => 'Email пароль',
        ];

        return $aliases[$key];
    }
}
