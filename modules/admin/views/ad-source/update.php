<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdSource */
?>
<div class="ad-source-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
