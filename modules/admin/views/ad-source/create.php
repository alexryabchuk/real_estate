<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AdSource */

?>
<div class="ad-source-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
