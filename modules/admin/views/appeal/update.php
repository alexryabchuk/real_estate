<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Appeal */
?>
<div class="appeal-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
