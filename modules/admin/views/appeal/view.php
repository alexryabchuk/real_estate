<?php

use app\models\Appeal;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Appeal */
?>
<div class="appeal-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'phone',
            'name',
            'email',
            [
                'attribute' => 'type_appeal',
                'value' => function($model){
                    return Appeal::getTypes()[(int)$model->type_appeal];
                }
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                    return Appeal::getAllStatus()[(int)$model->status];
                }
            ],
        ],
    ]) ?>

</div>
