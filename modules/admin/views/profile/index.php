<?php

use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Профиль пользователя';
?>
<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group field-user-username required">
        <label class="control-label" for="user-username">Имя пользователя: <?=$model->username?> </label>
    </div>

    <div class="form-group field-user-username required">
        <label class="control-label" for="user-username">Электронная почта: <?=$model->email?> </label>
    </div>

    <div class="form-group field-user-username required">
        <label class="control-label" for="user-username">Статус: <?=User::getStatuses()[$model->status]?> </label>
    </div>

    <div class="form-group field-user-username required">
        <label class="control-label" for="user-username">Права: <?=User::getRoles()[$model->role]?> </label>
    </div>

    <?= $form->field($model, 'password')->textInput() ?>

    <?= $form->field($model, 'newpassword')->textInput() ?>

    <?= $form->field($model, 'firstname')->textInput() ?>

    <?= $form->field($model, 'lastname')->textInput() ?>

    <?= $form->field($model, 'phone')->widget(MaskedInput::className(),
        [
            'name' => 'tel',
            'mask' => '+7(999)999-99-99',
            'options'=>[
                'class'=>'form-control tel_input',
            ],
            'clientOptions'=>[
                'clearIncomplete'=>true
            ]
        ]) ?>

    <?= $form->field($model, 'nik_telegram')->textInput() ?>

    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-info']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>

