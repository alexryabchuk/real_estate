<?php

use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">
    <?php if(isset($_GET['id'])):?>
    <?php
    \yii\bootstrap\Modal::begin([
        'header' => 'Выбрите файл',
        'toggleButton' => ['label' => '<i class="fa fa-upload"></i> Импортировать семантику',
            'tag' => 'button',
            'class' => 'btn btn-info',],
    ]);
    ?>
    <?php $form = ActiveForm::begin(['action' => 'import','options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="panel panel-default panel-body">
        <input type="hidden" name="modelid" value="<?=$model->id?>">

        <?= $form->field($model, 'name')->hiddenInput(['value'=> 'some variable' ])->label(false); ?>

        <?= $form->field($model, 'file')->fileInput(['required'=> 'required' ]) ?>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Импортировать', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php \yii\bootstrap\Modal::end(); ?>
    <?php endif;?>
    <p>
    </p>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	
	 <div class="panel panel-default panel-body">

         <div class="row">

             <div class="col-md-4">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'allowed_groups')->textInput(['type' => 'number']) ?>
             </div>
         </div>
	 </div>
    <div class="panel panel-default panel-body">

        <div class="form-group">
            <?php if($model->id):?>
                <?= Html::a('Добавить Проект Tz Binet', ['tz-binet-connect/create','id'=>$model->id], ['class' => 'btn btn-primary']) ?>
            <?php endif;?>
        </div>
        <?php
        $query = \app\modules\admin\models\TzBinetConnect::find()->where(['project_id' => $model->id]);
        $provider = new ActiveDataProvider([
            'query' => $query,

        ]);
        ?>
        <?= GridView::widget([
            'id'=>'available',
            'dataProvider' => $provider,
            //'filterModel' => $searchModel,
            'rowOptions' => function ($model, $key, $index, $grid) {
                return ['data-id' => $model->id];
            },
            'pjax'=>true,
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],


                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'name',
                ],
                [
                    'class'    => 'kartik\grid\ActionColumn',
                    'template' => ' {leadView} {leadDelete}',
                    'buttons'  => [

                        'leadView' => function ($url, $provider) {
                            $url = Url::to(['tz-binet-connect/update', 'id' => $provider->id]);
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'data-toggle'=>'tooltip']);
                        },
                        'leadDelete' => function ($url, $provider) {
                            $url = Url::to(['tz-binet-connect/delete', 'id' => $provider->id]);
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'role'=>'modal-remote','title'=>'Удалить',
                                'data-confirm'=>false, 'data-method'=>false,
                                'data-request-method'=>'post',
                                'data-toggle'=>'tooltip',
                                'data-confirm-title'=>'Подтвердите действие',
                                'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                            ]);
                        },
                    ]
                ]
            ],

        ])?>

    </div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>





</div>
