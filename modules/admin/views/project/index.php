<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Проекты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">


    <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    <?= Html::a('<i class="fa fa-trash"></i> Очистить', ['remove-not-use'], ['class' => 'btn btn-danger']) ?>

    <p>
    </p>
	<div class="panel panel-default panel-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width:7%'],
            ],
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	
	 </div>
</div>

<script type="text/javascript">


    function group() {


        var group_name = $('#group_name').val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Выбирайте файл',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Объединить выбранные ключевики в группу?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'create',
                data: {
                    keylist: keys,
                    group_name: group_name
                },
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

</script>
