<?php

use app\helpers\OkNo;
use app\models\User;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Subscription */
?>
<div class="subscription-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_two',
                'value' => function ($model) {
                    $user = User::getUserFullName($model->user_one);
                    return $user ? $user : 'Не определен';
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'dd.MM.Y h:m'],
            ],
            'message',
            [

                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    return OkNo::icon($model->status);
                }
            ],
        ],
    ]) ?>

</div>
