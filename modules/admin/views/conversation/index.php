<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clients';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="client-index">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                <span class="visible-xs">Входящие сообщения</span>
                <span class="hidden-xs">Входящие сообщения</span>
            </a>
        </li>
        <li class="">
            <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs">Исходящие сообщения</span>
                <span class="hidden-xs">Исходящие сообщения</span>
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="default-tab-1">
            <?= $this->render('_index-in', [
                'searchModelIn' => $searchModelIn,
                'dataProviderIn' => $dataProviderIn,
            ]) ?>
        </div>
        <div class="tab-pane fade" id="default-tab-2">
            <?= $this->render('_index-out', [
                'searchModelOut' => $searchModelOut,
                'dataProviderOut' => $dataProviderOut,

            ]) ?>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
