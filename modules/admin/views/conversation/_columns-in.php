<?php

use app\helpers\OkNo;
use app\models\User;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'user_one',
        'filter' => User::getUserObject(),
        'value' => function ($model) {
            $user = User::getUserFullName($model->user_one);
            return $user ? $user : 'Не определен';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'created_at',
        'format' => ['date', 'dd.MM.Y HH:mm'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'headerOptions' => ['style' => 'width:50%'],
        'attribute' => 'message',
        'value' => function ($model) {
            $message = $model->message;
            if ($message) {
                return strlen($message) > 300 ? substr($message, 0, strpos($message, ' ', 150)) : $message;
            } else {
                return 'Нет информации';
            }
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status',
        'filter' => ['0'=>'Прочитано', 1=>'Не Прочитано'],
        'format' => 'raw',
        'value' => function($model) {
            return OkNo::icon($model->status);
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => '{view}{delete}',
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'urlCreator' => function ($action, $model, $key, $index) {
            return $action=='view' ? Url::to([$action.'-in', 'id' => $key]) : Url::to([$action, 'id' => $key]);
        },
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'viewOptions' => ['label' => '<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>', 'role' => 'modal-remote'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Удалить',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверенны?',
            'data-confirm-message' => 'Вы действительно хотите удалить запись'],
    ],

];   