<?php

use miloschuman\highcharts\Highcharts;
$this->title = 'Дашбоард(Менеджер)';
?>
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title-box">
                    <h3>Активность агентов </h3>
                    <span>за текущий месяц </span>
                </div>
                <ul class="panel-controls" style="margin-top: 2px;">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a>
                            </li>
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="panel-body padding-0">
                <div class="chart-holder" id="dashboard-donut-1" style="height: 200px;">
                    <?php echo Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'plotBackgroundColor' => null,
                                'plotBorderWidth' => null,
                                'plotShadow' => false,
                                'type' => 'pie'
                            ],
                            'title' => ['text' => 'Активность агентов'],
                            'series' => [
                                ['name' => 'Jane',
                                    'data' => $activeAgent,
                                ]
                            ]
                        ]]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title-box">
                    <h3>Активность партнеров</h3>
                    <span>за текущий месяц</span>
                </div>
                <ul class="panel-controls" style="margin-top: 2px;">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a>
                            </li>
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="panel-body padding-0">
                <div class="chart-holder" id="dashboard-donut-1" style="height: 200px;">
                    <?php echo Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'plotBackgroundColor' => null,
                                'plotBorderWidth' => null,
                                'plotShadow' => false,
                                'type' => 'pie'
                            ],
                            'title' => ['text' => 'Активность партнеров'],
                            'series' => [
                                ['name' => 'Jane',
                                    'data' => $activePartner,
                                ]
                            ]
                        ]]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="row">
            <div class="widget widget-default widget-item-icon" onclick="location.href='pages-messages.html';">
                <div class="widget-item-left">
                    <span class="fa fa-envelope"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?= $countDeadlineRequest ?></div>
                    <div class="widget-title">Заявок (не связывались 7 дней)</div>
                </div>
                <div class="widget-controls">
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top"
                       title="Remove Widget"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="widget widget-default widget-item-icon" onclick="location.href='pages-messages.html';">
                <div class="widget-item-left">
                    <span class="fa fa-envelope"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?= $countEmptyRequest ?></div>
                    <div class="widget-title">Заявок без задач</div>
                    <div class="widget-subtitle">за текущий месяц</div>
                </div>
                <div class="widget-controls">
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top"
                       title="Remove Widget"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" style="margin-top: 220px">

    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title-box">
                    <h3>Количество заявок по статусах</h3>
                    <span>за текущий месяц</span>
                </div>
                <ul class="panel-controls" style="margin-top: 2px;">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a>
                            </li>
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="panel-body padding-0">
                <div class="chart-holder" id="dashboard-donut-1" style="height: 200px;">
                    <?php echo Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'type' => 'column',
                            ],
                            'title' => ['text' => 'Количество заявок по статусах (общее)'],
                            'xAxis' => ['categories' => $requestStatusAll['label'],
                                'crosshair' => true,
                            ],
                            'yAxis' => [
                                'min' => 0,
                                'title' => [
                                    'text' => 'Количество студий',
                                ]
                            ],
                            'plotOptions' => [
                                'column' => [
                                    'pointPadding' => 0.2,
                                    'borderWidth' => 0,
                                ]
                            ],
                            'tooltip' => [
                                'headerFormat' => '<span style="font-size:10px">{point.key}</span><table>',
                                'pointFormat' => '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y} </b></td></tr>',
                                'footerFormat' => '</table>',
                                'shared' => true,
                                'useHTML' => true
                            ],
                            'series' => [
                                ['name' => 'Количество заявок',
                                    'data' => $requestStatusAll['data']
                                ],
                            ]
                        ]]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title-box">
                    <h3>Количество заявок по статусах по текущему пользователю</h3>
                    <span>за текущий месяц</span>
                </div>
                <ul class="panel-controls" style="margin-top: 2px;">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a>
                            </li>
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="panel-body padding-0">
                <div class="chart-holder" id="dashboard-donut-1" style="height: 200px;">
                    <?php echo Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'type' => 'column',
                            ],
                            'title' => ['text' => 'Количество заявок по статусах по текущему пользователю'],
                            'xAxis' => ['categories' => $requestStatus['label'],
                                'crosshair' => true,
                            ],
                            'yAxis' => [
                                'min' => 0,
                                'title' => [
                                    'text' => 'Количество студий',
                                ]
                            ],
                            'plotOptions' => [
                                'column' => [
                                    'pointPadding' => 0.2,
                                    'borderWidth' => 0,
                                ]
                            ],
                            'tooltip' => [
                                'headerFormat' => '<span style="font-size:10px">{point.key}</span><table>',
                                'pointFormat' => '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y} </b></td></tr>',
                                'footerFormat' => '</table>',
                                'shared' => true,
                                'useHTML' => true
                            ],
                            'series' => [
                                ['name' => 'Количество заявок',
                                    'data' => $requestStatus['data']
                                ],
                            ]
                        ]]); ?>
                </div>
            </div>
        </div>
    </div>
</div>







