<div class="widget widget-default widget-item-icon">
    <div class="widget-item-left">
        <span class="fa fa-envelope"></span>
    </div>
    <div class="widget-data">
        <div class="widget-int num-count"><?=$value?></div>
        <div class="widget-title"><?=$title?></div>
        <div class="widget-subtitle"><?=$subtitle?></div>
    </div>
    <div class="widget-controls">
        <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top"
           title="Remove Widget"><span class="fa fa-times"></span></a>
    </div>
</div>