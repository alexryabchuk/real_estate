<?php

use miloschuman\highcharts\Highcharts;

$this->title = 'Дашбоард(Агент)';
?>
<div style="height: 1200px">

<div class="row">
    <div class="col-lg-4">
        <div class="widget widget-default widget-item-icon">
            <div class="widget-item-left">
                <span class="fa fa-envelope"></span>
            </div>
            <div class="widget-data">
                <div class="widget-int num-count"><?= $allCountRequest ?></div>
                <div class="widget-title">К-во заявок всех пользователей</div>
                <div class="widget-subtitle">за текущий месяц</div>
            </div>
            <div class="widget-controls">
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top"
                   title="Remove Widget"><span class="fa fa-times"></span></a>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="widget widget-default widget-item-icon" onclick="location.href='pages-messages.html';">
            <div class="widget-item-left">
                <span class="fa fa-envelope"></span>
            </div>
            <div class="widget-data">
                <div class="widget-int num-count"><?= $myCountRequest ?></div>
                <div class="widget-title">Количество моих заявок</div>
                <div class="widget-subtitle">за текущий месяц</div>
            </div>
            <div class="widget-controls">
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top"
                   title="Remove Widget"><span class="fa fa-times"></span></a>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="widget widget-default widget-item-icon" onclick="location.href='pages-messages.html';">
            <div class="widget-item-left">
                <span class="fa fa-envelope"></span>
            </div>
            <div class="widget-data">
                <div class="widget-int num-count"><?= $countDeadlineRequest ?></div>
                <div class="widget-title">Заявок (не связывались 7 дней)</div>
            </div>
            <div class="widget-controls">
                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top"
                   title="Remove Widget"><span class="fa fa-times"></span></a>
            </div>
        </div>
    </div>
</div>
<div class="row" style="margin-bottom: 20px">
    <div class="col-lg-6" >
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title-box">
                    <h3>Рекламные источники</h3>
                    <span>за текущий месяц</span>
                </div>
                <ul class="panel-controls" style="margin-top: 2px;">

                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>
            </div>
            <div class="panel-body padding-0" style="height: 400px;">
                <div class="chart-holder" id="dashboard-donut-1" style="height: 200px;">
                    <?php echo Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'plotBackgroundColor' => null,
                                'plotBorderWidth' => null,
                                'plotShadow' => false,
                                'type' => 'pie'
                            ],
                            'title' => ['text' => 'Рекламные источники'],
                            'series' => [
                                ['name' => 'Jane',
                                    'data' => $adSourceStat,
                                ]
                            ]
                        ]]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title-box">
                    <h3>% свободных студий</h3>
                    <span>за текущий месяц</span>
                </div>
                <ul class="panel-controls" style="margin-top: 2px;">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>
            </div>
            <div class="panel-body padding-0" style="height: 400px;">
                <div class="chart-holder" id="dashboard-donut-1" style="height: 200px;">
                    <?php echo Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'type' => 'column',
                            ],
                            'title' => ['text' => '% свободных студий'],
                            'xAxis' => ['categories' => $freeStudio[2],
                                'crosshair' => true,
                            ],
                            'yAxis'=> [
                                'min' => 0,
                                'title' => [
                                    'text'=> 'Количество студий',
                                ]
                            ],
                            'plotOptions' => [
                                'column' => [
                                    'pointPadding' => 0.2,
                                    'borderWidth' => 0,
                                ]
                            ],
                            'tooltip' => [
                                'headerFormat' => '<span style="font-size:10px">{point.key}</span><table>',
                                'pointFormat' => '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y} </b></td></tr>',
                                'footerFormat' => '</table>',
                                'shared' => true,
                                'useHTML' => true
                            ],
                            'series' => [
                                ['name' => 'Всего',
                                    'data' => $freeStudio[0],
                                ],
                                ['name' => 'Свободно',
                                    'data' => $freeStudio[1],
                                ],
                            ]
                        ]]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title-box">
                    <h3>Активность агента</h3>
                    <span>за 3 месяца</span>
                </div>
                <ul class="panel-controls" style="margin-top: 2px;">

                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>
            </div>
            <div class="panel-body padding-0">
                <div class="chart-holder" id="dashboard-donut-1" style="height: 200px;">
                    <?php echo Highcharts::widget([
                        'options' => [

                            'title' => ['text' => 'График активности по дням'],
                            'xAxis' => ['categories' => array_keys($activeRequest),
                                'crosshair' => true,
                            ],
                            'yAxis'=> [
                                'min' => 0,
                                'title' => [
                                    'text'=> 'Количество заявок',
                                ]
                            ],
                            'plotOptions' => [
                                'column' => [
                                    'pointPadding' => 0.2,
                                    'borderWidth' => 0,
                                ]
                            ],
                            'tooltip' => [
                                'headerFormat' => '<span style="font-size:10px">{point.key}</span><table>',
                                'pointFormat' => '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y} </b></td></tr>',
                                'footerFormat' => '</table>',
                                'shared' => true,
                                'useHTML' => true
                            ],
                            'series' => [
                                ['name' => 'Количество заявок',
                                    'data' => array_values($activeRequest),
                                ],
                            ]
                        ]]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>