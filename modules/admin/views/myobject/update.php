<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Myobject */
$this->title = 'Список объектов: Редактирование';
?>

<div class="myobject-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
