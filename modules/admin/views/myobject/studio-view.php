<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Studio */
?>
<div class="studio-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'square',
            'balcon',
            'price',
            'status',
            'myobject_id',
        ],
    ]) ?>

</div>
