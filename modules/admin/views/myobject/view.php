<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Myobject */
?>
<div class="myobject-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [

                'attribute' => 'image',
                'format' => 'raw',
                'value' => function($model) {
                    $pos = strpos($model->image,'files')-1;
                    $thumb = substr_replace($model->image, '/.thumbs', $pos, 0);
                    //return $thumb;
                    return Html::img($thumb);
                }
            ],
            'metro',
            'etazh',
            'address',
            'price',
            'short_description',
        ],
    ]) ?>

</div>
