<?php


use app\models\User;
use app\widgets\CKEditor;
use iutbay\yii2kcfinder\KCFinderInputWidget;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


if ($model->id) {
    $uploadDir = '@app/web/upload/objects_photo/' . $model->id;
    $uploadURL = '@web/upload/objects_photo/' . $model->id;
} else {
    $uploadDir = '@app/web/upload/objects_photo/temp';
    $uploadURL = '@web/upload/objects_photo/temp';
}
/* @var $this yii\web\View */
/* @var $model app\models\Myobject */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="myobject-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group pull-right">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>
    <ul class="nav nav-tabs" style="margin-top: 0px">
        <li class="active">
            <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                <span class="visible-xs">Общая информация</span>
                <span class="hidden-xs">Общая информация</span>
            </a>
        </li>
        <li class="">
            <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs">Описание</span>
                <span class="hidden-xs">Описание</span>
            </a>
        </li>
        <li class="">
            <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs">СЕО</span>
                <span class="hidden-xs">СЕО</span>
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="default-tab-1">
            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($model, 'image')->widget(KCFinderInputWidget::className(), [
                        'kcfOptions' => [
                            'access' => [
                                'files' => [
                                    'upload' => true,
                                    'delete' => false,
                                    'copy' => false,
                                    'move' => false,
                                    'rename' => false,
                                ],
                                'dirs' => [
                                    'create' => true,
                                    'delete' => false,
                                    'rename' => false,
                                ],
                            ],
                            'uploadDir' => $uploadDir,
                            'uploadURL' => $uploadURL,
                            'thumbWidth' => 100,
                            'thumbHeight' => 100,
                        ],
                        'multiple' => false,
                        'buttonLabel' => 'Главное изображение',

                    ]);
                    ?>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            if (Yii::$app->user->identity->isAdmin()) {
                                echo $form->field($model, 'user_id')->dropDownList(User::getUserObject());
                            }
                            ?>
                            <?= $form->field($model, 'metro')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'etazh')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <?= $form->field($model, 'coord_x')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'coord_y')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-2">
                            <br>
                            <?= Html::Button('Определить', ['class' => 'btn btn-primary', 'onClick' => "getLocation();"]) ?>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'short_description')->textArea(['rows' => 6]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'object_type')->checkbox() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'hot')->checkbox() ?>
                    </div>
                    <div class="col-md-4">
                        <?php
                            if (Yii::$app->user->identity->isAdmin()) {
                                echo $form->field($model, 'show_in_site')->checkbox();
                            }
                         ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="default-tab-2">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'description1')->widget(CKEditor::className(), [
                        'options' => ['rows' => 4],
                        'preset' => 'full',
                        'clientOptions' => [
                            'kcfOptions' => [
                                'access' => [
                                    'files' => [
                                        'upload' => true,
                                        'delete' => false,
                                        'copy' => false,
                                        'move' => false,
                                        'rename' => false,
                                    ],
                                    'dirs' => [
                                        'create' => true,
                                        'delete' => false,
                                        'rename' => false,
                                    ],
                                ],
                                'uploadDir' => '@app/web/upload',
                                'uploadURL' => '@web/upload',
                                'thumbWidth' => 100,
                                'thumbHeight' => 100,
                            ],

                        ],

                    ]); ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'description2')->widget(CKEditor::className(), [
                        'options' => ['rows' => 4],
                        'preset' => 'full'
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="default-tab-3">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'SEO_TITLE')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'SEO_KEY')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'SEO_DESCRIPTION')->textarea(['rows' => 3]) ?>

                    <?= $form->field($model, 'SEO_TAGS')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
<script>
    function getLocation() {
        $.post( "/admin/service/get-coords?address="+document.getElementById('myobject-address').value,
            {},
            function (e) {
                console.log(e);
                document.getElementById("myobject-coord_x").value = e.x;
                document.getElementById("myobject-coord_y").value = e.y;
            }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log("error " + textStatus);
            console.log("incoming Text " + jqXHR.responseText);
        });
    }
</script>
<?php
$pos = strpos($model->image,'files')-1;
$thumb = substr_replace($model->image, '/.thumbs', $pos, 0);
$script = <<< JS
$("#w1-thumbs").append('<li class="sortable"><div class="remove"><span class="glyphicon glyphicon-trash"></span></div><img src="$thumb" /><input type="hidden" name="{inputName}" value="{inputValue}"></li>');
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>