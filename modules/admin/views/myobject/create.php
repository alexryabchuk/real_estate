<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Myobject */
$this->title = 'Список объектов: Создание';
?>
<div class="myobject-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
