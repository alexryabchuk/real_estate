<?php

use app\models\User;
use iutbay\yii2kcfinder\KCFinderInputWidget;
use yii\helpers\Html;
use johnitvn\ajaxcrud\CrudAsset;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $object \app\models\Myobject */


$uploadDir = '@app/web/upload/objects_photo/' . $model->id;
$uploadURL = '@web/upload/objects_photo/' . $model->id;


$this->title = 'Фотографии';
$this->params['breadcrumbs'][] = $this->title;
$this->title = 'Список объектов: Фото';
CrudAsset::register($this);

?>
<div class="studio-index">
    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default">
        <div class="panel-heading ui-draggable-handle">
            <h3 class="panel-title">Объект: Метро:<?= $object->metro ?> Адрес:<?= $object->address ?></h3>
            <ul class="panel-controls">
                <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
            </ul>
        </div>
        <div class="panel-body">
            <?php if (Yii::$app->user->identity->role == User::ROLE_AGENT || Yii::$app->user->identity->role == User::ROLE_PARTNER) : ?>
                <?php foreach ($model->objects as $item) : ?>
                    <?= Html::img($item,['style'=>'max-width:100px']) ?>
                <?php endforeach; ?>
            <?php else: ?>
                <?= $form->field($model, 'objects')->widget(KCFinderInputWidget::className(), [
                    'kcfOptions' => [
                        'access' => [
                            'files' => [
                                'upload' => true,
                                'delete' => true,
                                'copy' => false,
                                'move' => false,
                                'rename' => false,
                            ],
                            'dirs' => [
                                'create' => true,
                                'delete' => false,
                                'rename' => false,
                            ],
                        ],
                        'uploadDir' => $uploadDir,
                        'uploadURL' => $uploadURL,
                        'thumbWidth' => 100,
                        'thumbHeight' => 100,

                    ],
                    'multiple' => true,
                    'buttonLabel' => 'Выбрать изображения',
                    'thumbTemplate' => '<li class="sortable"><div class="remove"><span class="glyphicon glyphicon-trash"></span></div><img src="{thumbSrc}" /><input type="hidden" name="{inputName}" value="{inputValue}"></li>',
                ])->label(false); ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading ui-draggable-handle">
            <h3 class="panel-title">Проекты</h3>
            <ul class="panel-controls">
                <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
            </ul>
        </div>
        <div class="panel-body">
            <?php if (Yii::$app->user->identity->role == User::ROLE_AGENT || Yii::$app->user->identity->role == User::ROLE_PARTNER) : ?>
                <?php foreach ($model->projects as $item) : ?>
                    <?= Html::img($item,['style'=>'max-width:100px']) ?>
                <?php endforeach; ?>
            <?php else: ?>
            <?= $form->field($model, 'projects')->widget(KCFinderInputWidget::className(), [
                'kcfOptions' => [
                    'access' => [
                        'files' => [
                            'upload' => true,
                            'delete' => true,
                            'copy' => false,
                            'move' => false,
                            'rename' => false,
                        ],
                        'dirs' => [
                            'create' => true,
                            'delete' => false,
                            'rename' => false,
                        ],
                    ],
                    'uploadDir' => $uploadDir,
                    'uploadURL' => $uploadURL,
                    'thumbWidth' => 100,
                    'thumbHeight' => 100,

                ],
                'thumbTemplate' => '<li class="sortable"><div class="remove"><span class="glyphicon glyphicon-trash"></span></div><img src="{thumbSrc}" /><input type="hidden" name="{inputName}" value="{inputValue}"></li>',
                'multiple' => true,
                'buttonLabel' => 'Выбрать изображения',
            ])->label(false); ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="form-group pull-right">
        <?php if (Yii::$app->user->identity->role = User::ROLE_AGENT || Yii::$app->user->identity->role = User::ROLE_PARTNER) : ?>
        <?= Html::submitButton('Закрыть', ['class' => 'btn btn-success']) ?>
        <?php else:?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?php endif?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

