<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Studio */

?>
<div class="studio-create">
    <?= $this->render('_form-studio', [
        'model' => $model,
    ]) ?>
</div>
