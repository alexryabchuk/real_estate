<?php

use app\helpers\OkNo;
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

$template = '{view} ';
in_array(Yii::$app->user->identity->role, [User::ROLE_ADMIN, User::ROLE_MANAGER]) ? $template .= '{update} ' : $template .= '';
$template .= '{photo} {studio} ';
in_array(Yii::$app->user->identity->role, [User::ROLE_ADMIN]) ? $template .= '{delete}' : $template .= '';
return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'image',
        'format' => 'raw',
        'filter' => false,
        'value' => function ($model) {
            $pos = strpos($model->image, 'files') - 1;
            $thumb = substr_replace($model->image, '/.thumbs', $pos, 0);
            //return $thumb;
            return Html::img($thumb);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'metro',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'etazh',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'address',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'price',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'object_type',
        'format' => 'raw',
        'value' => function ($model) {
            return OkNo::icon($model->object_type);
        },
        'label' => 'Реализований <br>объект',
        'encodeLabel' => false,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'hot',
        'format' => 'raw',
        'value' => function ($model) {
            return OkNo::icon($model->hot);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'show_in_site',
        'format' => 'raw',
        'value' => function ($model) {
            return OkNo::icon($model->show_in_site);
        },
        'label' => 'Показивать <br>на сайте',
        'encodeLabel' => false,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'deleted',
        'format' => 'raw',
        'value' => function ($model) {
            return OkNo::icon($model->deleted);
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'header' => "Действия",
        'template' => $template,
        'buttons' => [
            'photo' => function ($url, $model, $key) {     // render your custom button
                return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-picture"></span></button>',
                    ['photo', 'object_id' => $model->id], ['title' => 'Фотогалерея',]);
            },
            'studio' => function ($url, $model, $key) {     // render your custom button
                return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-home"></span></button>',
                    ['studio', 'object_id' => $model->id], ['title' => 'Студии']);
            }
        ],
        'viewOptions' => ['label' => '<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>', 'role' => 'modal-remote'],
        'updateOptions' => ['label' => '<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>', 'title' => 'Изменить', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Удалить',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверенны?',
            'data-confirm-message' => 'Вы действительно хотите удалить запись'],
    ],

];   