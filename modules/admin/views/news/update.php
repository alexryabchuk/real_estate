<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Instagram */
?>
<div class="news-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
