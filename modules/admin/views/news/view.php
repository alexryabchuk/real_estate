<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Instagram */
?>
<div class="news-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'created_at',
            'updated_at',
            'category',
            'description:ntext',
            'SEO_TITLE',
            'SEO_KEY',
            'SEO_DESCRIPTION',
            'SEO_TAGS',
        ],
    ]) ?>

</div>
