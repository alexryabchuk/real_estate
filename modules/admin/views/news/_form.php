<?php

use app\widgets\CKEditor;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Instagram */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <ul class="nav nav-tabs" style="margin-top: 0px">
        <li class="active">
            <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                <span class="visible-xs">Общая информация</span>
                <span class="hidden-xs">Общая информация</span>
            </a>
        </li>
        <li class="">
            <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs">СЕО</span>
                <span class="hidden-xs">СЕО</span>
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="default-tab-1">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'category')->textInput() ?>

            <?= $form->field($model, 'description')->widget(CKEditor::className(),[
                'options' => ['rows' => 6],
                'preset' => 'full'

            ]);?>
        </div>
        <div class="tab-pane fade" id="default-tab-3">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'SEO_TITLE')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'SEO_KEY')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'SEO_DESCRIPTION')->textarea(['rows' => 3]) ?>

                    <?= $form->field($model, 'SEO_TAGS')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
