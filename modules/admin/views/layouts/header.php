<?php
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;


//Yii::$app->db->createCommand()->update('users', ['last_login_date' => date('Y-m-d H:i:s')], [ 'id' => Yii::$app->user->identity->id ])->execute();
?>

                
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <li class="xn-icon-button">
                        <a href="#" onclick="$.post('/site/menu-position');" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <li class="xn-icon-button pull-right">
                        <?=Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(Html::a('<span class="fa fa-sign-out"></span>',"/site/logout",['style'=>"line-height: 19px;color: #8b91a0;font-size: 13px; display: block;text-align: center;width: 50px;padding: 10px 15px"]),['class'=>'btn btn-primary' ])
                        . Html::endForm()?>
                    </li>

                    <li class="dropdown messages-menu">
                        <a href="/admin/conversation" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success"><?=\app\models\Conversation::getNewConversation()?></span>
                        </a>
                    </li>
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                    
                
                <!-- START BREADCRUMB -->
                <!-- <ul class="breadcrumb push-down-0">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Layouts</a></li>
                    <li class="active">Custom Navigation</li>                    
                </ul> -->

                <section class="breadcrumb push-down-0">
                <?php
               /* echo Breadcrumbs::widget(
                    [
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]
                )*/ ?>
                <?php if (isset($this->blocks['content-header'])) { ?>
                    <h1><?php $this->blocks['content-header'] ?></h1>
                <?php } else { ?>
                    <!-- <h1>
                        <?php
                        /*if ($this->title !== null) {
                            echo \yii\helpers\Html::encode($this->title);
                        } else {
                            echo \yii\helpers\Inflector::camel2words(
                                \yii\helpers\Inflector::id2camel($this->context->module->id)
                            );
                            echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                        }*/ ?>
                    </h1> -->
                <?php } ?>

            </section>

                <!-- END BREADCRUMB -->                           
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                <br>                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->
