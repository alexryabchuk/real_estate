<?php

use app\models\User;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;

//if (!file_exists('avatars/'.Yii::$app->user->identity->foto) || Yii::$app->user->identity->foto == '') {
$path = 'http://' . $_SERVER['SERVER_NAME'] . '/img/avatar.png';
//} else {
//   $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.Yii::$app->user->identity->foto;
//}

?>
<?php
//        $session = Yii::$app->session;
//        if($session['left'] == null | $session['left'] == 'small') $left="x-navigation-minimized";
//        else
$left = "x-navigation-custom";
?>
<!-- START PAGE SIDEBAR -->
<div class="page-sidebar page-sidebar-fixed scroll mCustomScrollbar _mCS_1 mCS-autoHide mCS_no_scrollbar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation <?= $left ?>">
        <li class="xn-logo">
            <a href="<?= Url::toRoute([Yii::$app->homeUrl]) ?>"><?= Yii::$app->name ?></a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="<?= $path ?>" alt="John Doe"/>
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="<?= $path ?>" alt="John Doe"/>
                </div>
                <div class="profile-data">
                    <div class="profile-data-name"><?= Yii::$app->user->identity->username ?></div>
                    <div class="profile-data-title"></div>
                </div>
                <div class="profile-controls">
                    <?= Html::a('<span class="fa fa-info"></span>', ['/users/profile'], ['title' => 'Профиль', 'class' => 'profile-control-left']); ?>
                    <!-- <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a> -->
                    <!-- <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a> -->
                    <?= Html::a('<span class="fa fa-envelope"></span>', ['/users/profile'], ['title' => 'Профиль', 'class' => 'profile-control-right']); ?>
                </div>
            </div>
        </li>
        <li class="xn-title">Menu</li>
        <?php if (Yii::$app->user->identity->role == User::ROLE_ADMIN) { ?>
            <li> <?= Html::a('<span class="fa fa-desktop"></span> <span class="xn-text">Дашбоард (админ)</span>', ['/admin/dashboard/admin'], []); ?> </li>
        <?php } ?>
        <?php if (Yii::$app->user->identity->role == User::ROLE_MANAGER) { ?>
            <li> <?= Html::a('<span class="fa fa-desktop"></span> <span class="xn-text">Дашбоард (менеджер)</span>', ['/admin/dashboard/manager'], []); ?> </li>
        <?php } ?>
        <?php if (Yii::$app->user->identity->role == User::ROLE_AGENT) { ?>
            <li> <?= Html::a('<span class="fa fa-desktop"></span> <span class="xn-text">Дашбоард (агент)</span>', ['/admin/dashboard/agent'], []); ?> </li>
        <?php } ?>
        <?php if (Yii::$app->user->identity->role == User::ROLE_PARTNER) { ?>
            <li> <?= Html::a('<span class="fa fa-desktop"></span> <span class="xn-text">Дашбоард (партнер)</span>', ['/admin/dashboard/partner'], []); ?> </li>
        <?php } ?>
        <?php if (in_array(Yii::$app->user->identity->role, [User::ROLE_ADMIN, User::ROLE_MANAGER])) { ?>
            <li> <?= Html::a('<span class="fa fa-phone"></span>Обращения', ['/admin/appeal'], []); ?></li>
        <?php } ?>


        <li> <?= Html::a('<span class="glyphicon glyphicon-list"></span>Заявки', ['/admin/requests'], []); ?></li>
        <li> <?= Html::a('<span class="fa fa-user"></span> <span class="xn-text">Клиенты</span>', ['/admin/client'], []); ?> </li>
        <li> <?= Html::a('<span class="fa fa-home"></span> <span class="xn-text">Объекты</span>', ['/admin/myobject'], []); ?> </li>
        <?php if (Yii::$app->user->identity->role == User::ROLE_ADMIN) { ?>
            <li> <?= Html::a('<span class="glyphicon glyphicon-file"></span> <span class="xn-text">Статьи</span>', ['/admin/article'], []); ?> </li>
        <?php } ?>
        <li> <?= Html::a('<span class="glyphicon glyphicon-user"></span> <span class="xn-text">Личный кабинет</span>', ['/admin/profile'], []); ?> </li>

        <?php if (Yii::$app->user->identity->role == User::ROLE_ADMIN) { ?>
            <li class='xn-openable <?= in_array(Yii::$app->controller->id, ["category-article", "ad-source", "faq"]) ? "active" : "" ?>'>
                <a href="#"><span class="fa fa-book"></span> <span class="xn-text">Справочники</span></a>
                <ul>
                    <li> <?= Html::a('<span class="fa fa-th-list"></span> <span class="xn-text">Категории</span>', ['/admin/category-article'], []); ?> </li>
                    <li> <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Рекламный источник</span>', ['/admin/ad-source'], []); ?> </li>
                    <li> <?= Html::a('<span class="fa fa-question"></span> <span class="xn-text">FAQ</span>', ['/admin/faq'], []); ?> </li>
                    <li> <?= Html::a('<span class="fa fa-question"></span> <span class="xn-text">Рассылка</span>', ['/admin/subscription'], []); ?> </li>
                </ul>
            </li>
        <?php } ?>
        <?php if (Yii::$app->user->identity->role == User::ROLE_ADMIN) { ?>
            <li class='xn-openable <?= in_array(Yii::$app->controller->id, ["menu", "user", "settings", "seo"]) ? "active" : "" ?>'>
                <a href="#"><span class="fa fa-university"></span> <span class="xn-text">Настройки</span></a>
                <ul>
                    <li> <?= Html::a('<span class="fa fa-th-list"></span> <span class="xn-text">Редактор меню</span>', ['/admin/menu'], []); ?> </li>
                    <li> <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Пользователи</span>', ['/admin/user'], []); ?> </li>
                    <li><?= Html::a('<span class="fa fa-bookmark"></span>Параметры ', ['/admin/settings'], []); ?></li>
                    <li><?= Html::a('<span class="glyphicon glyphicon-list-alt"></span>Настройки СЕО', ['/admin/seo'], []); ?></li>
                    <li><?= Html::a('<span class="glyphicon glyphicon-list-alt"></span>Настройки уведомлений', ['/admin/conversation-settings'], []); ?></li>

                </ul>
            </li>
        <?php } ?>


    </ul>
    <!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->