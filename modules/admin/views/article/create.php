<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Article */
$this->title = 'Статьи: Создание';
?>
<div class="article-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
