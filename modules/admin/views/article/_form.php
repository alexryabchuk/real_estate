<?php

use app\models\CategoryArticle;
use app\widgets\CKEditor;
use iutbay\yii2kcfinder\KCFinderInputWidget;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

if ($model->id) {
    $uploadDir = '@app/web/upload/article_photo/' . $model->id;
    $uploadURL = '@web/upload/article_photo/' . $model->id;
} else {
    $uploadDir = '@app/web/upload/article_photo/temp';
    $uploadURL = '@web/upload/article_photo/temp';
}

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group pull-right">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>
    <ul class="nav nav-tabs" style="margin-top: 0px">
        <li class="active">
            <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                <span class="visible-xs">Общая информация</span>
                <span class="hidden-xs">Общая информация</span>
            </a>
        </li>
        <li class="">
            <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs">СЕО</span>
                <span class="hidden-xs">СЕО</span>
            </a>
        </li>
        <li class="">
            <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs">Коментарии</span>
                <span class="hidden-xs">Коментарии</span>
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="default-tab-1">


            <div class="col-md-6">
                <?=
                $form->field($model, 'photo')->widget(KCFinderInputWidget::className(), [
                    'kcfOptions' => [
                        'access' => [
                            'files' => [
                                'upload' => true,
                                'delete' => false,
                                'copy' => false,
                                'move' => false,
                                'rename' => false,
                            ],
                            'dirs' => [
                                'create' => true,
                                'delete' => false,
                                'rename' => false,
                            ],
                        ],
                        'uploadDir' => $uploadDir,
                        'uploadURL' => $uploadURL,
                        'thumbWidth' => 100,
                        'thumbHeight' => 100,
                    ],
                    'multiple' => false,
                    'buttonLabel' => 'Главное изображение',

                ]);
                ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'category_id')->dropDownList(CategoryArticle::getCategories()) ?>
                <?= $form->field($model, 'short_description')->textarea() ?>

            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'description')->widget(CKEditor::className(),[
                    'options' => ['rows' => 6],
                    'preset' => 'full'

                ]);?>
            </div>

        </div>
        <div class="tab-pane fade" id="default-tab-2">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'SEO_TITLE')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'SEO_KEY')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'SEO_DESCRIPTION')->textarea(['rows' => 3]) ?>

                    <?= $form->field($model, 'SEO_TAGS')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="default-tab-3">
            <div class="row">
                <div class="col-md-12">
                    <div class="comments_wrap">
                        <ul>
                            <?php echo $comments;?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <?php ActiveForm::end(); ?>

</div>
<?php
if ($model->photo) {
$pos = strpos($model->photo,'files')-1;
$thumb = substr_replace($model->photo, '/.thumbs', $pos, 0);
$script = <<< JS
function deletecomment(id){
    $.post('/admin/article/delete-comment?id='+id);
}
$('.delete-comment').on('click',function() {
    $(this).parent().parent().parent().remove();
})
$("#w1-thumbs").append('<li class="sortable"><div class="remove"><span class="glyphicon glyphicon-trash"></span></div><img src="$thumb" /><input type="hidden" name="{inputName}" value="{inputValue}"></li>');
JS;

$this->registerJs($script, yii\web\View::POS_END);
}
?>

<style>
    div.comments_wrap{
        width: 1000px;
    }

    div.comments_wrap ul
    {
        list-style-type: none;
    }

    div.comments_wrap ul li
    {
        margin: 7px 0 7px 7px;
    }

    div.comments_wrap ul li div.comment
    {
        border: solid 1px #ccc;
        padding: 5px 10px;
    }

    span.date
    {
        padding-left: 20px;
        font-size: 0.8em ;
        color: rgb(32,36,59);
    }
    span.time
    {
        padding-left: 20px;
        font-size: 0.8em ;
        color: rgb(32,36,59);
    }

    div.author
    {
        font-weight: bold;
        font-size: 1.667em;
        color:rgb(71,75,98);
        margin: 3px 0;
    }

    div.comment-answer
    {
        padding-top: 20px;
        font-weight: bold;
        font-size: 1.33em;
        color:rgb(32,36,59);
    }

    div.comment img {
        margin-right: 25px;
        height: 100px;
    }

    div.comment-answer img {
        margin-right: 10px;
        height: auto;
    }

    div.comment_text
    {
        overflow: auto;
        font-size: 1.5em;
        color:rgb(71,75,98);

    }
</style>
