<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
?>
<div class="article-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'photo',
            'created_at',
            'updated_at',
            'category_id',
            'description:ntext',
            'SEO_TITLE',
            'SEO_KEY',
            'SEO_DESCRIPTION',
            'SEO_TAGS',
        ],
    ]) ?>

</div>
