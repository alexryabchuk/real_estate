<?php

use app\helpers\OkNo;
use app\models\CategoryArticle;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'photo',
        'format' => 'raw',
        'value' => function($model) {
            $pos = strpos($model->photo,'files')-1;
            $thumb = substr_replace($model->photo, '/.thumbs', $pos, 0);
            //return $thumb;
            return Html::img($thumb);
        }
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'category_id',
        'value' => function($model) {
            return CategoryArticle::getCategories()[$model->category_id];
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'short_description',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'deleted',
        'format' => 'raw',
        'value' => function($model) {
            return OkNo::icon($model->deleted,true);
        }
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'viewOptions' => ['label' => '<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>', 'role' => 'modal-remote'],
        'updateOptions' => ['label' => '<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>', 'title' => 'Изменить', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Удалить',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверенны?',
            'data-confirm-message' => 'Вы действительно хотите удалить запись'],
    ],

];   