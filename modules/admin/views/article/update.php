<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
$this->title = 'Статьи: Редактирование';
?>
<div class="article-update">

    <?= $this->render('_form', [
        'model' => $model,
        'comments' => $comments,
    ]) ?>

</div>
