<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryArticle */
?>
<div class="category-article-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
