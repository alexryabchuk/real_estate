<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryArticle */
?>
<div class="category-article-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
