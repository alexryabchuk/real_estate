<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ConversationSettings */
?>
<div class="conversation-settings-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'key',
            'name',
            'conversation_inner',
            'conversation_email:email',
            'conversation_telegram',
        ],
    ]) ?>

</div>
