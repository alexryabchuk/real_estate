<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ConversationSettings */

?>
<div class="conversation-settings-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
