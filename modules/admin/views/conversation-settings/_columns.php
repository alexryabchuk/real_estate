<?php

use app\helpers\OkNo;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'key',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'conversation_inner',
        'format'=>'raw',
        'value' => function($model) {
            return OkNo::icon($model->conversation_inner);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'conversation_email',
        'format'=>'raw',
        'value' => function($model) {
            return OkNo::icon($model->conversation_email);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'conversation_telegram',
        'format'=>'raw',
        'value' => function($model) {
            return OkNo::icon($model->conversation_telegram);
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$model->key]);
        },
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'template' => '{view} {update}',
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','role'=>'modal-remote'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
        'data-request-method'=>'post',
        'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
        'data-toggle'=>'tooltip',
        'data-confirm-title'=>'Вы уверенны?',
        'data-confirm-message'=>'Вы действительно хотите удалить запись'],
    ],

];   