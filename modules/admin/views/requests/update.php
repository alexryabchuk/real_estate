<?php

use app\models\AdSource;
use app\models\Client;
use app\models\Myobject;
use app\models\Requests;
use app\models\User;
use kartik\grid\GridView;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

use yii\widgets\MaskedInput;

CrudAsset::register($this);
$this->title = 'Заявки: Изменить';
/* @var $this yii\web\View */
/* @var $model app\models\Requests */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="requests-update">


    <div class="requests-form">

        <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

        <div class="row">
            <div class="col-md-5">
                <div class="row" style="font-size: 1.5em">
                    <div class="col-lg-12"><strong>Клиент:</strong>
                        <?= Client::getClient($model->client_id)->firstname ?>&nbsp;
                        <?= Client::getClient($model->client_id)->lastname ?>&nbsp;тел.
                        <?= Client::getClient($model->client_id)->phone ?>
                    </div>
                    <div class="col-lg-12"><strong>Объект:</strong>
                        <?php if (isset(Myobject::getObjectsAddress()[$model->myobject_id])) {
                            echo Myobject::getObjectsAddress()[$model->myobject_id];
                        } else {
                            echo 'Неизвестний объект';
                        }
                        ?>
                    </div>
                    <div class="col-lg-12"><strong>Агент:</strong>
                        <?= User::getUserName($model->user_id)->firstname ?>&nbsp;
                        <?= User::getUserName($model->user_id)->lastname ?></div>
                    <div class="col-lg-12"><strong>Источник:</strong>
                        <?= AdSource::getAdSource()[$model->ad_source_id] ?>
                    </div>
                    <div class="col-lg-12"><?= $form->field($model, 'status')->dropDownList(Requests::getStatuses()) ?></div>
                </div>
            </div>
            <div class="col-md-7">
                <?php if (!Yii::$app->request->isAjax) { ?>
                    <div class="form-group pull-right">
                        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info']) ?>
                    </div>
                <?php } ?>
                <?= $form->field($model, 'comment')->textarea(['rows' => 4]) ?>
            </div>
        </div>
        <div id="ajaxCrudDatatable">
            <?= GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'columns' => require(__DIR__ . '/_columns-comment.php'),
                'toolbar' => [
                    ['content' =>
                        '<div style="margin-top:10px;">' .
                        Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['create-comment', 'id' => $model->id],
                            ['role' => 'modal-remote', 'title' => 'Добавить', 'class' => 'btn btn-info']) .
                        '</div>'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i> История общения по заявке ',
                ]
            ]) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    <?php Modal::begin([
        "id" => "ajaxCrudModal",
        "footer" => "",// always need it for jquery plugin
        'options' => [
            'id' => 'ajaxCrudModal',
            'tabindex' => -1 // important for Select2 to work properly
        ],
    ]) ?>
    <?php Modal::end(); ?>

</div>
