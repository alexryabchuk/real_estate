<?php

use app\models\AdSource;
use app\models\Client;
use app\models\Myobject;
use app\models\Requests;
use app\models\User;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;

$this->title = 'Заявки: Создать';
/* @var $this yii\web\View */
/* @var $model app\models\Requests */

?>
<div class="requests-create">
    <div class="requests-form">

        <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'client_id')->hiddenInput() ?>
        <div class="row" id="new-client">
            <div class="col-md-6">
                <?= $form->field($model, 'phone')->widget(MaskedInput::className(),
                    [
                        'mask' => '+7(999)999-99-99',
                        'class' => 'form-control tel_input',
                        'clientOptions' => [
                            'clearIncomplete' => true
                        ],
                        'options' => [
                            'class' => 'form-control tel_input',
                            'onChange' => "changePhone ()",
                        ],
                    ]) ?>
                <div class="form-group" id = 'clientFound'>

                </div>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'client_firstname')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'client_lastname')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php if (Yii::$app->user->getIdentity()->isAdmin()) echo $form->field($model, 'user_id')->dropDownList(User::getAgents()) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'myobjects')->widget(Select2::className(), [
                    'data' => Myobject::getObjectsAddress(),
                    'options' => ['placeholder' => 'Выбрать объекты ...','autocomplete' => 'user-password'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => true
                    ],
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'status')->dropDownList(Requests::getStatuses()) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'ad_source_id')->dropDownList(AdSource::getAdSource()) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'comment')->textarea(['rows' => 4]) ?>
            </div>
        </div>
        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
<?php
$script = <<<JS

function client_type() {
  let value = $("input[name='Requests[new_client]']:checked"). val();
  if (value == 0) {
      $("#old-client").show();
      $("#new-client").hide();
  } else {
      $("#old-client").hide();
      $("#new-client").show();
  }
}

function changePhone () {
    var valuePhone = ($('#requests-phone').val());
     $.get("/admin/client/get-client-by-phone",{phone:valuePhone},function (data) {
        if (data.cnt == 1) {
         $('#requests-client_firstname').val(data.firstname).prop('readonly', true);;
         $('#requests-client_lastname').val(data.lastname).prop('readonly', true);;
         $('#requests-client_id').val(data.id)
         $('#clientFound').html('Клиент найден в базе');
} else {
         $('#requests-client_id').val(0)
            $('#requests-client_firstname').val('').prop('disabled', false);
         $('#requests-client_lastname').val('').prop('disabled', false);
         $('#clientFound').html('Клиент не найден в базе');
}
         
         
    })
}
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);
?>

