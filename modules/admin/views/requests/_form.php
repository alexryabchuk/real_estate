<?php

use app\models\AdSource;
use app\models\Client;
use app\models\Myobject;
use app\models\Requests;
use app\models\User;
use kartik\grid\GridView;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;

use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Requests */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="requests-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <div class="row">
        <div class="col-lg-4">
            <div class="row">
                <div class="col-lg-12">Клиент:
                    <?=Client::getClient($model->client_id)->firstname?>&nbsp;
                    <?=Client::getClient($model->client_id)->lastname?></div>
                <div class="col-lg-12">Объект:</div>
                <div class="col-lg-12">Агент:
                    <?=User::getUserName($model->user_id)->firstname?>&nbsp;
                    <?=User::getUserName($model->user_id)->lastname?></div>
                <div class="col-lg-12">Источник:
                    <?=AdSource::getAdSource()[$model->ad_source_id]?>
                </div>
                <div class="col-lg-12"><?= $form->field($model, 'status')->dropDownList(Requests::getStatuses()) ?></div>
            </div>
        </div>
        <div class="col-lg-8">
            <?php if (!Yii::$app->request->isAjax){ ?>
                <div class="form-group pull-right">
                    <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info']) ?>
                </div>
            <?php } ?>
            <?= $form->field($model, 'comment')->textarea(['rows' => 4]) ?>
        </div>
    </div>
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    '<div style="margin-top:10px;">' .
                    Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['create'],
                        [/*'role'=>'modal-remote',*/'title'=> 'Добавить', 'class'=>'btn btn-info']).
                    '<ul class="panel-controls">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>  '.
                    '</div>'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="glyphicon glyphicon-list"></i> Заявки ',
                'after'=>BulkButtonWidget::widget([
                        'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                            ["bulk-delete"] ,
                            [
                                "class"=>"btn btn-danger btn-xs",
                                'role'=>'modal-remote-bulk',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                'data-request-method'=>'post',
                                'data-confirm-title'=>'Вы уверенны?',
                                'data-confirm-message'=>'Действительно удалить выделенные записи'
                            ]),
                    ]).
                    '<div class="clearfix"></div>',
            ]
        ])?>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    'options' => [
        'id' => 'ajaxCrudModal',
        'tabindex' => -1 // important for Select2 to work properly
    ],
])?>
<?php Modal::end(); ?>