<?php

use app\models\AdSource;
use app\models\Client;
use app\models\Requests;
use app\models\User;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Requests */
?>
<div class="requests-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'client_id',
                'value' => function ($model) {
                    return Client::getClients()[$model->client_id];
                }
            ],
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return User::getUserFullName($model->user_id);
                }
            ],
            [
                'attribute' => 'status',
                'format'=>'raw',
                'value' => function ($model) {
                    return '<span style="color:#'.Requests::getColorStatuses()[$model->status].'" class="fa fa-circle"></span>'.Requests::getStatuses()[$model->status];
                }
            ],
            [
                'attribute' => 'ad_source_id',
                'value' => function ($model) {
                    return AdSource::getAdSource()[$model->ad_source_id];
                }
            ],
        ],
    ]) ?>

</div>
