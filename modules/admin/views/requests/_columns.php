<?php

use app\helpers\OkNo;
use app\models\AdSource;
use app\models\Client;
use app\models\Myobject;
use app\models\Requests;
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'format'=>['date', 'dd.MM.Y'],

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'filter' =>User::getAgentsName(),
        'visible' => in_array(Yii::$app->user->identity->getRole(),[User::ROLE_MANAGER,User::ROLE_ADMIN]),
        'value' => function($model){
            $userIsSet = User::getUserFullName($model->user_id);
            return isset ($userIsSet) ? User::getUserFullName($model->user_id) : 'Не определен';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_id',
        'filter' =>Client::getClientsFullname(),
        'value' => function($model){
            return isset(Client::getClientsFullname()[$model->client_id]) ? Client::getClientsFullname()[$model->client_id] : 'Не определен';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'clientPhone',
        'label'=>'Телефон'


    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'filter' => User::getAgentsName(),
        'value' => function($model){
            return User::getAgentsName()[$model->user_id];
        },
        'visible' => false,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'myobject_id',
        'filter' => Myobject::getObjectsAddress(),
        'value' => function($model){
            return $model->myobject_id ? Myobject::getObjectsAddress()[$model->myobject_id] : 'Не опрелен';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status',
        'filter' => Requests::getStatuses(),
        'format' => 'raw',
        'value' => function($model) {
            return Requests::getStatuses()[$model->status] ? ''.Requests::getStatuses()[$model->status] : 'Не опрелен';

        },
        'contentOptions' =>function ($model){
            return ['style'=>'color:#'.Requests::getColorStatuses()[$model->status]];
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ad_source_id',
        'filter' => AdSource::getAdSource(),
        'value' => function($model){
            return AdSource::getAdSource()[$model->ad_source_id];
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
        'label'=>'Важная информация',
        'headerOptions' => ['style' => 'width:30%'],
        'value' => function($model){
            $comment = \app\models\RequestsComment::find()->where(['requests_id'=>$model->id])->orderBy('id DESC')->one();
            if ($comment) {
                return strlen($comment->comment) > 300 ? substr($comment->comment, 0, strpos($comment->comment, ' ', 20)) : $comment->comment;
            } else {
                return 'Нет информации';
            }
        }
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'deleted',
        'filter' => [0=>'Нет',1=>'Да'],
        'format' => 'raw',
        'value' => function($model) {
            return OkNo::icon($model->deleted);
        }
    ],


    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template'=> Yii::$app->user->identity->isAdmin() ? '{view} {update} {delete} {history}' : '{view} {update}',
        'buttons' => [
            'history' => function ($url, $model, $key) {     // render your custom button
                return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-picture"></span></button>',
                    ['history', 'id' => $model->id], ['title' => 'История изменений']);
            },
        ],
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','role'=>'modal-remote'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>',/*'role'=>'modal-remote',*/'title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
        'data-request-method'=>'post',
        'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
        'data-toggle'=>'tooltip',
        'data-confirm-title'=>'Вы уверенны?',
        'data-confirm-message'=>'Вы действительно хотите удалить запись'],
    ],

];   