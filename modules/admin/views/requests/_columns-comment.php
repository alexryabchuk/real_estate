<?php

use app\models\AdSource;
use app\models\Client;
use app\models\Requests;
use app\models\User;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'filter' => false,
        'value' => function($model){
            return User::getUserName($model->user_id)->firstname.User::getUserName($model->user_id)->lastname;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
        'value' => function($model){
            return strlen($model->comment) > 50 ? substr($model->comment,0,strpos($model->comment,' ',20)) : $model->comment;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'format'=>['date', 'dd.MM.Y HH:mm'],

    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template'=>'{update} {delete}',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action.'-comment','id'=>$key]);
        },
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
        'data-request-method'=>'post',
        'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
        'data-toggle'=>'tooltip',
        'data-confirm-title'=>'Вы уверенны?',
        'data-confirm-message'=>'Вы действительно хотите удалить запись'],
    ],

];   