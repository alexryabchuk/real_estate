<?php

use app\models\AdSource;
use app\models\Client;
use app\models\Myobject;
use app\models\Requests;
use app\models\User;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;


/* @var $this yii\web\View */
/* @var $model app\models\Requests */

?>
<div class="requests-create">
        <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'comment')->textarea(['rows' => 4]) ?>

        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

</div>
