<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ClientLog */
$this->title = 'Заявки: История';
?>
<div class="client-view">
 <table class="table">
     <thead>
        <tr>
            <th>Дата</th>
            <th>Пользователь</th>
            <th>Изменения</th>
        </tr>
     </thead>
     <tbody>


    <?php foreach ($model as $item) :?>
        <tr>
            <td>
                <?=date('d.m.Y h:i:s',$item->created_at);?>
            </td>
            <td>
                <?=\app\models\User::getUserName($item->user_id)->firstname.' '.\app\models\User::getUserName($item->user_id)->lastname?>
            </td>
            <td>
                <?=$item->data?>
            </td>
        </tr>
    <?php endforeach;?>
     </tbody>
 </table>
</div>
