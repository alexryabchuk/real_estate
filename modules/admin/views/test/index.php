<style>
    #map {
        width: 100%;
        height: 500px;
    }
</style>
<div id="map"></div>

<!-- 4. Пишем скрипт который создаст и отобразит карту Google Maps на странице. -->
<script type="text/javascript">

    // Определяем переменную map
    var map;

    // Функция initMap которая отрисует карту на странице
    function initMap() {

        // В переменной map создаем объект карты GoogleMaps и вешаем эту переменную на <div id="map"></div>
        map = new google.maps.Map(document.getElementById('map'), {
            // При создании объекта карты необходимо указать его свойства
            // center - определяем точку на которой карта будет центрироваться
            center: {lat: -34.397, lng: 150.644},
            // zoom - определяет масштаб. 0 - видно всю платнеу. 18 - видно дома и улицы города.
            zoom: 8
        });
    }
</script>

<!-- 3. Подключаем библиотеку Google Maps Api, чтобы создать карту -->
<!-- Атрибуты async и defer - позволяют загружать этот скрипт асинхронно, вместе с загрузкой всей страницы  -->
<!-- В подключении библиотеки Google Maps Api в конце указан параметр callback, после  подключения и загрузки этого Api сработает функция initMap для отрисовки карты,  которую мы описали выше -->
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC67aPpW9WdU4cewPfGXJzlf8niD_q9-Ew&callback=initMap"></script>
