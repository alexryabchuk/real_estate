<?php

use yii\helpers\Html;
use app\modules\admin\controllers\TaskController;
use app\modules\admin\models\Task;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Task */

$this->title = 'Изменение: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Все ТЗ', 'url' =>  TaskController::generatePreviousUrl($model->id)];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';


?>
<div class="task-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
