<?php

use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Готовое ТЗ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('<i class="fa fa-clone"></i> Групповое создание ТЗ', ['group-creation'], ['class' => 'btn btn-success']) ?>
        <span class="btn btn-success" onClick="deleteSelectedTasks()"><i class="fa fa-trash-o"></i> Удалить</span>
    <span class="btn btn-success" onClick="rebuild()"><i class="fa fa-backward"></i> На доработку</span>
        <span class="btn btn-success" onClick="used()"><i class="fa fa-hdd-o"></i> Использованы</span>
        <?php
        Modal::begin([
            'header' => 'Экспорт ТЗ',
            'toggleButton' => ['label' => '<i class="fa fa-download"></i> Экспорт ТЗ',
                'tag' => 'button',
                'class' => 'btn btn-success',
                'onClick' => 'exportTask()'],
        ]);
        ?>
        <span class="btn btn-default" onClick="setSelection()">Выделить все</span>
        <br><br>

        <?= Html::textArea('', '', ['id' => 'taskText', 'rows' => 24, 'style' => 'width:100%']) ?>

        <?php Modal::end(); ?>
    <?php
    Modal::begin([
        'header' => 'Групповая смена дедлайна',
        'toggleButton' => ['label' => '<i class="fa fa-pencil"></i> Групповая смена дедлайна',
            'tag' => 'button',
            'class' => 'btn btn-success',],
    ]);
    ?>
    <?php
    echo \yii\jui\DatePicker::widget([
        'language' => 'ru',
        'dateFormat' => 'dd.MM.yyyy',
        'options' => [
                'id' => 'from_date',
        ],
    ]);
    ?>

    <span class="btn btn-default" onClick="changeDeadline()">Изменить дедлайны</span>

    <?php Modal::end(); ?>
    <p></p>
    <div class="panel panel-default panel-body">
	
		<?php $projects = (new \yii\db\Query())
					->select(['project_id AS id', 'project.name AS name'])
					->distinct()
					->from('task')
					->join('LEFT JOIN', 'project', 'project_id = project.id')
					->where(['status' => '2'])
					->orderBy('project.name')
					->all(); ?>

        <?php Pjax::begin(['id' => 'pjax_grid']); ?>

        <?=
        GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                    'class' => 'yii\grid\CheckboxColumn',
                    'name' => 'checked',
                    'checkboxOptions' => function($model) {
                        return ['value' => $model->id];
                    },
					'contentOptions' => ['style' => 'text-align:center;'],
					'headerOptions' => ['style' => 'text-align:center;'],
                ],
                [
                    'attribute' => 'fire',	
                    'label' => Html::img('@web/images/fire.png', ['height' => '19px', 'width' => 'auto']),	
					'encodeLabel' => false,
                    'content' => function($data) {
                        if ($data->fire == 1)							
							return Html::img('@web/images/fire.png', ['height' => '20px', 'width' => 'auto']);
                    },
					'contentOptions' => ['style' => 'text-align:center;'],
					'headerOptions' => ['style' => 'text-align:center;'],					
                ],
                [
                    'attribute' => 'name',
                    'value' => function ($model) {
                        $modelColor = $model->getMissedKeywords(true);
                        $add = [];

                        if(!$modelColor){
                            $add = ['style'=>'color:#8A2BE2'];
                        }

                        return Html::a(
                            Html::encode($model->name) .
                            \app\modules\admin\widgets\CommentCountWidget::widget(['task' => $model]),
                            Url::to(["update?id={$model->id}"]),
                            $add
                        );
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'semantic',
                    'value' => function ($data) {
                        return Html::encode($data->getSemantic());
                    },
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'white-space: nowrap; '],
                ],
                    [
                    'attribute' => 'status',
                    'content' => function($data) {
                        return $data->statusName;
                    },
                    'contentOptions' => ['style' => 'white-space: nowrap; '],
                ],
                    [
                    'attribute' => 'project_id',
                    'content' => function($data) {
                        return $data->projectName;
                    },
                    'contentOptions' => ['style' => 'white-space: nowrap; '],
					'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'project_id',
                        'data' => ArrayHelper::Map($projects, 'id', 'name'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => ''
                        ]
                    ]),
                ],
				'project_TZBinet',
                    [
                    'attribute' => 'date_add',
                    'content' => function($data) {
                        return date('d.m.Y H:i', strtotime($data->date_add));
                    },
                    'contentOptions' => ['style' => 'white-space: nowrap; '],
                ],
                    [
                    'attribute' => 'deadline',
                    'content' => function($data) {
                        return date('d.m.Y', strtotime($data->deadline));
                    },
					'contentOptions' => function($data) {	
						if ($data->deadline < date('Y-m-d'))
                            return ['style' => 'color:red; white-space: nowrap;'];
                        else
                            return ['style' => 'white-space: nowrap;'];
                    },
                ],
                    [
                    'attribute' => 'group_id',
					'content' => function($data) {
                        return isset($data->group_id) ? $data->group_id : '';
                    },
                    'contentOptions' => ['style' => 'width: 50px;text-align:center white-space: nowrap;;'],
                ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'white-space: nowrap; '],
                    ],
            ],
        ]);
        ?>

        <?php Pjax::end(); ?>

    </div>

</div>

<script type="text/javascript">

    function used() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ТЗ',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Перевести в статус "Готовое ТЗ (использовано)"?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'used',
                data: {keylist: keys},
                success: function (result) {
                    /*swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });*/
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function deleteSelectedTasks() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ТЗ',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Удалить выбранные ТЗ?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'delete-selected-tasks',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function rebuild() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ТЗ',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Отправить на доработку выбранные ТЗ?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'rebuild-selected-tasks',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }


    function exportTask() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        $("#taskText").html('');

        $.ajax({
            type: "POST",
            url: 'export-task',
            data: {keylist: keys},
            success: function (result) {
                $("#taskText").html(result);
            }
        });
    }

    function changeDeadline() {

        var keys = $('#grid').yiiGridView('getSelectedRows');
        var from_date = $('#from_date').datepicker().val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ТЗ',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Изменить дедлайны выбранных ТЗ?');

        if (dialog == true) {
            $.ajax({
                type: "POST",
                url: 'change-deadline',
                data: {
                    keylist: keys,
                    deadline: from_date
                },
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }


    function setSelection() {

        var target = document.getElementById('taskText');
        var rng, sel;
        if (document.createRange) {
            rng = document.createRange();
            rng.selectNode(target);
            sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(rng);
            //document.execCommand("Copy");
        } else {
            var rng = document.body.createTextRange();
            rng.moveToElementText(target);
            rng.select();
        }
    }

</script>





















