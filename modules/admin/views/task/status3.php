<?php

use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Готовое ТЗ (использовано)';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    <?= Html::a('<i class="fa fa-clone"></i> Групповое создание ТЗ', ['group-creation'],
        ['class' => 'btn btn-success']) ?>
    <span class="btn btn-success" onClick="deleteSelectedTasks()"><i class="fa fa-trash-o"></i> Удалить</span>
    <?php
    \yii\bootstrap\Modal::begin([
        'header' => 'Групповая смена дедлайна',
        'toggleButton' => [
            'label' => '<i class="fa fa-pencil"></i> Групповая смена дедлайна',
            'tag' => 'button',
            'class' => 'btn btn-success',
        ],
    ]);
    ?>
    <?php
    echo \yii\jui\DatePicker::widget([
        'language' => 'ru',
        'dateFormat' => 'dd.MM.yyyy',
        'options' => [
            'id' => 'from_date',
        ],
    ]);
    ?>

    <span class="btn btn-default" onClick="changeDeadline()">Изменить дедлайны</span>

    <?php \yii\bootstrap\Modal::end(); ?>

    <?php
    \yii\bootstrap\Modal::begin([
        'header' => 'Экспорт',
        'toggleButton' => [
            'label' => '<i class="fa fa-clone"></i> Экспортировать все ТЗ',
            'tag' => 'button',
            'class' => 'btn btn-warning',
        ],
    ]);
    ?>

    <?php
    $tasks = $dataProvider->getModels();

    $exportFormModel = new \app\modules\admin\models\ExportForm();
    $exportFormModel->tasksIdList = ArrayHelper::getColumn($tasks, 'id');

    ?>

    <div class="export-form">
        <?php $form = \yii\bootstrap\ActiveForm::begin(
        //['action' => ['builder/saveform'],'options' => ['method' => 'post']]
        ); ?>

        <div class="panel panel-default panel-body">

            <div class="row">
                <div class="col-md-12">
                    <h3>Экспортировать все ТЗ в новый проект</h3>
                    <ul>
                        <li>
                            Все задания из списка будут скопированы в выбранный проект
                        </li>
                        <li>
                            Созданным задачам будет присвоен статус "Готовое ТЗ"
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row">


                <div class="col-md-12">

                    <?php $projects = \app\modules\admin\models\Project::find()
                        ->orderBy('name')
                        ->all(); ?>

                    <?php
                    echo $form->field($exportFormModel, 'projectId')->widget(Select2::class, [
                        'data' => ArrayHelper::Map($projects, 'id', 'name'),
                        'options' => ['placeholder' => 'Выберите'],
                        'pluginOptions' => [
                            'maximumSelectionLength'=> 1,
                            'allowClear' => false,
                            'multiple' => true,
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($exportFormModel, 'taskIdListInputString')->hiddenInput()->label(false) ?>
                </div>


            </div>

            <div class="form-group">
                <?= Html::submitButton('Экспортировать', ['class' => 'btn btn-success']) ?>
            </div>

            <?php \yii\bootstrap\ActiveForm::end(); ?>

        </div>
    </div>


    <?php \yii\bootstrap\Modal::end(); ?>

    <p></p>

    <div class="panel panel-default panel-body">

        <?php $projects = (new \yii\db\Query())
            ->select(['project_id AS id', 'project.name AS name'])
            ->distinct()
            ->from('task')
            ->join('LEFT JOIN', 'project', 'project_id = project.id')
            ->where(['status' => '3'])
            ->orderBy('project.name')
            ->all(); ?>

        <?php Pjax::begin(['id' => 'pjax_grid']); ?>

        <?=
        GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'name' => 'checked',
                    'checkboxOptions' => function ($model) {
                        return ['value' => $model->id];
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'headerOptions' => ['style' => 'text-align:center;'],
                ],
                [
                    'attribute' => 'name',
                    'value' => function ($model) {
                        return Html::a(Html::encode($model->name) .
                            \app\modules\admin\widgets\CommentCountWidget::widget(['task' => $model]),
                            Url::to(["update?id={$model->id}"]));
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'semantic',
                    'value' => function ($data) {
                        return Html::encode($data->getSemantic());
                    },
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'white-space: nowrap; '],
                ],
                [
                    'attribute' => 'status',
                    'content' => function ($data) {
                        return $data->statusName;
                    },
                    'contentOptions' => ['style' => 'white-space: nowrap; '],
                ],
                [
                    'attribute' => 'project_id',
                    'content' => function ($data) {
                        return $data->projectName;
                    },
                    'contentOptions' => ['style' => 'white-space: nowrap; '],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'project_id',
                        'data' => ArrayHelper::Map($projects, 'id', 'name'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => ''
                        ]
                    ]),
                ],
                'project_TZBinet',
                [
                    'attribute' => 'date_add',
                    'content' => function ($data) {
                        return date('d.m.Y H:i', strtotime($data->date_add));
                    },
                    'contentOptions' => ['style' => 'white-space: nowrap; '],
                ],
                [
                    'attribute' => 'deadline',
                    'content' => function ($data) {
                        return date('d.m.Y', strtotime($data->deadline));
                    },
                    'contentOptions' => function ($data) {
                        if ($data->deadline < date('Y-m-d'))
                            return ['style' => 'color:red; white-space: nowrap;'];
                        else
                            return ['style' => 'white-space: nowrap;'];
                    },
                ],
                [
                    'attribute' => 'group_id',
                    'content' => function ($data) {
                        return isset($data->group_id) ? $data->group_id : '';
                    },
                    'contentOptions' => ['style' => 'width: 50px;text-align:center white-space: nowrap;;'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'white-space: nowrap; '],
                ],
            ],
        ]);
        ?>

        <?php Pjax::end(); ?>

    </div>

</div>
<?php
$this->registerJs('
$(document).ready(function(){
    $(\'.modal-dialog\').click(function(){
    
        $(\'.s2-select-label\').hide();
    });
   
});
');
?>
<script type="text/javascript">

    function deleteSelectedTasks() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ТЗ',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Удалить выбранные ТЗ?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'delete-selected-tasks',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function changeDeadline() {

        var keys = $('#grid').yiiGridView('getSelectedRows');
        var from_date = $('#from_date').datepicker().val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ТЗ',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Изменить дедлайны выбранных ТЗ?');

        if (dialog == true) {
            $.ajax({
                type: "POST",
                url: 'change-deadline',
                data: {
                    keylist: keys,
                    deadline: from_date
                },
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

</script>










