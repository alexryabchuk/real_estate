<?php

use yii\helpers\Html;
use app\modules\admin\controllers\TaskController;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Task */

$this->title = 'Создание нового ТЗ';
$this->params['breadcrumbs'][] = ['label' => 'Все ТЗ', 'url' =>  TaskController::generatePreviousUrl($model->id)];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
