<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Project;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Task */

$this->title = 'Групповое создание ТЗ';
$this->params['breadcrumbs'][] = ['label' => 'Все ТЗ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default panel-body">  

        <?php $projects = Project::find()->orderBy('name')->all(); ?>

        <?= $form->field($model, 'keywords')->textArea(['maxlength' => true, 'rows' => 20]) ?>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>  

</div>












