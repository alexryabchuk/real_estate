<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'format'=>['date', 'dd.MM.Y'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'firstname',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'lastname',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'phone',

    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template'=> Yii::$app->user->identity->isAdmin() ? '{view} {update} {delete} {history}' : '{view} {update} ',
        'buttons' => [
            'history' => function ($url, $model, $key) {     // render your custom button
                return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-picture"></span></button>',
                    ['history', 'id' => $model->id], ['title' => 'История изменений']);
            },
        ],
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'viewOptions' => ['label' => '<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>', 'role' => 'modal-remote'],
        'updateOptions' => ['label' => '<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>', 'role' => 'modal-remote', 'title' => 'Изменить', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Удалить',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверенны?',
            'data-confirm-message' => 'Вы действительно хотите удалить запись'],
    ],

];   