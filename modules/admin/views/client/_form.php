<?php

use app\models\User;
use unclead\multipleinput\MultipleInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->widget(MaskedInput::className(),
        [
                        'mask' => '+7(999)999-99-99',
                        'class' => 'form-control tel_input',
                        'clientOptions' => [
                            'clearIncomplete' => true
                        ],
                        'options' => [
                            'class' => 'form-control tel_input',
                        ],
        ]) ?>

    <?php if (Yii::$app->user->identity->isAdmin()) echo $form->field($model, 'user_id')->dropDownList(User::getManagers()) ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
