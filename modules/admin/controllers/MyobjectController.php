<?php

namespace app\modules\admin\controllers;

use app\models\Studio;
use app\models\StudioSearch;
use app\models\User;
use app\modules\admin\models\PhotoGallery;
use Yii;
use app\models\Myobject;
use app\models\MyobjectSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * MyobjectController implements the CRUD actions for Myobject model.
 */
class MyobjectController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'studio', 'photo'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return in_array(yii::$app->user->getIdentity()->getRole(), [User::ROLE_ADMIN, User::ROLE_MANAGER, User::ROLE_AGENT, User::ROLE_PARTNER]);
                        }
                    ],
                    [
                        'actions' => ['create', 'update', 'studio-create', 'studio-update', 'studio-view'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return in_array(yii::$app->user->getIdentity()->getRole(), [User::ROLE_ADMIN, User::ROLE_MANAGER]);
                        }
                    ],
                    [
                        'actions' => ['delete', 'bulk-delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return in_array(yii::$app->user->getIdentity()->getRole(), [User::ROLE_ADMIN,]);
                        }
                    ],
                ],

            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new MyobjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStudio($object_id)
    {
        $object = $this->findModel($object_id);
        $searchModel = new StudioSearch();
        $searchModel->myobject_id = $object_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('studio-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'object_id' => $object_id,
            'object' => $object,
        ]);
    }

    public function actionPhoto($object_id)
    {
        $object = $this->findModel($object_id);
        $model = new PhotoGallery($object_id);
        if ($model->load(Yii::$app->request->post())) {
            $model->objects = Yii::$app->request->post('PhotoGallery')['objects'];
            $model->projects = Yii::$app->request->post('PhotoGallery')['projects'];
            $model->save();
            return $this->redirect(['index']);
        }
        return $this->render('photo-index', [
            'model' => $model,
            'object' => $object,
        ]);
    }

    public function actionStudioCreate($object_id)
    {
        $request = Yii::$app->request;
        $model = new Studio();
        $model->myobject_id = $object_id;
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать новую студию",
                    'content' => $this->renderAjax('studio-create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Создать новую студию",
                    'content' => '<span class="text-success">Create Studio success</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать еще', ['studio-create'], ['class' => 'btn btn-info', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Создать новую студию",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('studio-create', [
                    'model' => $model,
                ]);
            }
        }

    }

    public function actionStudioUpdate($id)
    {
        $request = Yii::$app->request;
        $model = Studio::findOne($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Изменить студию " . $id,
                    'content' => $this->renderAjax('studio-update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Студия " . $id,
                    'content' => $this->renderAjax('studio-view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-info', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Изменить студию " . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('studio-update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionStudioView($id)
    {
        $request = Yii::$app->request;
        $model = Studio::findOne($id);
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Studio #" . $id,
                'content' => $this->renderAjax('studio-view', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('studio-view', [
                'model' => $model,
            ]);
        }
    }

    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Myobject #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-info'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    private function recurse_copy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    $this->recurse_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    rename($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Myobject();
        $model->user_id = Yii::$app->user->id;
        if ($model->load($request->post()) && $model->save()) {
            $path_src = Yii::getAlias('@app/web/upload/objects_photo/temp');
            $path = Yii::getAlias('@app/web/upload/objects_photo/' . $model->id);
            if (!is_dir($path)) {
                mkdir($path);
            }
            $this->recurse_copy($path_src, $path);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);


        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $item = $this->findModel($id);
        $item->deleted == 1 ? $item->deleted = 0 : $item->deleted = 1;
        $item->save();
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    protected function findModel($id)
    {
        if (($model = Myobject::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
