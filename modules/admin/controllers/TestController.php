<?php

namespace app\modules\admin\controllers;

use app\models\Client;
use app\models\ClientLog;
use app\modules\admin\models\KeywordGroups;
use Yii;
use app\modules\admin\models\Keywords;
use app\modules\admin\controllers\KeywordsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TestController implements the CRUD actions for Keywords model.
 */
class TestController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Keywords models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = Client::findOne(27);
        //$model = new Client();
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Keywords model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Keywords model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Keywords();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $grp = KeywordGroups::findOne(['id' => $_GET['id']]);
            $model->group_id = $grp->connect_id;
            $model->save();
            $grp->main_keywords .= $model->keyword."\n";
            if($model->frequency != null)
                $grp->frequency += $model->frequency;
            $grp->save();
            return $this->redirect(['keyword-groups/update', 'id' => $_GET['id']]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Keywords model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Keywords model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Keywords model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Keywords the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Keywords::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGroup()
    {

        $keylist = Yii::$app->request->post('keylist');
        $group_name = Yii::$app->request->post('group_name');
        $group_id = (new \yii\db\Query())
            ->select(['max(group_id)'])
            ->from('keywords')
            ->scalar();

        $next_group_id = $group_id + 1;

        $old_id = Keywords::findOne($keylist[0])->group_id;
        $count = $old_id;
        if($old_id){
            foreach ($keylist as $key) {
                $task = Keywords::findOne($key);
                $count = $task->group_id;
            }
            if($count == $old_id){
                return 'Ключевики уже в группе!';
            }
        }


        foreach ($keylist as $key) {
            $task = Keywords::findOne($key);
            $task->group_id = $next_group_id;
            $task->save();
        }

        return 'Ключевики объединены в группу.';
    }

    public function actionDeleteSelectedTasks()
    {

        $keylist = Yii::$app->request->post('keylist');

        foreach ($keylist as $key) {
            Keywords::findOne($key)->delete();
        }

        return 'Ключевики удалены.';
    }
}
