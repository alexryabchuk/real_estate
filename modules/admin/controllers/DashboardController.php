<?php

namespace app\modules\admin\controllers;

use app\models\AdSource;
use app\models\Myobject;
use app\models\Requests;
use app\models\service\Dashboard;
use app\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\modules\admin\models\StatAdminReport;

/**
 * Default controller for the `admin` module
 */
class DashboardController extends Controller {

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        $date1 = strtotime(date('01.m.Y'));
        $date2 = strtotime(date('d.m.Y'). ' + 1 days');
        //Количество заявок всех пользователей
        $allRequest = Dashboard::factory()->RequestsCount();
        //$allRequest = Requests::find()->where(['>','created_at',$date1])->andWhere(['<','created_at',$date2])->andWhere(['deleted'=>0])->count();

        //Стастистика по рекламному источнику
        $adSourceStat = Dashboard::factory()->AdSourseData();
        //% броней
        $allStudios = Myobject::find()->joinWith('studio')->select(['count(studio.status) as countStudio','myobject.id as id'])
            ->andWhere(['myobject.deleted'=>0])->groupBy('myobject.id')->asArray()->all();
        $rezervStudios = Myobject::find()->joinWith('studio')->select(['count(studio.status) as countStudio','myobject.id'])
            ->andWhere(['myobject.deleted'=>0])->where(['studio.status'=>0])->groupBy('myobject.id')->asArray()->all();
        $rezArr = [];
        foreach ($rezervStudios as $rezervStudio) {
            $rezArr[$rezervStudio['id']] = $rezervStudio['countStudio'];
        }
        $rezervPercent = [];
        $data1 =[];
        $data2 = [];
        $data3 =[];
        foreach ($allStudios as $studio) {
            $data1[] = (int)$studio['countStudio'];
            if (isset($rezArr[$studio['id']])) {
                $data2[] = (int)$rezArr[$studio['id']];
            } else {
                $data2[] = 0;
            }
            $data3[] = Myobject::getObjectsAddress()[$studio['id']];
        }
        $rezervPercent =[$data1,$data2,$data3];
        $rezervPercent = Dashboard::factory()->freeStudioData();
        return $this->render('index',[
            'allRequest'=>$allRequest,
            'adSourceStat'=>$adSourceStat,
            'rezervPercent'=>$rezervPercent,
            ]
            );
    }

    public function actionIndex2() {

        return $this->render('index2');
    }

    public function actionAgent() {
        $user_id = Yii::$app->user->id;
        //Количество заявок всех пользователей
        $allCountRequest = Dashboard::factory()->RequestsCount();
        //Количество моих заявок
        $myCountRequest = Dashboard::factory()->RequestsCount($user_id);
        //Заявки бездействие 7 дней
        $countDeadlineRequest = Dashboard::factory()->notActiveRequestsCount();
        // Статистика по рекламному источнику
        $adSourceStat = Dashboard::factory()->AdSourseData();
        //%свободних студий
        $freeStudio = Dashboard::factory()->freeStudioData();
        //Активность за 3 месяца
        $activeRequest=Dashboard::factory()->activeAgentByDay($user_id);
        return $this->render('agent',[
                'allCountRequest' => $allCountRequest,
                'myCountRequest' => $myCountRequest,
                'countDeadlineRequest' => $countDeadlineRequest,
                'adSourceStat'=>$adSourceStat,
                'freeStudio' => $freeStudio,
                'activeRequest'=>$activeRequest,
            ]
        );
    }

    public function actionPartner() {
        $user_id = Yii::$app->user->id;
        //Количество заявок всех пользователей
        $allCountRequest = Dashboard::factory()->RequestsCount();
        //Количество моих заявок
        $myCountRequest = Dashboard::factory()->RequestsCount($user_id);
        //Заявки бездействие 7 дней
        $countDeadlineRequest = Dashboard::factory()->notActiveRequestsCount($user_id);
        //Количество заявок без задач
        $countEmptyRequest = Dashboard::factory()->EmptyRequestsCount($user_id);
        // Статистика по рекламному источнику
        $adSourceStat = Dashboard::factory()->AdSourseData();
        //%свободних студий
        $freeStudio = Dashboard::factory()->freeStudioData();
        //Активность за месяц
        $activeRequest=Dashboard::factory()->activePartnerByDay($user_id);
        //Количество всех поступивших заявок по статусам
        $requestStatus=Dashboard::factory()->requestsStatusData($user_id);

        return $this->render('partner',[
                'allCountRequest' => $allCountRequest,
                'myCountRequest' => $myCountRequest,
                'countDeadlineRequest' => $countDeadlineRequest,
                'countEmptyRequest' => $countEmptyRequest,
                'adSourceStat'=>$adSourceStat,
                'freeStudio' => $freeStudio,

                'requestStatus'=>$requestStatus,
                'activeRequest'=>$activeRequest,
            ]
        );
    }

    public function actionManager() {
        $user_id = Yii::$app->user->id;
        $date1 = strtotime(date('01.m.Y'));
        $date2 = strtotime(date('d.m.Y'). ' + 1 days');
        $allRequest = Requests::find()->where(['>','created_at',$date1])->andWhere(['<','created_at',$date2])
            ->andWhere(['user_id'=>$user_id])->andWhere(['deleted'=>0])->all();

        //Активность за месяц
        $activeRequest=[];
        for ($i=1;$i <= (int)date('d');$i++) {
            $activeRequest[$i] = 0;
        }
        foreach ($allRequest as $request) {
            $day = (int)date('d',$request['created_at']);
            $activeRequest[$day]++;
        }
        //Количество поступивших заявок по статусам по пользователю
        $allRequest = Requests::find()->where(['>','created_at',$date1])->andWhere(['<','created_at',$date2])
            ->andWhere(['user_id'=>$user_id])->andWhere(['deleted'=>0])->select(['count(id) as countStatus','status'])->groupBy('status')
            ->orderBy('status')->indexBy('status')->asArray()->all();
        $data = [];
        foreach (Requests::getStatuses() as $key=>$item) {
            if (isset($allRequest[$key])) {
                $data[] = (int)$allRequest[$key]['countStatus'];
            } else {
                $data[] = 0;
            }
        }
        $requestStatus=['data'=>$data,'label'=>array_values(Requests::getStatuses())];
        //Количество всех поступивших заявок по статусам
        $allRequest = Requests::find()->where(['>','created_at',$date1])->andWhere(['<','created_at',$date2])
            ->andWhere(['deleted'=>0])->select(['count(id) as countStatus','status'])->groupBy('status')
            ->orderBy('status')->indexBy('status')->asArray()->all();
        $data = [];
        foreach (Requests::getStatuses() as $key=>$item) {
            if (isset($allRequest[$key])) {
                $data[] = (int)$allRequest[$key]['countStatus'];
            } else {
                $data[] = 0;
            }
        }
        $requestStatusAll=['data'=>$data,'label'=>array_values(Requests::getStatuses())];
        //Активность агентов
        $agents = array_keys(User::find()->where(['role'=>User::ROLE_AGENT])->select('id')->indexBy('id')->asArray()->all());
        $allRequest = Requests::find()->where(['>','created_at',$date1])->andWhere(['<','created_at',$date2])->andWhere(['in','user_id',$agents])
            ->andWhere(['deleted'=>0])->select(['count(id) as countStatus','user_id'])->groupBy('user_id')
            ->orderBy('user_id')->indexBy('user_id')->asArray()->all();
        $data = [];
        foreach (User::getAgentsName() as $key=>$item) {
            if (isset($allRequest[$key])) {
                $data[] = ['name' => $item,'y' => (int)$allRequest[$key]['countStatus']] ;
            } else {
                $data[] = ['name' => $item,'y' => 0] ;
            }
        }
        $activeAgent = $data;
        //Активность партнеров
        $partner = array_keys(User::find()->where(['role'=>User::ROLE_PARTNER])->select('id')->indexBy('id')->asArray()->all());
        $allRequest = Requests::find()->where(['>','created_at',$date1])->andWhere(['<','created_at',$date2])->andWhere(['in','user_id',$partner])
            ->andWhere(['deleted'=>0])->select(['count(id) as countStatus','user_id'])->groupBy('user_id')
            ->orderBy('user_id')->indexBy('user_id')->asArray()->all();

        $data = [];
        foreach (User::getPartnerName() as $key=>$item) {
            if (isset($allRequest[$key])) {
                $data[] = ['name' => $item,'y' => (int)$allRequest[$key]['countStatus']] ;
            } else {
                $data[] = ['name' => $item,'y' => 0] ;
            }
        }
        $activePartner = $data;
        //Заявки без задач
        $date3 = strtotime(date('d.m.Y'). ' - 7 days');
        $countDeadlineRequest = Requests::find()->where(['<','last_active',$date3])
            ->andWhere(['user_id'=>$user_id])->andWhere(['deleted'=>0])->count();
        //Заявки без задач
        $countEmptyRequest = Requests::find()->where(['>','created_at',$date1])->andWhere(['<','created_at',$date2])->andWhere(['count_active'=>0])
            ->andWhere(['deleted'=>0])->count();
        return $this->render('manager',[
                'allRequest'=>count($allRequest),
                'countEmptyRequest' => $countEmptyRequest,
                'requestStatus'=>$requestStatus,
                'requestStatusAll'=>$requestStatusAll,
                'activeRequest'=>$activeRequest,
                'countDeadlineRequest' => $countDeadlineRequest,
                'activeAgent'=>$activeAgent,
                'activePartner'=>$activePartner,
            ]
        );
    }

    public function actionAdmin() {
        $user_id = Yii::$app->user->id;
        $date1 = strtotime(date('01.m.Y'));
        $date2 = strtotime(date('d.m.Y'). ' + 1 days');
        //Количество заявок всех пользователей
        $allCountRequest = Dashboard::factory()->RequestsCount();
        //Количество заявок без задач
        $countEmptyRequest = Dashboard::factory()->EmptyRequestsCount();
        //Количество заявок в работе
        $countRequest = Requests::find()->where(['>','created_at',$date1])->andWhere(['<','created_at',$date2])->andWhere(['deleted'=>0])->count();
        // Статистика по рекламному источнику
        $adSourceStat = Dashboard::factory()->AdSourseData();
        //%свободних студий
        $freeStudio = Dashboard::factory()->freeStudioData();
        //Количество всех поступивших заявок по статусам
        $requestStatus=Dashboard::factory()->requestsStatusData();
        //Активность агентов
        $activeAgent = Dashboard::factory()->activeAgents();
        //Активность партнеров
        $activePartner = Dashboard::factory()->activePartner();
        //Заявки бездействие 7 дней
        $countDeadlineRequest = Dashboard::factory()->notActiveRequestsCount();
        return $this->render('admin',[
                'allCountRequest' => $allCountRequest,
                'countRequest'=>$countRequest,
                'countEmptyRequest' => $countEmptyRequest,
                'countDeadlineRequest' => $countDeadlineRequest,

                'adSourceStat'=>$adSourceStat,
                'freeStudio' => $freeStudio,

                'activeAgent'=>$activeAgent,
                'activePartner'=>$activePartner,
                'requestStatus'=>$requestStatus,

            ]
        );
    }
	
}

