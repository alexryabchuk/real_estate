<?php

namespace app\modules\admin\controllers;

use app\models\Comments;
use app\models\User;
use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','update','create','delete','bulk-delete','delete-comment'],
                        'allow' => true,
                        'matchCallback' => function($rule, $action){
                            return in_array(yii::$app->user->getIdentity()->getRole(), [User::ROLE_ADMIN]);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Статья " . $model->name,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Article();
        if ($model->load($request->post())) {
            $model->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $comments = Comments::find()->where(['post_id' => $id])->asArray()->all();
        $_comments = array();
        foreach ($comments as $comment) {
            $_comments[$comment['id']] = $comment;
        }
        $comments = build_tree($_comments);
        unset($_comments);
        $comments = getCommentsTemplate($comments);
        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'comments' => $comments,
            ]);
        }
    }


    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $item = $this->findModel($id);
        $item->deleted == 1 ? $item->deleted = 0 : $item->deleted = 1;
        $item->save();
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionDeleteComment($id)
    {
        $comment = Comments::findOne($id);
        $comment->delete();
    }

    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

function getCommentsTemplate($comments)
{

    $html = '';
    foreach ($comments as $comment) {
        $html .= '<li><div class="comment"><img src="/img/comment-av.png" style="float: left"><div><div class="author">' . $comment['author'] . '<span class="date"><i class="fa fa-calendar-o"></i> ';
        $html .= date('d.m.Y ', $comment['created_at']) . '</span><span class="time"><i class="fa fa-clock-o"></i> ' . date('H:i', $comment['created_at']);
        $html .= '</span> </div><div class="comment_text">' . $comment['comment'] ;
        $html .= '</div><div class="comment-answer"><img src="/img/detail-ico.png"><a href="#" onclick="deletecomment('.$comment['id'].')" class="delete-comment">Удалить</a></div></div></div>';
        if (!empty($comment['childs'])) {
            $html .= '<ul>' . getCommentsTemplate($comment['childs']) . '</ul>';
        }

        $html .= '</li>';
    }

    return $html;
}

function build_tree($data)
{
    $tree = array();
    foreach ($data as $id => &$row) {
        if (empty($row['parent_id'])) {
            $tree[$id] = &$row;
        } else {
            $data[$row['parent_id']]['childs'][$id] = &$row;
        }
    }
    return $tree;
}
