<?php

namespace app\modules\admin\controllers;


use app\models\User;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;


class ProfileController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $model = User::findOne(Yii::$app->user->id);
        if ($model->load($request->post())) {
            if (($model->newpassword != '') )  {
                if (($model->password != '') &&(!$model->validatePassword($model->password))) {
                    Yii::$app->session->setFlash('error','Неверный пароль');
                } else {
                    $model->updateUser();
                    Yii::$app->session->setFlash('success','Данние успешно изменнены.');
                }
            } else {
                $model->updateUser();
                Yii::$app->session->setFlash('success','Данние успешно изменнены.');
            }
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }


}
