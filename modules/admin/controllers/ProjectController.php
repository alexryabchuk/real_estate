<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Colors;
use app\modules\admin\models\KeywordGroups;
use app\modules\admin\models\Keywords;
use app\modules\admin\models\Task;
use app\modules\admin\models\TzBinetConnect;
use moonland\phpexcel\Excel;
use Yii;
use app\modules\admin\models\Project;
use app\modules\admin\models\ProjectSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    const MAX_IMPORT = 35;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Project();
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->set('current_project_id',$model->id);

            return $this->actionIndex();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $count = TzBinetConnect::find()->where(['project_id' => $model->id])->count();

        if($count == 0){
            $model->tz_binet = null;
            $model->save();
        }
        if($count == 1){
            $model->tz_binet = Project::BINET_ONE;
            $model->save();
        }
        if($count > 1){
            $model->tz_binet = Project::BINET_MULTIPLE;
            $model->save();
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $count = TzBinetConnect::find()->where(['project_id' => $model->id])->count();

            if($count == 0){
                $model->tz_binet = 0;
                $model->save();
            }
            if($count == 1){
                $model->tz_binet = Project::BINET_ONE;
                $model->save();
            }
            if($count > 1){
                $model->tz_binet = Project::BINET_MULTIPLE;
                $model->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionImport()
    {

        $model = new Project();

        if($model->load(Yii::$app->request->post())) {
            ini_set('memory_limit', '-1');  
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file && $model->validate()) {
                $path = Yii::getAlias('@webroot')."/upload";
                if (\yii\helpers\FileHelper::createDirectory($path, $mode = 0775, $recursive = true)) {
                    $model->file->saveAs($path.'/'. $model->file->baseName . '.' . $model->file->extension);
                }
            }
            $fileName = Yii::getAlias('@webroot').'/upload/'. $model->file->baseName . '.' . $model->file->extension;
            $data = \moonland\phpexcel\Excel::import($fileName,[
                'setFirstRecordAsKeys' => false, // if you want to set the keys of record column with first record, if it not set, the header with use the alphabet column on excel.
            ]);
            unset($data[0][1]);
            $group_id = (new \yii\db\Query())
                ->select(['max(connect_id)'])
                ->from('keyword_groups')
                ->scalar();
            $group_id += 11;
            $current_group = null;
            $count = 0;
            $color_count = 0;
            $keywords=[];
            foreach ($data[0] as $d)
            {
                if(isset($d['A']) || isset($d['B']) ){
                    if(isset($d['A'])){
                        $color_count++;
                        $group = new KeywordGroups();
                        $group->name = $d['A'];
                        $group->status = KeywordGroups::STATUS_ISXODNIE_GRUPPI;
                        $group->project = $_POST['modelid'];
                        $group->connect_id = $group_id;
                        $group->color = $color_count;
                        if($color_count == 10){
                            $color_count = 0;
                        }
                        $group->save(false);
                        $current_group = $group;
                    }
                    if(isset($d['B'])){
                        $count++;


                        if ($count<=self::MAX_IMPORT) {
                            $keywords[$count]['keyword'] = $d['B'];
                            $keywords[$count]['frequency'] = $d['C'];
                            $keywords[$count]['group_id'] = $group_id;
                            $current_group->main_keywords .= " ".$d['B']."\n";
                            $current_group->frequency += (int)$d['C'];
                        } else {
                            $current_group->add_keywords.= " ".$d['B']."\n";
                        }
                        if($count == 1){
                            if((int)$d['C'] > 300){
                                $current_group->import_key = 2;
                            }
                            if((int)$d['C'] <= 300){
                                $current_group->import_key = 1;
                            }
                        }

                    }
                }else{
                    Yii::$app->db->createCommand()->batchInsert('keywords', ['keyword', 'frequency','group_id'],
                        $keywords)->execute();

                    $current_group->save(false);
                    $keywords=[];
                    $count = 0;
                    $group_id = $group_id + 11;
                }
            }
            unlink($fileName);
            Yii::$app->getSession()->setFlash('success', 'Файл импортирован успешно!');
            Yii::$app->db->createCommand()->batchInsert('keywords', ['keyword', 'frequency','group_id'],
                $keywords)->execute();

            $current_group->save(false);
            return $this->redirect(['index']);
        }
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $project_id = $id;
        $tasks = Task::findAll(['project_id' => $project_id]);
        foreach ($tasks as $task) {
            $task->delete();
        }

        $this->findModel($id)->delete();

        $groups = KeywordGroups::findAll(['project' => $project_id]);
        foreach ($groups as $group){
            $keys = Keywords::findAll(['group_id' => $group->connect_id]);
            foreach ($keys as $key){
                $key->delete();
            }
            $group->delete();
        }

        if(Yii::$app->session->get('current_project_id') == $id)
        {
           Yii::$app->session->remove('current_project_id');

        }

         Yii::$app->getSession()->setFlash('success', 'Проект удален!');

        return $this->redirect(['index']);
    }

    public function actionRemoveNotUse()
    {
        $subQuery = Project::find()->select('id');
        $groups = KeywordGroups::find()->where(['not in', 'project', $subQuery])->all();

        foreach ($groups as $group){

            $keys = Keywords::find()->where(['group_id' => $group->connect_id])->all();
            foreach ($keys as $key){
                $key->delete(true);
            }

            $group->delete(true);
        }

        Yii::$app->getSession()->setFlash('success', 'Очистька прошла успешно!');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
