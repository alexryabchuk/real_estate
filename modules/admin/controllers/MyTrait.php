<?php
/**
 * Created by PhpStorm.
 * User: Abbos
 * Date: 10/29/2018
 * Time: 10:31 PM
 */

namespace app\modules\admin\controllers;


use app\modules\admin\models\KeywordGroups;
use app\modules\admin\models\Keywords;
use Yii;

trait MyTrait
{
    public function actionGroup()
    {

        foreach ($_POST['keylist'] as $key){
            $model = Keywords::findOne(['id' => $key]);
            $model->group_id = $_POST['id'][0];
            $model->color = 0;
            $model->save();

            $old_group = KeywordGroups::findOne(['id' => $_POST['model']]);
            $old_group->frequency -= $model->frequency;
            $old_group->save();

            $new_group = KeywordGroups::findOne(['connect_id' => $_POST['id'][0]]);
            $new_group->project = $old_group->project;
            $new_group->frequency += $model->frequency;
            $new_group->save();
        }
        Yii::$app->getSession()->setFlash('success', 'Ключевики перешли  в другую группу');
        return true;
    }


    public function actionDeleteKey()
    {
        foreach ($_POST['keylist'] as $key) {

            $model = Keywords::findOne(['id' => $key]);
            $gid = $model->group_id;
            $model->status = Keywords::STATUS_DELETED;
            $model->group_id = null;
            $model->save(false);
            $group = KeywordGroups::findOne(['connect_id' => $gid]);
            $group->frequency -= $model->frequency;
            $group->save(false);

        }
        Yii::$app->getSession()->setFlash('success','Ключевики удалены!' );



        return true;
    }

    public function actionSetDefault()
    {
        $keywds = Keywords::find()->where(['group_id' => $_POST['id']])->all();
        foreach ($keywds as $keys){
            $keys->color = 0;
            $keys->save();
        }
        Yii::$app->getSession()->setFlash('success', 'Успешно');
        return true;
    }
}