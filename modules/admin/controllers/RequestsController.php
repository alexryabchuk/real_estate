<?php

namespace app\modules\admin\controllers;

use app\models\Client;
use app\models\ClientLog;
use app\models\Myobject;
use app\models\RequestsComment;
use app\models\RequestsCommentSearch;
use app\models\RequestsLog;
use app\models\RequestsObjects;
use app\models\User;
use Yii;
use app\models\Requests;
use app\models\RequestsSearch;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * RequestsController implements the CRUD actions for Requests model.
 */
class RequestsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','new','create'],
                        'allow' => true,
                        'matchCallback' => function($rule, $action){
                            return in_array(yii::$app->user->getIdentity()->getRole(), [User::ROLE_ADMIN,User::ROLE_MANAGER,User::ROLE_AGENT,User::ROLE_PARTNER]);
                        }
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'matchCallback' => function($rule, $action){
                            return in_array(yii::$app->user->getIdentity()->getRole(), [User::ROLE_ADMIN,User::ROLE_MANAGER]);
                        }
                    ],
                    [
                        'actions' => ['create','delete','bulk-delete'],
                        'allow' => true,
                        'matchCallback' => function($rule, $action){
                            return in_array(yii::$app->user->getIdentity()->getRole(), [User::ROLE_ADMIN,]);
                        }
                    ],
                ],

            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
               //     'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Requests models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequestsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Requests model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Заявка: " . $model->id,
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Закрыть', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-info', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    public function actionHistory($id)
    {
        $model = RequestsLog::find()->where(['requests_id' => $id])->all();
        return $this->render('history', [
            'model' => $model,
        ]);
    }
    /**
     * Creates a new Requests model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Requests();
        $model->user_id = Yii::$app->user->id;

        if ($model->load($request->post()) && $model->validate()) {

            if ($model->client_id == 0) {
                $client = new Client();
                $client->firstname = $model->client_firstname;
                $client->lastname = $model->client_lastname;
                $client->phone = $model->phone;
                $client->save();
                $model->client_id = $client->id;
            }

            if ($model->myobjects) {
                foreach ($model->myobjects as $myobject) {
                    if (Requests::issetRequest($model->client_id,$myobject)) {
                        Yii::$app->session->setFlash('error', "Заявка существует: ".Client::getClients()[$model->client_id].' Обьект: '.Myobject::getObjectsAddress()[$myobject]);
                    } else {
                        $item = new Requests();
                        $item->client_id = $model->client_id;
                        $item->user_id = $model->user_id;
                        $item->status = $model->status;
                        $item->ad_source_id = $model->ad_source_id;
                        $item->comment = $model->comment;
                        $item->myobject_id = $myobject;
                        $item->save(false);
                    }


                };
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
//        }

    }

    public function actionCreateComment($id)
    {
        $request = Yii::$app->request;
        $model = new RequestsComment();
        $model->user_id = Yii::$app->user->id;
        $model->requests_id = $id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать заметку",
                    'content' => $this->renderAjax('create-comment', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Создать заметку",
                    'content' => '<span class="text-success">Create Requests success</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать еще', ['create-comment'], ['class' => 'btn btn-info', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Создать заметку",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Requests model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $searchModel = new RequestsCommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['requests_id'=>$id]);
        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
//        }
    }

    public function actionUpdateComment($id)
    {
        $request = Yii::$app->request;
        $model = RequestsComment::findOne($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить заметку: ".$id,
                    'content'=>$this->renderAjax('create-comment', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
            }else{
                 return [
                    'title'=> "Изменить заявку: ".$id,
                    'content'=>$this->renderAjax('create-comment', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];
            }
        }else{
        /*
        *   Process for non-ajax request
        */
        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
        }
    }

    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $item = $this->findModel($id);
        $item->deleted = 1;
        $item->new_client = 0;
        $item->save();
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionDeleteComment($id)
    {

        $request = Yii::$app->request;
        $comment = RequestsComment::findOne($id);
        $comment->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Requests model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Requests the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Requests::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
