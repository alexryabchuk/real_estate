<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\export\ExportProcessor;
use app\modules\admin\models\ExportForm;
use app\modules\admin\models\ProjectSearch;
use app\modules\admin\models\Task;
use app\modules\admin\models\TaskGroupCreation;
use app\modules\admin\models\TaskSearch;
use app\modules\admin\models\TzBinetConnect;
use Yii;
use yii\base\Exception;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->actionProjectsStatus(2);
    }

    public function actionProjectsStatus($status = false)
    {
        $projectsIds = (new Query())
            ->select('task.project_id AS id, project.name')
            ->distinct()
            ->from('task')
            ->join('LEFT JOIN', 'project', 'project_id = project.id');

        if ($status) {
            $projectsIds = $projectsIds->where(['task.status' => $status]);
        }

        $projectsIds = $projectsIds->orderBy('project.name')
            ->all();

        foreach ($projectsIds as $key => &$projectsId) {
            $projectsId = (int)$projectsId['id'];
        }

        if (!$projectsIds) {
            $projectsIds = ['-1'];
        }

        $searchModel = new ProjectSearch();
        $searchModel->projectsIds = $projectsIds;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('projectsstatus', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => $status ? Task::getStatusesLabel()[$status] : 'Все ТЗ',
            'status' => $status,
        ]);
    }


    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionAllTasks()
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStatus1()
    {

        $searchModel = new TaskSearch();
        $searchModel->statusParam = '1';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('status1', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionStatus2()
    {

        $searchModel = new TaskSearch();
        $searchModel->statusParam = '2';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('status2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * @return string|\yii\web\Response
     * @throws Exception
     */
    public function actionStatus3()
    {
        if (Yii::$app->request->isPost) {


//            $form = new ExportForm();
//            if ($form->load(Yii::$app->request->post())) {
//                $taskIds = $form->tasksIdList;
//
//                /**
//                 * @var Task[] $tasks
//                 */
//                $tasks = Task::find()->where(['in', 'id', $taskIds])->all();
//
//                foreach ($tasks as $task) {
//                    $newTask = Task::copy($task);
//                    $newTask->status = Task::STATUS_DONE;
//                    $newTask->project_id = $form->projectId;
//                    if (!$newTask->save()) {
//                        throw new Exception('Чтото не так с $newTask. Вероятно не проходит валидацию');
//                    }
//
//                }
//                return $this->redirect(['task/status2', 'project_id' => $form->projectId]);
//            }

//            echo "<pre>";
//            print_r($tasks);
//            echo "</pre>";
//            exit();

            $from = $_GET['TaskSearch']['project_id'];
            $to = Yii::$app->request->post()['ExportForm']['projectId'][0];
            $from_status = Task::STATUS_USED;
            $to_status = Task::STATUS_DONE;

            $tasks = Task::find()->where(['project_id' => $from])->andWhere(['status' =>
                    $from_status])->all();



            foreach ($tasks as $task){
                $clone = new Task();
                $clone->setAttributes($task->attributes);
                $clone->project_id = $to;
                $clone->additional_keywords = $task->additional_keywords;
                $clone->status = $to_status;
                $clone->save();
            }
            Yii::$app->getSession()->setFlash('success', 'Экспортирование завершено успешно!');
            return $this->redirect(['task/status2', 'project_id' => $from]);


        }

        $searchModel = new TaskSearch();
        $searchModel->statusParam = Task::STATUS_USED;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('status3', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionStatus4()
    {
        $searchModel = new TaskSearch();
        $searchModel->statusParam = '4';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('status4', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUsed()
    {

        $keylist = Yii::$app->request->post('keylist');

        foreach ($keylist as $key) {
            $task = Task::findOne($key);
            $task->status = '3';
            $task->save();
        }

        return 'Статусы обновлены.';
    }

    public function actionFire()
    {

        $keylist = Yii::$app->request->post('keylist');

        foreach ($keylist as $key) {
            $task = Task::findOne($key);
            $task->fire = 1;
            $task->save(false);
        }

        return 'Пометка установлена.';
    }

    public function actionGroup()
    {

        $keylist = Yii::$app->request->post('keylist');

        $group_id = (new \yii\db\Query())
            ->select(['max(group_id)'])
            ->from('task')
            ->scalar();

        $next_group_id = $group_id + 1;

        foreach ($keylist as $key) {
            $task = Task::findOne($key);
            $task->group_id = $next_group_id;
            $task->save();
        }

        return 'ТЗ объединены в группу.';
    }

    public function actionDeleteSelectedTasks()
    {

        $keylist = Yii::$app->request->post('keylist');

        foreach ($keylist as $key) {
            Task::findOne($key)->delete();
        }

        return 'ТЗ удалены.';
    }

    public function actionRebuildSelectedTasks()
    {

        $keyList = Yii::$app->request->post('keylist');

        foreach ($keyList as $key) {
            $task = Task::findOne($key);
            $task->status = Task::STATUS_IN_WORK;
            $task->save();
        }

        return 'ТЗ отправлены на доработку.';
    }

    public function actionToModeration()
    {
        if(isset($_POST['model_id'])){
            $model = Task::findOne(['id' => $_POST['model_id']]);

            if($model->status == Task::STATUS_IN_WORK){
                $model->status = Task::STATUS_SOURCE_KEYS;
            }
            if($model->status == Task::STATUS_DONE){
                $model->status = Task::STATUS_IN_WORK;
            }
            if($model->status == Task::STATUS_USED){
                $model->status = Task::STATUS_DONE;
            }

            $model->save();
            Yii::$app->getSession()->setFlash('success', 'Группа отправлень на доработку!');
            return $this->redirect(['task/projects-status','status' => $model->status]);
        }

    }

    public function actionChangeDeadline()
    {

        $keyList = Yii::$app->request->post('keylist');
        $deadline = Yii::$app->request->post('deadline');

        if (!$keyList) {
            return 'Вы не выбрали ТЗ';
        }

        foreach ($keyList as $key) {
            $task = Task::findOne($key);
            $task->deadline = $deadline;
            $task->save();
        }

        return 'Дедлайны изменены.';
    }

    public function actionExportTask()
    {

        $idList = Yii::$app->request->post('keylist');

        if (empty($idList)) {
            return 'Отметьте флажками необходимые ТЗ.';
        }

        /**
         * @var Task[] $tasks
         */
        $tasks = Task::find()
            ->where(['id' => $idList])
            ->all();

        $taskText = '';

        foreach ($tasks as $task) {
            if($task->import_key == 3){
                $taskText = $taskText . PHP_EOL . PHP_EOL."\t". "ВЧ";
            }
            $taskText = $taskText .
                ($taskText == '' ? '' : PHP_EOL . PHP_EOL) .
                $task->main_keywords . PHP_EOL . '!' . PHP_EOL . $task->task_text;
        }

        $processor = new ExportProcessor();
        return $processor->process($taskText);
    }

    public function actionSearch()
    {

        $search_text = Yii::$app->request->get('text');

        $tasks = (new \yii\db\Query())->select([
            'id',
            'name'
        ])
            ->from('task')
            ->where(['like', 'main_keywords', $search_text])
            ->orWhere(['like', 'additional_keywords', $search_text])
            ->orWhere(['like', 'task_text', $search_text])
            ->orderBy('id')
            ->all();

        return $this->render('search', [
            'tasks' => $tasks,
        ]);

    }

    public function actionTest()
    {


    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'previousPageUrl' => Yii::$app->request->referrer,
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Task();
        if ($model->load(Yii::$app->request->post())) {
            $model->task_text = rtrim(ArrayHelper::getValue(Yii::$app->request->post(), 'Task.task_text'));

            if ($model->isNewRecord && !$model->deadline) {
                $model->deadline = date('Y-m-d', strtotime("+3 days"));
            }

            if ($model->deadline && !$model->isNewRecord) {
                if (!stripos($model->deadline, '-')) {
                    $model->deadline = date_create_from_format('d.m.Y', $model->deadline)->format('Y-m-d');
                }
            }

            $strings = explode(PHP_EOL, $model->main_keywords);

            $name = '';
            if (isset($strings[0]) && !is_numeric(trim($strings[0]))) {
                $name = trim($strings[0]);
            } elseif (isset($strings[1]) && !is_numeric(trim($strings[1]))) {
                $name = trim($strings[1]);
            }

            $model->name = $name;
            $model->user_id = Yii::$app->user->identity->getId();
            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'previousPageUrl' => Yii::$app->request->referrer,
        ]);
    }

    public function actionGroupCreation()
    {

        $model = new TaskGroupCreation();

        if ($model->load(Yii::$app->request->post()) && $model->createMany()) {
            return $this->redirect(['projects-status?status=1']);
        }

        return $this->render('group-creation', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {

            $date = str_replace('.',"/",Yii::$app->request->post()['Task']['deadline']);

            $date= date_create($date);
            $date = date_format($date,"Y/m/d");
            $model->deadline = $date;
            if($model->status == Task::STATUS_SOURCE_KEYS){
                $model->status = Task::STATUS_IN_WORK;
            }

            if($model->status == Task::STATUS_IN_WORK){
                $model->status = Task::STATUS_DONE;
            }


//            if($model->status == Task::STATUS_DONE ){
//                $model->status = Task::STATUS_USED;
//            }

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'previousPageUrl' => Yii::$app->request->referrer,
        ]);
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public static function generatePreviousUrl($taskId)
    {
        if (($model = Task::findOne($taskId)) !== null) {

            return ['task/status' . $model->status, 'TaskSearch[project_id]' => $model->project->id];
        }

        return ['index'];
    }


    public function actionLists($id)
    {
        $datas = TzBinetConnect::find()->where(['project_id' => $id ])->all();
        foreach($datas as $data){
            echo "<option value = '".$data->name."'>".$data->name."</option>" ;
        }
    }


}
