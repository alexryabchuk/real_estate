<?php

namespace app\modules\admin\widgets;

use app\modules\admin\models\Task;
use yii\bootstrap\Html;

class CommentCountWidget extends \yii\bootstrap\Widget
{
    /**
     * @var Task $task
     */
    public $task;

    public function run()
    {
        $count = $this->task->getCommentsCountAsInteger();
        $icon = Html::tag('i', null, ['class' => 'fa fa-comments']);

        $countOptions = $count > 0 ? ['class' => 'count-red'] : [];
        $countHtml = Html::tag('span', $count, $countOptions);

        return "&nbsp;(&nbsp;$icon&nbsp;$countHtml&nbsp;)&nbsp;";
    }
}
