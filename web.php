<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'homeUrl' => '/',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'cssOptions' => [
                        'position' => \yii\web\View::POS_READY
                    ],
                ],
            ],
        ],
        'view' => [
            'class' => '\rmrevin\yii\minify\View',
            'enableMinify' => false, //!YII_DEBUG,
            'concatCss' => false, // concatenate css
            'minifyCss' => false, // minificate css
            'concatJs' => false, // concatenate js
            'minifyJs' => false, // minificate js
            'minifyOutput' => false, // minificate result html page
            'webPath' => '@web', // path alias to web base
            'basePath' => '@webroot', // path alias to web base
            'minifyPath' => '@webroot/minify', // path alias to save minify result
            'jsPosition' => [ \yii\web\View::POS_END ], // positions of js files to be minified
            'forceCharset' => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
            'expandImports' => true, // whether to change @import on content
            'compressOptions' => ['extra' => true], // options for compress
            ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@app/messages',
                'fileMap' => [
                    'app' => 'app.php',
                ]
            ],
            ],
        ],
        'user' => [
            'loginUrl' => '/Admin/Login',
            'identityClass' => 'app\models\Admin',
            'enableAutoLogin' => true,
        ],
        'request' => [
            'cookieValidationKey' => 'DSFgksdifhiw899734hekfDFGisjdfi9374',
            'baseUrl' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'exception/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class' => 'app\components\LangUrlManager',
            'rules' => [
                '/' => 'game/index',
                '/Games/Index/<page:\d+>'=>'/game/index',
                '/Games/Index' => 'game/index',
                '/Games/Search' => 'game/search',
                '/Games/Vote' => 'game/vote',
                '/Games/Order' => 'game/order',
                '/Game/<alias:.+>/<id:\d+>' => 'game/view',
                '/Games/Info' => 'game/info',
            //    '/4-kartinki-1-slovo-Otvety'=>'game/newpage',
			//	'/4-kartinki-1-slovo-Otvety'=>'game/newpage',
                '/4-foto-1-slovo-otvety'=>'game/newpage2',
                '/Admin' => 'admin/index/index',
                '/admin' => 'admin/index/index',
                '/Admin/Logout' => 'admin/index/logout',
                '/Admin/Login' => 'admin/index/login',
                '/Admin/Games' => 'admin/game/test',
                '/Admin/Games/test' => 'admin/game/index',
                '/Admin/Games/Create' => 'admin/game/create',
                '/Admin/Games/<id:\d+>' => 'admin/game/edit',
                '/Admin/Games/<id:\d+>/Levels' => 'admin/game/levels',
                '/Admin/Games/<id:\d+>/Remove' => 'admin/game/remove',
                '/Admin/Level/<id:\d+>' => 'admin/level/edit',
                '/Admin/Level/<id:\d+>/Remove' => 'admin/level/remove',
                '/Admin/Level/<id_level:\d+>/RemovePhotos' => 'admin/level/remove-photos',
                '/Admin/Games/<id_game:\d+>/RemovePhotos' => 'admin/level/remove-photos',
                '/Admin/Level/<id:\d+>/Photos' => 'admin/level/photos',
                '/Admin/Level/<id_level:\d+>/Photos/Create' => 'admin/photo/create',
                '/Admin/Games/<id_game:\d+>/Photos/Create' => 'admin/photo/create',
                '/Admin/Games/<id_game:\d+>/Level/Create' => 'admin/level/create',
                '/Admin/Photo/<id:\d+>' => 'admin/photo/edit',
                '/Admin/Photo/<id:\d+>/Remove' => 'admin/photo/remove',
                '/Admin/Photo/Editajax' => 'admin/photo/editajax',
                '/Admin/Stat' => 'admin/stat/index',
                'sitemap2.xml' => 'sitemap/index',
                '/<slug>' => 'game/slug',
                '/<slug>/otvetu_k_igre_<alias:.+>' => 'game/slug',
            ],
        ]
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module'
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;