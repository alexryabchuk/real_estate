<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $name Заголовок
 * @property string $photo Фото
 * @property int $created_at
 * @property int $updated_at
 * @property int $category Категория
 * @property string $description Текст новости
 * @property string $SEO_TITLE SEO TITLE
 * @property string $SEO_KEY SEO KEY
 * @property string $SEO_DESCRIPTION SEO DESCRIPTION
 * @property string $SEO_TAGS Теги
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'category_id'], 'integer'],
            [['short_description','description'], 'string'],
            [['name', 'photo', 'SEO_TITLE'], 'string', 'max' => 255],
            [['SEO_KEY', 'SEO_DESCRIPTION', 'SEO_TAGS'], 'string', 'max' => 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Заголовок',
            'photo' => 'Фото',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'category_id' => 'Категория',
            'short_description' => 'Краткое описание',
            'description' => 'Текст статьи',
            'SEO_TITLE' => 'SEO TITLE',
            'SEO_KEY' => 'SEO KEY',
            'SEO_DESCRIPTION' => 'SEO DESCRIPTION',
            'SEO_TAGS' => 'Теги',
            'deleted' => 'Показывать на сайте'
        ];
    }

    public static function getCategoryWithCount()
    {
        $cat_article = static::find()->select(['category_id'=>'category_id','category_count'=>'count(category_id)'])->where(['deleted'=>0])->groupBy(['category_id'])->asArray()->all();
        $data = [];
        foreach ($cat_article as $item) {
            $data[] = ['category_id'=>$item['category_id'],'category_count'=>$item['category_count'],'name'=>CategoryArticle::getCategories()[$item['category_id']]];
        }
        return $data;
    }

    public static function getLastArticle()
    {
        $article = static::find()->orderBy('created_at DESC')->limit(3)->asArray()->all();
        return $article;
    }
}
