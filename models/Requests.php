<?php

namespace app\models;

use Amp\Artax\Request;
use app\service\Notify;
use Mpdf\Tag\S;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "requests".
 *
 * @property int $id
 * @property int $client_id Клиент
 * @property int $user_id Пользователь
 * @property int $status Статус
 * @property int $ad_source_id Источник
 * @property string $comment Коментарий
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 * @property integer $myobject_id
 *
 * @property AdSource $adSource
 * @property Client $client
 * @property User $user
 */
class Requests extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requests';
    }

    private $oldattr;

    public $myobjects = [];
    public $client_firstname;
    public $client_lastname;
    public $phone;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'user_id', 'status', 'ad_source_id', 'deleted', 'myobject_id'], 'integer'],
            [['comment'], 'string', 'max' => 512],
            [['myobjects', 'new_client', 'client_firstname', 'client_lastname', 'client_phones', 'phone', 'status'], 'safe'],
            [['ad_source_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdSource::className(), 'targetAttribute' => ['ad_source_id' => 'id']],

            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
//            ['client_firstname', 'required'],
//            ['client_lastname', 'required'],
            ['client_firstname', 'required', 'when' => function ($model) {
                return $model->client_id == 0;
            }, 'enableClientValidation' => false],
            ['client_lastname', 'required', 'when' => function ($model) {
                return $model->client_id == 0;
            }, 'enableClientValidation' => false],
            //['phone',  'unique', 'when' => function($model) {return Client::find()->where(['phone'=>$model->phone])->all();}, 'enableClientValidation' => false],
            [['phone'], 'string', 'min' => 16],
            ['phone', 'required', 'when' => function ($model) {
                return $model->client_id == 0;
            }, 'enableClientValidation' => false],

        ];
    }

    public function validatePhoneEmailEmpty()
    {
        $client = Client::find()->where(['phone' => $this->phone])->all();
        if ($client && $this->new_client == 1) {
            $errorMsg = 'Клиент с таким телефоном существует';
            $this->addError('phone', $errorMsg);
        }

    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Клиент',
            'user_id' => 'Пользователь',
            'status' => 'Статус',
            'ad_source_id' => 'Источник',
            'comment' => 'Коментарий',
            'myobjects' => 'Объекты',
            'client_firstname' => 'Имя клиента',
            'phone' => 'Телефон',
            'client_lastname' => 'Фамилия клиента',
            'created_at' => 'Дата',
            'updated_at' => 'Изменен',
            'deleted' => 'В архиве',
            'myobject_id' => 'Объект',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdSource()
    {
        return $this->hasOne(AdSource::className(), ['id' => 'ad_source_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    public function getClientPhone()
    {
        return $this->client->phone;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getStatuses()
    {
        return [
            0 => 'Создана, записать на показ',
            10 => 'Передал на показ',
            20 => 'Не устраивает доля',
            30 => 'Ипотека, мат.капитал',
            40 => 'Покупка',
            50 => 'Бронь, передать юристу',
            60 => 'Не обработано',
            70 => 'Назначен звонок',
            80 => 'Біл показ, перезвонить',
            90 => 'Вернуть агенту, см. коммент',
            100 => 'Отказался от показа, перезвонить',
            110 => 'Купил другое',
        ];
    }

    public static function getColorStatuses()
    {
        return [
            0 => 'c0c0c0',
            10 => '007abf',
            20 => 'c00000',
            30 => 'ff0000',
            40 => 'ff644e',
            50 => '61d836',
            60 => 'bbbcbb',
            70 => 'ef5fa7',
            80 => 'ffff00',
            90 => '7030a0',
            100 => '2f7116',
            110 => '000000',
        ];
    }

    public function afterFind()
    {
        parent::afterFind(); // TODO: Change the autogenerated stub
        $this->oldattr = $this->attributes;
    }

    public function afterSave($insert, $changedAttributes)
    {

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub

        if ($insert) {
            $log = new RequestsLog();
            $log->requests_id = $this->id;
            $log->user_id = Yii::$app->user->id;
            $log->data = 'Добавлена запись: Объект: ' . Myobject::getObjectsAddress()[$this->myobject_id];
            $log->save();
            Yii::error(serialize($log->errors));
            $this->sendNotifyAdd();
        } else {
            $change = '';
            if ($this->oldattr['status'] !== $this->status) {
                $change .= 'Статус: ' . static::getStatuses()[$this->oldattr['status']] . ' --> ' . static::getStatuses()[$this->status] . '; ';
            }
            if ($change !== '') {
                $log = new RequestsLog();
                $log->requests_id = $this->id;
                $log->user_id = Yii::$app->user->id;
                $log->data = 'Изменена запись: ' . $change;
                $log->save();
                Yii::error(serialize($log->errors));
                $this->sendNotifyChange();
            }
        }
    }

    public static function issetRequest($client_id, $myobject_id)
    {
        return static::find()->where(['client_id' => $client_id])->andWhere(['myobject_id' => $myobject_id])->one();
    }

    private function sendNotifyAdd()
    {
        $myobject = Myobject::findOne($this->myobject_id);
        $client = Client::findOne($this->client_id);
        $message = $client->firstname.' '.$client->lastname.' хочет записаться на просмотр объекта '.$myobject->metro.' '.$myobject->address;
        $message .='.Телефон клиента '.$client->phone;
        $notifySetting = ConversationSettings::findOne('add_request');
        if ($notifySetting->conversation_inner) {
            Notify::sendInnerNotify($myobject->user_id,$message);
        }
        if ($notifySetting->conversation_email) {
            Notify::sendEmailNotify(User::getUserEmail($myobject->user_id),$message);
        }
        if ($notifySetting->conversation_telegram) {
            Notify::sendTelegramNotify($myobject->user_id,$message);
        }

    }

    private function sendNotifyChange()
    {
        $myobject = Myobject::findOne($this->myobject_id);
        $client = Client::findOne($this->client_id);
        $message = 'По заявке от '. $client->firstname.' '.$client->lastname.' изменен статус по заявке по объекту '.$myobject->metro.' '.$myobject->address;
        $message .='.Телефон клиента '.$client->phone.', новый статус '.static::getStatuses()[$this->status];
        Notify::sendInnerNotify($this->user_id,$message);
        Notify::sendInnerNotifyToAdmin($message);
    }


}
