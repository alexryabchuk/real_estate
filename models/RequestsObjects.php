<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requests_objects".
 *
 * @property int $id
 * @property int $requests_id
 * @property int $myobject_id
 *
 * @property Myobject $myobject
 * @property Requests $requests
 */
class RequestsObjects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requests_objects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['requests_id', 'myobject_id'], 'integer'],
            [['myobject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Myobject::className(), 'targetAttribute' => ['myobject_id' => 'id']],
            [['requests_id'], 'exist', 'skipOnError' => true, 'targetClass' => Requests::className(), 'targetAttribute' => ['requests_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'requests_id' => 'Requests ID',
            'myobject_id' => 'Myobject ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyobject()
    {
        return $this->hasOne(Myobject::className(), ['id' => 'myobject_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasOne(Requests::className(), ['id' => 'requests_id']);
    }
}
