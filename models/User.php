<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $role
 * @property integer $firstname
 * @property integer $lastname
 * @property integer $phone
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const ROLE_ADMIN = 0;
    const ROLE_MANAGER = 1;
    const ROLE_AGENT = 2;
    const ROLE_PARTNER = 3;

    public $password;
    public $newpassword;

    public static function tableName()
    {
        return '{{%user}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [
            [['username', 'email','auth_key', 'password_hash', 'firstname', 'lastname', 'phone', 'role'], 'required'],
            [['password', 'newpassword','nik_telegram'],'safe'],
            [['username','email'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Имя пользователя',
            'email' => 'E-mail',
            'status' => 'Статус',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
            'role' => 'Права',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'phone' => 'Телефон',
            'password' => 'Пароль',
            'newpassword' => 'Новый пароль',
            'nik_telegram' =>'Telegram_id',

        ];
    }


    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }


    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public static function findByPasswordResetToken($token)
    {

        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {

        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function getRoles()
    {
        return [
            self::ROLE_ADMIN => 'Администратор',
            self::ROLE_MANAGER => 'Менеджер',
            self::ROLE_AGENT => 'Агент',
            self::ROLE_PARTNER => 'Партнер',
        ];
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_DELETED => 'Заблокирован',
            self::STATUS_ACTIVE => 'Активен',
        ];
    }

    public static function getUserName($id)
    {
        return static::findOne($id);
    }

    public static function getUserEmail($id)
    {
        return static::findOne($id)->email;
    }

    public static function getUserFullName($id)
    {
        $user = static::findOne($id);
        if (!$user) {
            return false;
        }
        return $user->firstname.' '.$user->lastname.' '.$user->phone;
    }

    public static function getManagers()
    {
        return ArrayHelper::map(static::find()->where(['role'=>self::ROLE_MANAGER])->all(),'id','username');
    }

    public static function getUserObject()
    {
        $users = static::find()->where(['role'=>self::ROLE_MANAGER])->orWhere(['role'=>self::ROLE_ADMIN])->orWhere(['role'=>self::ROLE_PARTNER])->all();
        $data = [];
        foreach ($users as $user) {
            $data[$user->id] = $user->firstname.' '.$user->lastname.' '.$user->phone;
        }
        return $data;
    }

    public static function getAgents()
    {
        return ArrayHelper::map(static::find()->where(['role'=>self::ROLE_AGENT])->all(),'id','username');
    }

    public static function getAgentsName()
    {
        $users = static::find()->where(['role'=>self::ROLE_AGENT])->all();
        $data =[];
        foreach ($users as $user) {
            $data[$user->id] = $user->firstname.' '.$user->lastname.' '.$user->phone;
        }
        return $data;
    }

    public static function getPartnerName()
    {
        $users = static::find()->where(['role'=>self::ROLE_PARTNER])->all();
        $data =[];
        foreach ($users as $user) {
            $data[$user->id] = $user->firstname.' '.$user->lastname.' '.$user->phone;
        }
        return $data;
    }

    public function isAdmin()
    {
        return $this->role === self::ROLE_ADMIN;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function createUser()
    {
        $this->setPassword($this->password);
        $this->generateAuthKey();
        return $this->save();
    }

    public function updateUser()
    {
        if ($this->newpassword != '') {
            $this->setPassword($this->newpassword);
        }
        return $this->save();
    }


}