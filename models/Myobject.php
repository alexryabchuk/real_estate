<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "myobject".
 *
 * @property int $id
 * @property string $image Фото
 * @property string $metro Метро
 * @property string $etazh Етаж
 * @property string $address Адрес
 * @property string $price Цена
 * @property string $short_description Описание 1
 * @property string $description1 Описание 1
 * @property string $description2 Описание 2
 * @property string $coord_x Долгота
 * @property string $coord_y Широта
 * @property string $SEO_TITLE SEO TITLE
 * @property string $SEO_KEY SEO KEY
 * @property string $SEO_DESCRIPTION SEO DESCRIPTION
 * @property string $SEO_TAGS Теги
 */
class Myobject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'myobject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'address', 'metro','etazh','user_id'],'required'],
            [['price','object_type','hot','show_in_site','deleted'], 'number'],
            [['short_description','description1', 'description2'], 'string'],
            [['image', 'address', 'SEO_TITLE'], 'string', 'max' => 255],
            [['metro'], 'string', 'max' => 80],
            [['etazh'], 'string', 'max' => 20],
            [['coord_x', 'coord_y'], 'string', 'max' => 30],
            [['SEO_KEY', 'SEO_DESCRIPTION', 'SEO_TAGS'], 'string', 'max' => 1024],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Фото',
            'metro' => 'Метро',
            'etazh' => 'Этаж',
            'address' => 'Адрес',
            'price' => 'Цена',
            'short_description' => 'Краткое описание',
            'description1' => 'Описание 1',
            'description2' => 'Описание 2',
            'coord_x' => 'Долгота',
            'coord_y' => 'Широта',
            'SEO_TITLE' => 'SEO TITLE',
            'SEO_KEY' => 'SEO KEY',
            'SEO_DESCRIPTION' => 'SEO DESCRIPTION',
            'SEO_TAGS' => 'Теги',
            'user_id' => 'Менеджер',
            'object_type' => 'Реализованый объект',
            'deleted' => 'В архиве',
            'hot' => 'Горящий',
            'show_in_site' => 'Показывать на сайте',
        ];
    }

    public static function getObjectsAddress()
    {
        $object = static::find()->all();
        $data = [];
        foreach ($object as $item) {
            $data[$item->id] = $item->metro.', '.$item->address;
        }
        return $data;
    }

    public static function getAllGeoObject(){
        $objects = static::find()->where(['object_type' => 0, 'show_in_site' => 1, 'deleted'=>0])->all();
        $arobj=array();
        $id=0;
        foreach ($objects as $object) {
            $studio = Myobject::getStudioCount($object->id);
            $arobj[$id]['studio']=$studio['payStudio'].'/'.$studio['allStudio'];
            $arobj[$id]["metro"]= $object->metro;
            $arobj[$id]["etazh"]= $object->etazh;
            $arobj[$id]["short_description"]= $object->short_description;
            $arobj[$id]["address"]= $object->address;
            $arobj[$id]["price"]= $object->price;
            $arobj[$id]["link"]= $object->id;
            $arobj[$id]["coord_x"]= $object->coord_x;
            $arobj[$id]["coord_y"]= $object->coord_y;
            $id++;
        }
        return $arobj;
    }

    public static function getStudioCount($id) {
        $allStudio = Studio::find()->where(['myobject_id'=>$id])->count();
        $payStudio = Studio::find()->where(['myobject_id'=>$id,'status'=>0])->count();
        return ['allStudio'=>$allStudio,'payStudio'=>$payStudio];

    }
    public function getStudio()
    {
        return $this->hasMany(Studio::className(), ['myobject_id' => 'id']);
    }
}
