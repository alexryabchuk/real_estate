<?php

namespace app\models;

use app\models\Conversation;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * ConversationSearch represents the model behind the search form about `app\models\Conversation`.
 */
class ConversationSearch extends Conversation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_one', 'user_two', 'created_at', 'status'], 'integer'],
            [['ip', 'message'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchIn($params)
    {
        $query = Conversation::find()->where(['user_two'=>\Yii::$app->user->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]]
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_one' => $this->user_one,
            'user_two' => $this->user_two,
            'created_at' => $this->created_at,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }

    public function searchOut($params)
    {
        $query = Conversation::find()->where(['user_one'=>\Yii::$app->user->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_one' => $this->user_one,
            'user_two' => $this->user_two,
            'created_at' => $this->created_at,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}
