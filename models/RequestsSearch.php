<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Requests;

/**
 * RequestsSearch represents the model behind the search form about `app\models\Requests`.
 */
class RequestsSearch extends Requests
{
    public $clientPhone;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'user_id', 'status', 'ad_source_id','deleted','myobject_id'], 'integer'],
            [['clientPhone', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Requests::find();
        if (Yii::$app->user->identity->getRole() == User::ROLE_PARTNER || Yii::$app->user->identity->getRole() == User::ROLE_AGENT) {
            $query->where(['requests.user_id'=>Yii::$app->user->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['deleted'=>SORT_ASC,'updated_at'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['client']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'requests.id' => $this->id,
            'requests.client_id' => $this->client_id,
            'requests.user_id' => $this->user_id,
            'requests.status' => $this->status,
            'requests.ad_source_id' => $this->ad_source_id,
            'requests.deleted' => $this->deleted,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);
        $query->joinWith(['client' => function ($q) {
            $q->where('client.phone LIKE "%' . $this->clientPhone . '%"');
        }]);

        return $dataProvider;
    }
}
