<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "appeal".
 *
 * @property int $id
 * @property string $phone
 * @property int $lead_id
 * @property string name
 * @property string email
 * @property string type_appeal
 * @property string text_appeal
 *
 * @property Client $lead
 */
class Appeal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'appeal';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lead_id','type_appeal'], 'integer'],
            [['phone'], 'string', 'max' => 20],
            [['text_appeal','name','email'],'string'],
            [['lead_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['lead_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Телефон',
            'status' => 'Статус',
            'lead_id' => 'Клиент',
            'name' => 'Имя',
            'email' => 'E-mail',
            'created_at' =>'Дата',
            'type_appeal' => 'Тип обращения',
            'text_appeal' => 'Текст обращения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLead()
    {
        return $this->hasOne(Client::className(), ['id' => 'lead_id']);
    }

    public static function getStatusName($status)
    {
        switch ($status) {
            case 0 :
                return 'Новый';
                break;
            case 1 :
                return 'Обработан';
                break;
        }
    }

    public static function getAllStatus()
    {
        return [0 => 'Новый', 1 => 'Обработан'];
    }

    public static function getTypes()
    {
        return [0 => 'Запрос каталога', 1 => 'Запись на просмотр',2=>'Обращение через контакт',3=>'Запрос от инвестора'];
    }
}
