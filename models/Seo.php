<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seo".
 *
 * @property int $id
 * @property string $name Страница
 * @property string $SEO_TITLE SEO TITLE
 * @property string $SEO_KEY SEO KEY
 * @property string $SEO_DESCRIPTION SEO DESCRIPTION
 */
class Seo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'seo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'SEO_TITLE'], 'string', 'max' => 255],
            [['SEO_KEY', 'SEO_DESCRIPTION'], 'string', 'max' => 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Страница',
            'SEO_TITLE' => 'SEO TITLE',
            'SEO_KEY' => 'SEO KEY',
            'SEO_DESCRIPTION' => 'SEO DESCRIPTION',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        $result = parent::afterSave($insert, $changedAttributes);
        $cacheSEO = Yii::$app->cache;
        $settingsSEO = Seo::find()->indexBy('id')->asArray()->all();
        $cacheSEO->set('settingsSEO', $settingsSEO, null);
    }
}
