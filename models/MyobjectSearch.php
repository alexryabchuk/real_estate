<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Myobject;

/**
 * MyobjectSearch represents the model behind the search form about `app\models\Myobject`.
 */
class MyobjectSearch extends Myobject
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['image', 'metro', 'etazh', 'address', 'description1', 'description2', 'coord_x', 'coord_y', 'SEO_TITLE', 'SEO_KEY', 'SEO_DESCRIPTION', 'SEO_TAGS'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Myobject::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['deleted'=>SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'metro', $this->metro])
            ->andFilterWhere(['like', 'etazh', $this->etazh])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'description1', $this->description1])
            ->andFilterWhere(['like', 'description2', $this->description2])
            ->andFilterWhere(['like', 'coord_x', $this->coord_x])
            ->andFilterWhere(['like', 'coord_y', $this->coord_y])
            ->andFilterWhere(['like', 'SEO_TITLE', $this->SEO_TITLE])
            ->andFilterWhere(['like', 'SEO_KEY', $this->SEO_KEY])
            ->andFilterWhere(['like', 'SEO_DESCRIPTION', $this->SEO_DESCRIPTION])
            ->andFilterWhere(['like', 'SEO_TAGS', $this->SEO_TAGS]);

        return $dataProvider;
    }
}
