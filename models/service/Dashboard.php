<?php

namespace app\models\service;

use app\models\AdSource;
use app\models\Myobject;
use app\models\Requests;
use app\models\RequestsComment;
use app\models\User;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class Dashboard extends Model
{
    public $date1;
    public $date2;
    public $date3;

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->date1 = strtotime(date('01.m.Y'));
        $this->date2 = strtotime(date('d.m.Y'). ' + 1 days');
        $this->date3 = strtotime(date('01.m.Y'). ' - 2 month');
    }

    public static function factory()
    {
        return new Dashboard();
    }

    public function RequestsCount($user_id = null)
    {
        $allRequest = Requests::find()->where(['>','created_at',$this->date1])->andWhere(['<','created_at',$this->date2])->andWhere(['deleted'=>0]);
        if ($user_id) {
            $allRequest->andWhere(['user_id'=>$user_id]);
        }
        return $allRequest->count();
    }


    public function EmptyRequestsCount($user_id = null)
    {
        $allRequest = Requests::find()->where(['>','created_at',$this->date1])->andWhere(['<','created_at',$this->date2])
            ->andWhere(['count_active'=>0])->andWhere(['deleted'=>0]);
        if ($user_id) {
            $allRequest->andWhere(['user_id'=>$user_id]);
        }
        return $allRequest->count();
    }

    public function notActiveRequestsCount($user_id = null)
    {
        $date = strtotime(date('d.m.Y'). ' - 7 days');
        $allRequest = Requests::find()->where(['<','last_active',$date])
            ->andWhere(['deleted'=>0]);
        if ($user_id) {
            $allRequest->andWhere(['user_id'=>$user_id]);
        }
        return $allRequest->count();
    }

    public function AdSourseData($user_id = null)
    {
        $adSourses = Requests::find()->where(['>','created_at',$this->date1])->andWhere(['<','created_at',$this->date2])->andWhere(['deleted'=>0])
            ->select (['count(id) as countId','ad_source_id'])->groupBy(['ad_source_id'])->asArray()->all();
        if ($user_id) {
            $adSourses->andWhere(['user_id'=>$user_id]);
        }
        $adSourceStat = [];
        $adSoursesList = ArrayHelper::map(AdSource::find()->all(),'id','name');
        foreach ($adSourses as $adSours) {
            $adSourceStat[] = ['name'=>$adSoursesList[$adSours['ad_source_id']],'y'=>(int)$adSours['countId']];
        }
        return $adSourceStat;
    }
    public function freeStudioData()
    {
        $allStudios = Myobject::find()->joinWith('studio')->select(['count(studio.status) as countStudio','myobject.id as id'])
            ->andWhere(['myobject.deleted'=>0])->groupBy('myobject.id')->asArray()->all();
        $rezervStudios = Myobject::find()->joinWith('studio')->select(['count(studio.status) as countStudio','myobject.id'])
            ->andWhere(['myobject.deleted'=>0])->where(['studio.status'=>0])->groupBy('myobject.id')->asArray()->all();
        $rezArr = [];
        foreach ($rezervStudios as $rezervStudio) {
            $rezArr[$rezervStudio['id']] = $rezervStudio['countStudio'];
        }
        $data1 =[];
        $data2 = [];
        $data3 =[];
        foreach ($allStudios as $studio) {
            $data1[] = (int)$studio['countStudio'];
            if (isset($rezArr[$studio['id']])) {
                $data2[] = (int)$rezArr[$studio['id']];
            } else {
                $data2[] = 0;
            }
            $data3[] = Myobject::getObjectsAddress()[$studio['id']];
        }
        $rezervPercent =[$data1,$data2,$data3];
        return $rezervPercent;
    }
    public function requestsStatusData($user_id = null)
    {
        $allRequest = Requests::find()->where(['>','created_at',$this->date1])->andWhere(['<','created_at',$this->date2])
            ->andWhere(['deleted'=>0])->select(['count(id) as countStatus','status'])->groupBy('status');
        if ($user_id) {
            $allRequest->andWhere(['user_id'=>$user_id]);
        }
        $allRequest = $allRequest->orderBy('status')->indexBy('status')->asArray()->all();
        $data = [];
        foreach (Requests::getStatuses() as $key=>$item) {
            if (isset($allRequest[$key])) {
                $data[] = (int)$allRequest[$key]['countStatus'];
            } else {
                $data[] = 0;
            }
        }
        return ['data'=>$data,'label'=>array_values(Requests::getStatuses())];
    }
    public function activeAgents($agents = null)
    {
        if (!$agents) {
            $agents = array_keys(User::find()->where(['role'=>User::ROLE_AGENT])->select('id')->indexBy('id')->asArray()->all());
        }
        $allRequest = Requests::find()->where(['>','created_at',$this->date1])->andWhere(['<','created_at',$this->date2])->andWhere(['in','user_id',$agents])
            ->andWhere(['deleted'=>0])->select(['count(id) as countStatus','user_id'])->groupBy('user_id')
            ->orderBy('user_id')->indexBy('user_id')->asArray()->all();
        $data = [];
        foreach (User::getAgentsName() as $key=>$item) {
            if (isset($allRequest[$key])) {
                $data[] = ['name' => $item,'y' => (int)$allRequest[$key]['countStatus']] ;
            } else {
                $data[] = ['name' => $item,'y' => 0] ;
            }
        }
        return $data;
    }

    public function activePartner($partner = null)
    {
        if (!$partner) {
            $partner = array_keys(User::find()->where(['role'=>User::ROLE_PARTNER])->select('id')->indexBy('id')->asArray()->all());
        }
        $allRequest = Requests::find()->where(['>','created_at',$this->date1])->andWhere(['<','created_at',$this->date2])->andWhere(['in','user_id',$partner])
            ->andWhere(['deleted'=>0])->select(['count(id) as countStatus','user_id'])->groupBy('user_id')
            ->orderBy('user_id')->indexBy('user_id')->asArray()->all();
        $data = [];
        foreach (User::getPartnerName() as $key=>$item) {
            if (isset($allRequest[$key])) {
                $data[] = ['name' => $item,'y' => (int)$allRequest[$key]['countStatus']] ;
            } else {
                $data[] = ['name' => $item,'y' => 0] ;
            }
        }
        return $data;
    }

    public function activeAgentByDay($agent)
    {
        $datediff = round(($this->date2 - $this->date3)/(60 * 60 * 24));
        $allRequest = Requests::find()->where(['>','created_at',$this->date1])->andWhere(['<','created_at',$this->date2])->andWhere(['user_id'=>$agent])
            ->andWhere(['deleted'=>0])->all();
        $data = [];
        $date = $this->date3;
        for ($i=1;$i <= $datediff; $i++) {

            $index = date('d.m',$date);
            $data[$index] = 0;
            $date +=(60 * 60 * 24);
        }
        foreach ($allRequest as $request) {
            $day = date('d.m',$request['created_at']);
            $data[$day]++;
        }
        return $data;
    }

    public function activePartnerByDay($partner = null)
    {
        if (!$partner) {
            $partner = array_keys(User::find()->where(['role'=>User::ROLE_PARTNER])->select('id')->indexBy('id')->asArray()->all());
        }
        $allRequest = Requests::find()->where(['>','created_at',$this->date1])->andWhere(['<','created_at',$this->date2])->andWhere(['in','user_id',$partner])->andWhere(['deleted'=>0])
            ->select(['count(id) as countId','created_at'])->groupBy(['created_at'])->asArray()->all();
        $allCommentRequest = RequestsComment::find()->where(['>','created_at',$this->date1])->andWhere(['<','created_at',$this->date2])->andWhere(['in','user_id',$partner])
            ->select(['count(id) as countId','created_at'])->groupBy(['created_at'])->asArray()->all();
        $data=[];
        for ($i=1;$i <= (int)date('d');$i++) {
            $data[$i] = 0;
        }
        foreach ($allRequest as $request) {
            $day = (int)date('d',$request['created_at']);
            $data[$day] = (int)$request['countId'];
        }
        foreach ($allCommentRequest as $request) {
            $day = (int)date('d',$request['created_at']);
            $data[$day] += (int)$request['countId'];
        }
        return $data;
    }

}
