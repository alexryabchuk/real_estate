<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Instagram extends Model
{
    public function getPost($offset = 0)
    {

        $token = Settings::find()->where(['key'=>'instagram_api'])->one();
        $token = $token->value;

        $user_id = 'self';
        $instagram_cnct = curl_init(); // инициализация cURL подключения
        curl_setopt( $instagram_cnct, CURLOPT_URL, "https://api.instagram.com/v1/users/" . $user_id . "/media/recent?access_token=" . $token ); // подключаемся
        curl_setopt( $instagram_cnct, CURLOPT_RETURNTRANSFER, 1 ); // просим вернуть результат
        curl_setopt( $instagram_cnct, CURLOPT_TIMEOUT, 15 );
        $media = json_decode( curl_exec( $instagram_cnct ) ); // получаем и декодируем данные из JSON
        curl_close( $instagram_cnct ); // закрываем соединение


        $limit = 8;
        $output = "";
        if (isset($media->data)) {
            return array_slice($media->data, $offset, $limit);
        }
        return [];


    }
}
