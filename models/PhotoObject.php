<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "photo_object".
 *
 * @property int $id
 * @property int $myobject_id Объект
 * @property string $filename Файл
 * @property int $photo_type Тип фото
 *
 * @property Myobject $myobject
 */
class PhotoObject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'photo_object';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['myobject_id', 'photo_type'], 'integer'],
            [['filename'], 'string', 'max' => 255],
            [['myobject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Myobject::className(), 'targetAttribute' => ['myobject_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'myobject_id' => 'Объект',
            'filename' => 'Файл',
            'photo_type' => 'Тип фото',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyobject()
    {
        return $this->hasOne(Myobject::className(), ['id' => 'myobject_id']);
    }
}
