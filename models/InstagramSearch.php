<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Instagram;

/**
 * NewsSearch represents the model behind the search form about `app\models\News`.
 */
class InstagramSearch extends Instagram
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'category'], 'integer'],
            [['name', 'description', 'SEO_TITLE', 'SEO_KEY', 'SEO_DESCRIPTION', 'SEO_TAGS'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Instagram::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'category' => $this->category,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'SEO_TITLE', $this->SEO_TITLE])
            ->andFilterWhere(['like', 'SEO_KEY', $this->SEO_KEY])
            ->andFilterWhere(['like', 'SEO_DESCRIPTION', $this->SEO_DESCRIPTION])
            ->andFilterWhere(['like', 'SEO_TAGS', $this->SEO_TAGS]);

        return $dataProvider;
    }
}
