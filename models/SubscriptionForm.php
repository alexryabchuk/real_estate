<?php

namespace app\models;

use Yii;
use yii\base\Model;


class SubscriptionForm extends Model
{
    public $email;

    public function rules()
    {
        return [
            [[ 'email'], 'required'],
            ['email', 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'email',
        ];
    }


}
