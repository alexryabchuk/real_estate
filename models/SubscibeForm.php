<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class SubscibeForm extends Model
{
    public $email;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [[ 'email'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'email' => 'email',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        $appeal = new Appeal();
        $appeal->name = $this->name;
        $appeal->email = $this->email;
        $appeal->text_appeal = $this->body;
        $appeal->type_appeal = 2;
        $appeal->save();

        if (!$appeal->errors)    return true;

        return false;
    }
}
