<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requests_log".
 *
 * @property int $id
 * @property int $requests_id Заявка
 * @property int $created_at Дата
 * @property int $user_id Користувач
 * @property string $data Измененние значения
 *
 * @property Requests $requests
 * @property User $user
 */
class RequestsLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requests_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['requests_id', 'created_at', 'user_id'], 'integer'],
            [['data'], 'string', 'max' => 512],
            [['requests_id'], 'exist', 'skipOnError' => true, 'targetClass' => Requests::className(), 'targetAttribute' => ['requests_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'requests_id' => 'Заявка',
            'created_at' => 'Дата',
            'user_id' => 'Користувач',
            'data' => 'Измененние значения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasOne(Requests::className(), ['id' => 'requests_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
