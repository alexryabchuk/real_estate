<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "conversation".
 *
 * @property int $id
 * @property int $user_one  От кого
 * @property int $user_two Кому
 * @property string $ip IP
 * @property int $created_at Дата
 * @property int $status Статус
 * @property string $message Текст сообщения
 *
 * @property User $userOne
 * @property User $userTwo
 */
class Conversation extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at' ],
                    ActiveRecord::EVENT_BEFORE_UPDATE => [],
                ],
            ],
        ];
    }

    public static function tableName()
    {
        return 'conversation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_one', 'user_two', 'created_at', 'status'], 'integer'],
            [['message'], 'string'],
            [['ip'], 'string', 'max' => 30],
            [['user_one'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_one' => 'id']],
            [['user_two'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_two' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_one' => ' От кого',
            'user_two' => 'Кому',
            'ip' => 'IP',
            'created_at' => 'Дата',
            'status' => 'Статус',
            'message' => 'Текст сообщения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserOne()
    {
        return $this->hasOne(User::className(), ['id' => 'user_one']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTwo()
    {
        return $this->hasOne(User::className(), ['id' => 'user_two']);
    }

    public static function getNewConversation() {
        return static::find()->where(['user_two'=>Yii::$app->user->id])->andWhere(['status'=>0])->count();
    }
}
