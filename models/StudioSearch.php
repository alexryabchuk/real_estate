<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Studio;

/**
 * StudioSearch represents the model behind the search form about `app\models\Studio`.
 */
class StudioSearch extends Studio
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'balcon', 'status', 'myobject_id'], 'integer'],
            [['name'], 'safe'],
            [['square', 'price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Studio::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'square' => $this->square,
            'balcon' => $this->balcon,
            'price' => $this->price,
            'status' => $this->status,
            'myobject_id' => $this->myobject_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
