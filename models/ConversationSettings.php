<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conversation_settings".
 *
 * @property string $key
 * @property string $name
 * @property int $conversation_inner Внутреннее
 * @property int $conversation_email E-mail
 * @property int $conversation_telegram Telegram
 */
class ConversationSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conversation_settings';
    }

    public static function primaryKey()
    {
        return ['key'];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['conversation_inner', 'conversation_email', 'conversation_telegram'], 'integer'],
            [['key', 'name'], 'string', 'max' => 255],
            [['key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'key' => 'Key',
            'name' => 'Название',
            'conversation_inner' => 'Внутреннее',
            'conversation_email' => 'E-mail',
            'conversation_telegram' => 'Telegram',
        ];
    }
}
