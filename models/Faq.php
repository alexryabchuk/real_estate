<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property int $id
 * @property string $question Вопрос
 * @property string $answer Ответ
 * @property string $category Категория
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question', 'answer'], 'string', 'max' => 1024],
            ['category', 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'category' => 'Категория',
        ];
    }
    public static function getCategory(){
        return [
            0 => 'Основные вопросы',
            1 => 'Другие вопросы'
        ];
    }
}
