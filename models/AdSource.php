<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ad_source".
 *
 * @property int $id
 * @property string $name Название
 * @property int $deleted Удален
 * @property integer $created_at
 * @property integer $updated_at
 */
class AdSource extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_source';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [
            [['deleted'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'deleted' => 'Удален',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
        ];
    }

    public static function getAdSource()
    {
        return ArrayHelper::map(static::find()->all(),'id','name');
    }
}
