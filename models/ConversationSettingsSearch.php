<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ConversationSettings;

/**
 * ConversationSettingsSearch represents the model behind the search form about `app\models\ConversationSettings`.
 */
class ConversationSettingsSearch extends ConversationSettings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'name'], 'safe'],
            [['conversation_inner', 'conversation_email', 'conversation_telegram'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ConversationSettings::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'conversation_inner' => $this->conversation_inner,
            'conversation_email' => $this->conversation_email,
            'conversation_telegram' => $this->conversation_telegram,
        ]);

        $query->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
