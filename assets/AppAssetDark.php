<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetDark extends AssetBundle
{

    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/theme-default.css',
        'css/fontawesome/font-awesome.min.css',
        'css/site-dark.css',
        'css/style.css',
        'css/screen.css',
        'css/introjs.min.css',

    ];
    public $js = [
        'js/intro.min.js',
        'js/bootstrap.min.js',
        'js/plugins/jquery/jquery-ui.min.js',
        'js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js',
        'js/plugins.js',
        'js/actions.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
