<li>
    <div class="comment">
        <div class="author">
            <?php echo $comment['author'];?>
            <span class="date"><?php echo date('d.m.Y H:i',$comment['created_at']);?></span>
        </div>

        <div class="comment_text"><?php echo $comment['comment'];?></div>
    </div>
    
    <?php if(!empty($comment['childs'])):?>
    <ul>
        <?php echo getCommentsTemplate($comment['childs']);?>
    </ul>   
    <?php endif;?>
</li>