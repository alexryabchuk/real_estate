<?php


namespace app\helpers;


use yii\helpers\Html;

class OkNo
{
    public static function text($value, $inverse = null)
    {
        if ($inverse) {
            return  $value == 1 ? 'Нет' : 'Да';
        }
        return  $value == 0 ? 'Нет' : 'Да';
    }

    public static function icon($value, $inverse = null)
    {
        if ($inverse) {
            return $value == 1 ? '<i style="color: #ff0155" class="glyphicon glyphicon-remove"></i>' : '<i style="color: #159c04" class="glyphicon glyphicon-ok"></i>';
        }
        return $value == 0 ? '<i style="color: #ff0155" class="glyphicon glyphicon-remove"></i>' : '<i style="color: #159c04" class="glyphicon glyphicon-ok"></i>';
    }

}

