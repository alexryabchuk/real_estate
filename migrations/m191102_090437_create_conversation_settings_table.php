<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%conversation_settings}}`.
 */
class m191102_090437_create_conversation_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%conversation_settings}}', [
            'key' => $this->string()->unique(),
            'name'=> $this->string()->comment('Название')->notNull(),
            'conversation_inner'=>$this->smallInteger()->comment('Внутреннее')->defaultValue(1),
            'conversation_email'=>$this->smallInteger()->comment('E-mail')->defaultValue(0),
            'conversation_telegram'=>$this->smallInteger()->comment('Telegram')->defaultValue(0),
        ]);
        $this->insert('{{%conversation_settings}}',['key'=>'add_request','name'=>'Создана заявка']);
        $this->insert('{{%conversation_settings}}',['key'=>'add_request_comment','name'=>'Создана запись в истории общения по заявке']);
        $this->insert('{{%conversation_settings}}',['key'=>'change_request_status','name'=>'Изменен статус заявки']);
        $this->insert('{{%conversation_settings}}',['key'=>'change_client','name'=>'Изменены данные клиента']);
        $this->insert('{{%conversation_settings}}',['key'=>'request_catalog','name'=>'Запрос каталога']);
        $this->insert('{{%conversation_settings}}',['key'=>'request_contacts','name'=>'Контакти']);
        $this->insert('{{%conversation_settings}}',['key'=>'add_comment','name'=>'Коментарии']);
        $this->insert('{{%conversation_settings}}',['key'=>'view_object','name'=>'Запись на обьект']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%conversation_settings}}');
    }
}
