<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%requests_objects}}`.
 */
class m190906_105603_create_requests_objects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%requests_objects}}', [
            'id' => $this->primaryKey(),
            'requests_id' => $this->integer()->comment(''),
            'myobject_id' => $this->integer()->comment(''),

        ]);
        $this->createIndex('idx-requests_objects-requests_id','requests_objects','requests_id');
        $this->addForeignKey('fk-requests_objects-requests_id','requests_objects','requests_id','requests','id');
        $this->createIndex('idx-requests_objects-myobject_id','requests_objects','myobject_id');
        $this->addForeignKey('fk-requests_objects-myobject_id','requests_objects','myobject_id','myobject','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-requests_objects-requests_id','requests_objects');
        $this->dropForeignKey('fk-requests_objects-myobject_id','requests_objects');
        $this->dropTable('{{%requests_objects}}');
    }
}
