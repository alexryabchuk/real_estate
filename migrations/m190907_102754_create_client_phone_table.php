<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%client_phone_requests_objects}}`.
 */
class m190907_102754_create_client_phone_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%client_phones}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(),
            'phone' => $this->string(16)->comment('Телефон'),
            'comment' => $this->string(60)->comment('Примечание'),
        ]);
        $this->createIndex('idx-client_phones-client_id','client_phones','client_id');
        $this->addForeignKey('fk-client_phones-client_id','client_phones','client_id','client','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-client_phones-client_id','client_phones');
        $this->dropTable('{{%client_phone}}');
    }
}
