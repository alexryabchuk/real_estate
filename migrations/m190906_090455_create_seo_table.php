<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%seo}}`.
 */
class m190906_090455_create_seo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%seo}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Страница'),
            'SEO_TITLE' => $this->string(255)->comment('SEO TITLE'),
            'SEO_KEY' => $this->string(1024)->comment('SEO KEY'),
            'SEO_DESCRIPTION' => $this->string(1024)->comment('SEO DESCRIPTION'),
        ]);
        $this->insert('seo',['id'=>1, 'name'=>'Главная']);
        $this->insert('seo',['id'=>2, 'name'=>'Наши объекты']);
        $this->insert('seo',['id'=>3, 'name'=>'Реализованные проекты']);
        $this->insert('seo',['id'=>4, 'name'=>'Статьи']);
        $this->insert('seo',['id'=>5, 'name'=>'О компании']);
        $this->insert('seo',['id'=>6, 'name'=>'Контакты']);
        $this->insert('seo',['id'=>7, 'name'=>'FAQ']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%seo}}');
    }
}
