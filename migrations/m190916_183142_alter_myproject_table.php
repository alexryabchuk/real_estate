<?php

use yii\db\Migration;

/**
 * Class m190916_183142_alter_myproject_table
 */
class m190916_183142_alter_myproject_table extends Migration
{

    public function safeUp()
    {
        $this->addColumn('myobject','hot',$this->smallInteger()->comment('Горящий')->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('myobject','hot');
    }

}
