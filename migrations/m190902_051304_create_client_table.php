<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%client}}`.
 */
class m190902_051304_create_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%client}}', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string()->comment(''),
            'lastname' => $this->string()->comment(''),
            'phone' => $this->string(20)->comment(''),
            'lead' => $this->smallInteger()->comment('')->defaultValue(0),
            'user_id' => $this->integer()->comment('')
        ]);
        $this->createIndex('idx-client-user_id','client','user_id');
        $this->addForeignKey('fk-client-user_id','client','user_id','user','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%client}}');
    }
}
