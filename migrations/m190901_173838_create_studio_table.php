<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%studio}}`.
 */
class m190901_173838_create_studio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%studio}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'square' => $this->decimal(10,2)->comment('Площадь'),
            'balcon' =>$this->smallInteger(1)->comment('Балкон'),
            'price' => $this->decimal(18,2)->comment('Цена'),
            'status' => $this->smallInteger(1)->comment('Статус'),
            'myobject_id' => $this->integer()->comment('Объект'),
        ]);
        $this->createIndex('idx-studio-myobject_id','studio','myobject_id');
        $this->addForeignKey('fk-studio-myobject_id','studio','myobject_id','myobject','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-studio-myobject_id','studio');
        $this->dropTable('{{%studio}}');
    }
}
