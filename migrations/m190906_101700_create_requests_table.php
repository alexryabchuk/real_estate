<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%requests}}`.
 */
class m190906_101700_create_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%requests}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->comment('Клиент'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'status' => $this->integer()->comment('Статус'),
            'ad_source_id' => $this->integer()->comment('Источник'),
            'comment' => $this->string(512)->comment('Коментарий'),
        ]);
        $this->createIndex('idx-requests-user_id','requests','user_id');
        $this->addForeignKey('fk-requests-user_id','requests','user_id','user','id');
        $this->createIndex('idx-requests-client_id','requests','client_id');
        $this->addForeignKey('fk-requests-client_id','requests','client_id','client','id');
        $this->createIndex('idx-requests-ad_source_id','requests','ad_source_id');
        $this->addForeignKey('fk-requests-ad_source_id','requests','ad_source_id','ad_source','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-requests-user_id','requests');
        $this->dropForeignKey('fk-requests-client_id','requests');
        $this->dropForeignKey('fk-requests-ad_source_id','requests');
        $this->dropTable('{{%requests}}');
    }
}
