<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%conversation}}`.
 */
class m191011_141413_create_conversation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings',['name'=>'Контакты (Координаты)','key'=>'contact_coord','value'=>'55.80225852091544,37.58215623313522']);
        $this->createTable('{{%conversation}}', [
            'id' => $this->primaryKey(),
            'user_one' => $this->integer()->comment(' От кого'),
            'user_two' => $this->integer()->comment('Кому'),
            'ip' => $this->string(30)->comment('IP'),
            'created_at' => $this->integer()->comment('Дата'),
            'status' => $this->integer()->comment('Статус'),
            'message' => $this->text()->comment('Текст сообщения')
        ]);
        $this->createIndex('idx-conversation-user_one','conversation','user_one');
        $this->addForeignKey('fk-conversation-user_one','conversation','user_one','user','id');
        $this->createIndex('idx-conversation-user_two','conversation','user_two');
        $this->addForeignKey('fk-conversation-user_two','conversation','user_two','user','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('settings',['key'=>'contact_coord']);
        $this->dropForeignKey('fk-conversation-user_one','conversation');
        $this->dropForeignKey('fk-conversation-user_two','conversation');
        $this->dropTable('{{%conversation}}');
    }
}
