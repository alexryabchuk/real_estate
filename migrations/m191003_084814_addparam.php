<?php

use yii\db\Migration;

/**
 * Class m191003_084814_addparam
 */
class m191003_084814_addparam extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191003_084814_addparam cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191003_084814_addparam cannot be reverted.\n";

        return false;
    }
    */
}
