<?php

use yii\db\Migration;

/**
 * Class m190909_081409_alter_appeal_table
 */
class m190909_081409_alter_appeal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('appeal','name',$this->string()->comment('Имя'));
        $this->addColumn('appeal','email',$this->string()->comment('Имя'));
        $this->addColumn('appeal','type_appeal',$this->smallInteger()->comment('Тип обращения'));
        $this->addColumn('appeal','text_appeal',$this->string(512)->comment('Текст обращения'));
        $this->addColumn('appeal','object_id',$this->integer()->comment('Объект обращения'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('appeal','name');
        $this->dropColumn('appeal','email');
        $this->dropColumn('appeal','type_appeal');
        $this->dropColumn('appeal','text_appeal');
        $this->dropColumn('appeal','object_id');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190909_081409_alter_appeal_table cannot be reverted.\n";

        return false;
    }
    */
}
