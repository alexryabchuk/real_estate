<?php

use yii\db\Migration;

/**
 * Class m190906_060424_alter_user_table
 */
class m190906_060424_alter_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user','firstname',$this->string()->comment('Имя'));
        $this->addColumn('user','lastname',$this->string()->comment('Фамилия'));
        $this->addColumn('user','phone',$this->string()->comment('Телефон'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user','firstname');
        $this->dropColumn('user','lastname');
        $this->dropColumn('user','phone');

    }
}
