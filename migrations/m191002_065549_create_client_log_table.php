<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%client_log}}`.
 */
class m191002_065549_create_client_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%client_log}}', [
            'id' => $this->primaryKey(),
            'client_id'=>$this->integer()->comment('Клиент'),
            'created_at'=>$this->integer()->comment('Дата'),
            'user_id'=>$this->integer()->comment('Користувач'),
            'data'=>$this->string(512)->comment('Измененние значения'),
        ]);
        $this->createIndex('idx-client_log-client_id','client_log','client_id');
        $this->addForeignKey('fk-client_log-client_id','client_log','client_id','client','id');
        $this->createIndex('idx-client_log-user_id','client_log','user_id');
        $this->addForeignKey('fk-client_log-user_id','client_log','user_id','user','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-client_log-client_id','client_log');
        $this->dropForeignKey('fk-client_log-user_id','client_log');
        $this->dropTable('{{%client_log}}');
    }
}
