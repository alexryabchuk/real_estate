<?php

use yii\db\Migration;

/**
 * Class m190909_093847_add_deleted_fields
 */
class m190909_093847_add_deleted_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('requests','deleted',$this->smallInteger()->comment('В архиве')->defaultValue(0));
        $this->addColumn('article','deleted',$this->smallInteger()->comment('В архиве')->defaultValue(0));
        $this->addColumn('myobject','deleted',$this->smallInteger()->comment('В архиве')->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('requests','deleted');
        $this->dropColumn('article','deleted');
        $this->dropColumn('myobject','deleted');


    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190909_093847_add_deleted_fields cannot be reverted.\n";

        return false;
    }
    */
}
