<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%appeal}}`.
 */
class m190902_054621_create_appeal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%appeal}}', [
            'id' => $this->primaryKey(),
            'phone' => $this->string(20)->comment('Телефон'),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'lead_id' => $this->integer()->comment(''),
            'status' => $this->smallInteger()->comment('Статус'),
            'user_id' => $this->integer()->comment('Пользователь')
        ]);
        $this->createIndex('idx-appeal-lead_id','appeal','lead_id');
        $this->addForeignKey('fk-appeal-lead_id','appeal','lead_id','client','id');
        $this->createIndex('idx-appeal-user_id','appeal','user_id');
        $this->addForeignKey('fk-appeal-user_id','appeal','user_id','user','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      //  $this->dropForeignKey('fk-appeal-user_id','appeal',);
        $this->dropForeignKey('fk-appeal-lead_id','appeal');
        $this->dropTable('{{%appeal}}');
    }
}
