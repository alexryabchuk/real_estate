<?php

use app\models\User;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m190808_092627_create_user_table extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique()->comment('Логин пользователя'),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique()->comment('E-mail пользователя'),
            'status' => $this->smallInteger()->notNull()->defaultValue(10)->comment('Статус пользователя'),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'role' => $this->smallInteger()->notNull()->defaultValue(1)->comment('Тип пользователя'),
        ], $tableOptions);

        $model = User::find()->where(['username' => 'admin'])->one();
        if (empty($model)) {
            $user = new User();
            $user->username = 'admin';
            $user->email = 'admin@gmail.com';
            $user->role = 0;
            $user->setPassword('admin');
            $user->generateAuthKey();
            if ($user->save()) {
                echo 'good';
            }
        }

        $model = User::find()->where(['username' => 'user'])->one();
        if (empty($model)) {
            $user = new User();
            $user->username = 'user';
            $user->email = 'user@gmail.com';
            $user->setPassword('user');
            $user->generateAuthKey();
            if ($user->save()) {
                echo 'good';
            }
        }
    }

    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
