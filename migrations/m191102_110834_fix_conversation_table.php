<?php

use yii\db\Migration;

/**
 * Class m191102_110834_fix_conversation_table
 */
class m191102_110834_fix_conversation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{conversation}}','status',$this->integer()->comment('Статус')->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

}
