<?php

use yii\db\Migration;

/**
 * Class m190909_061142_create_fix1
 */
class m190909_061142_create_fix1 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('client','created_at',$this->integer()->notNull());
        $this->addColumn('client','updated_at',$this->integer()->notNull());
        $this->addColumn('requests','created_at',$this->integer()->notNull());
        $this->addColumn('requests','updated_at',$this->integer()->notNull());
        $this->addColumn('ad_source','created_at',$this->integer()->notNull());
        $this->addColumn('ad_source','updated_at',$this->integer()->notNull());
        $this->insert('settings',['name'=>'Telegram BOT ID','key'=>'telegram_bot_id']);
        $this->insert('settings',['name'=>'Код подключения счетчика Яндекс.Метрика','key'=>'code_yandex']);
        $this->insert('settings',['name'=>'Код подключения счетчика Google.Метрика','key'=>'code_google']);
        $this->insert('settings',['name'=>'Свой код','key'=>'code_my']);
        $this->insert('settings',['name'=>'Viber/telegram','key'=>'contact_phone_viber','value'=>'+7 909 906 00 77']);
        $this->insert('settings',['name'=>'Телефон','key'=>'contact_phone_main','value'=>'+7 923 912 02 41']);
        $this->insert('settings',['name'=>'E-mail','key'=>'contact_phone_main','value'=>'name@apartamio.com']);
        $this->insert('settings',['name'=>'Адрес','key'=>'contact_address','value'=>'г.Москва, ул.Вятская, д.70']);

    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190909_061142_create_fix1 cannot be reverted.\n";

        return false;
    }
    */
}
