<?php

use yii\db\Migration;

/**
 * Class m190930_194053_alter_fields_table
 */
class m190930_194053_alter_fields_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user','nik_telegram',$this->string(64)->comment('Ник в телеграм'));
        $this->addColumn('myobject','show_in_site',$this->smallInteger()->comment('Показывать на сайте')->defaultValue(1));
        $this->alterColumn('myobject','short_description',$this->text());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190930_194053_alter_fields_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190930_194053_alter_fields_table cannot be reverted.\n";

        return false;
    }
    */
}
