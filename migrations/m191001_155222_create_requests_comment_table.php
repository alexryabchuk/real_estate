<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%requests_comment}}`.
 */
class m191001_155222_create_requests_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%requests_comment}}', [
            'id' => $this->primaryKey(),
            'requests_id' => $this->integer()->comment('Заявка'),
            'created_at' => $this->integer()->comment('Дата заметки'),
            'comment' => $this->string()->comment('Заметка'),
            'user_id' => $this->integer()->comment('Пользователь')
        ]);
        $this->createIndex('idx-requests_comment-requests_id','requests_comment','requests_id');
        $this->addForeignKey('fk-requests_comment-requests_id','requests_comment','requests_id','requests','id');
        $this->createIndex('idx-requests_comment-user_id','requests_comment','user_id');
        $this->addForeignKey('fk-requests_comment-user_id','requests_comment','user_id','user','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-requests_comment-requests_id','requests_comment');
        $this->dropForeignKey('fk-requests_comment-user_id','requests_comment');
        $this->dropTable('{{%requests_comment}}');
    }
}
