<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ad_source}}`.
 */
class m190904_065926_create_ad_source_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ad_source}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'deleted' => $this->smallInteger()->defaultValue(0)->comment('Удален')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ad_source}}');
    }
}
