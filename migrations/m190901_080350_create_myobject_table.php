<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%myobject}}`.
 */
class m190901_080350_create_myobject_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%myobject}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string()->comment('Фото'),
            'metro' => $this->string(80)->comment('Метро'),
            'etazh' => $this->string(20)->comment('Этаж'),
            'address' => $this->string()->comment('Адрес'),
            'price' => $this->decimal(18, 2)->comment('Цена'),
            'short_description' => $this->string(512)->comment('Краткое описание'),
            'description1' => $this->text()->comment('Описание 1'),
            'description2' => $this->text()->comment('Описание 2'),
            'coord_x' => $this->string(30)->comment('Долгота'),
            'coord_y' => $this->string(30)->comment('Широта'),
            'SEO_TITLE' => $this->string(255)->comment('SEO TITLE'),
            'SEO_KEY' => $this->string(1024)->comment('SEO KEY'),
            'SEO_DESCRIPTION' => $this->string(1024)->comment('SEO DESCRIPTION'),
            'SEO_TAGS' => $this->string(1024)->comment('Теги'),
            'user_id' => $this->integer()->comment('Менеджер'),
            'object_type' => $this->integer()->comment('Тип объекта')->defaultValue(0),
        ]);
        $this->createIndex('idx-myobject-user_id','myobject','user_id');
        $this->addForeignKey('fk-myobject-user_id','myobject','user_id','user','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-myobject-user_id','myobject');
        $this->dropTable('{{%myobject}}');
    }
}
