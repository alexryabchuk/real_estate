<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%category_article}}`.
 */
class m190901_184921_create_category_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%category_article}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название категории'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%category_article}}');
    }
}
