<?php

use app\models\User;
use yii\db\Migration;

/**
 * Class m191102_104851_add_user
 */
class m191102_104851_add_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $model = User::find()->where(['username' => 'system'])->one();
        if (empty($model)) {
            $user = new User();
            $user->id = 999999;
            $user->username = 'system';
            $user->firstname = 'Система';
            $user->lastname = ':';
            $user->phone = '0';
            $user->role = User::ROLE_ADMIN;
            $user->email = 'system@gmail.com';
            $user->setPassword('user');
            $user->generateAuthKey();
            if ($user->save()) {
                echo 'good';
            } else (var_dump($user->errors));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
