<?php

use yii\db\Migration;


class m191005_092713_addcolumn_lastactive_requests extends Migration
{

    public function safeUp()
    {
        $this->addColumn('requests','last_active',$this->integer());
        $this->addColumn('requests','count_active',$this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('requests','last_active');
        $this->dropColumn('requests','count_active');
    }

}
