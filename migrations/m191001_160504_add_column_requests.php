<?php

use yii\db\Migration;

/**
 * Class m191001_160504_add_column_requests
 */
class m191001_160504_add_column_requests extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('requests','myobject_id',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('requests','myobject_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191001_160504_add_column_requests cannot be reverted.\n";

        return false;
    }
    */
}
