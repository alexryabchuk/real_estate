<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%article}}`.
 */
class m190902_051248_create_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%article}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Заголовок'),
            'photo' => $this->string()->comment('Фото'),
            'created_at' => $this->integer()->notNull()->comment('Создана'),
            'updated_at' => $this->integer()->notNull()->comment('Изменена'),
            'category_id' => $this->integer()->comment('Категория'),
            'short_description' => $this->string(1024)->comment('Краткое описание'),
            'description' => $this->text()->comment('Текст статьи'),
            'SEO_TITLE' => $this->string(255)->comment('SEO TITLE'),
            'SEO_KEY' => $this->string(1024)->comment('SEO KEY'),
            'SEO_DESCRIPTION' => $this->string(1024)->comment('SEO DESCRIPTION'),
            'SEO_TAGS' => $this->string(1024)->comment('Теги'),
        ]);
        $this->createIndex('idx-article-category_id','article','category_id');
        $this->addForeignKey('fk-article-category_id','article','category_id','category_article','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-article-category_id','article');
        $this->dropTable('{{%article}}');
    }
}
