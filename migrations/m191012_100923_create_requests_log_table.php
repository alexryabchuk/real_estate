<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%requests_log}}`.
 */
class m191012_100923_create_requests_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%requests_log}}', [
            'id' => $this->primaryKey(),
            'requests_id'=>$this->integer()->comment('Заявка'),
            'created_at'=>$this->integer()->comment('Дата'),
            'user_id'=>$this->integer()->comment('Користувач'),
            'data'=>$this->string(512)->comment('Измененние значения'),
        ]);
        $this->createIndex('idx-requests_log-requests_id','requests_log','requests_id');
        $this->addForeignKey('fk-requests_log-requests_id','requests_log','requests_id','requests','id');
        $this->createIndex('idx-requests_log-user_id','requests_log','user_id');
        $this->addForeignKey('fk-requests_log-user_id','requests_log','user_id','user','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-requests_log-requests_id','requests_log');
        $this->dropForeignKey('fk-requests_log-user_id','requests_log');
        $this->dropTable('{{%requests_log}}');
    }
}
