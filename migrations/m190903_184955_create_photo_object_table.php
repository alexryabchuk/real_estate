<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%photo_object}}`.
 */
class m190903_184955_create_photo_object_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%photo_object}}', [
            'id' => $this->primaryKey(),
            'myobject_id'=>$this->integer()->comment('Объект'),
            'filename'=>$this->string()->comment('Файл'),
            'photo_type'=>$this->smallInteger()->comment('Тип фото')
        ]);
        $this->createIndex('idx-myobject_id','photo_object','myobject_id');
        $this->addForeignKey('fk-myobject_id','photo_object','myobject_id','myobject','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-myobject_id','photo_object');
        $this->dropTable('{{%photo_object}}');
    }
}
